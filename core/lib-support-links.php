<?php
# lib-support-links.php
# ---------------------
# A centralisation of URL for donation and support, to be sure to update them everywhere from a single place
# @author: David Revoy
# @license: http://www.gnu.org/licenses/gpl.html GPL version 3 or higher


function _display_support_links($option,$width,$height,$separator) {
  # $options:
  #   "thumbnails" or "": use a thumbnail img or not for the donation link.
  # $width/$height: specify the thumbnail size
  # $separator: let you put something to separate entries (eg. "<br>", "|", "," etc)
  global $root;
  global $lang;


  # Liberapay
  echo '    <a href="https://liberapay.com/davidrevoy/">'."\n";
  if ($option == "thumbnails"){
  echo '      <img class="svgToBttn" width="'.$width.'" height="'.$height.'" src="'.$root.'/core/img/patronage_liberapay.svg" title="Liberapay" Alt="Liberapay">'."\n";
  } else {
    echo '      Liberapay'."\n";
  }
  echo '    </a>'."\n";
  echo '    '.$separator.''."\n";

  # Patreon
  echo '    <a href="https://www.patreon.com/davidrevoy">'."\n";
  if ($option == "thumbnails"){
    echo '      <img class="svgToBttn" width="'.$width.'" height="'.$height.'" src="'.$root.'/core/img/patronage_patreon.svg" title="Patreon" Alt="Patreon">'."\n";
  } else {
    echo '      Patreon'."\n";
  }
  echo '    </a>'."\n";
  echo '    '.$separator.''."\n";

  # Tipeee
  echo '    <a href="https://www.tipeee.com/pepper-carrot">'."\n";
  if ($option == "thumbnails"){
  echo '      <img class="svgToBttn" width="'.$width.'" height="'.$height.'" src="'.$root.'/core/img/patronage_tipeee.svg" title="Tipeee" Alt="Tipeee">'."\n";
  } else {
    echo '      Tipeee'."\n";
  }
  echo '    </a>'."\n";
  echo '    '.$separator.''."\n";

  # Paypal
  echo '    <a href="https://paypal.me/davidrevoy">'."\n";
  if ($option == "thumbnails"){
  echo '      <img class="svgToBttn" width="'.$width.'" height="'.$height.'" src="'.$root.'/core/img/patronage_paypal.svg" title="Paypal" Alt="Paypal">'."\n";
  } else {
    echo '      Paypal'."\n";
  }
  echo '    </a>'."\n";
  echo '    '.$separator.''."\n";

  # IBAN
  echo '    <a href="'.$root.'/'.$lang.'/support/index.html#iban">'."\n";
  if ($option == "thumbnails"){
  echo '      <img class="svgToBttn" width="'.$width.'" height="'.$height.'" src="'.$root.'/core/img/patronage_iban.svg" title="IBAN" Alt="IBAN">'."\n";
  } else {
    echo '      Iban'."\n";
  }
  echo '    </a>'."\n";
  echo '    '.$separator.''."\n";
}

?>
