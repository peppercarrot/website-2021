<?php if ($root=="") exit;

# A module that output 12 recent pictures from the artwork repository.
# It's a WIP draft, unused, not sure if I'll use it, but I'll keep it around.

function findJpgFiles($dir) {
    $jpgFiles = [];

    # Create a recursive directory iterator
    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir));

    # Loop through the files
    foreach ($iterator as $file) {
        # Check if the file is a JPG and is in a 'low-res' directory
        if ($file->isFile() && strtolower($file->getExtension()) === 'jpg' && strpos($file->getPathname(), '/low-res/') !== false) {
            $filename = $file->getPathname();
            
            # Check if the filename starts with an ISO date format (YYYY-MM-DD)
            if (preg_match('/^\d{4}-\d{2}-\d{2}/', basename($filename))) {
                $jpgFiles[] = $filename; # Add only the filename
            }
        }
    }

    # Sort the filenames alphabetically
    usort($jpgFiles, function($a, $b) {
        # Extract filenames from paths
        $filenameA = basename($a);
        $filenameB = basename($b);
        
        # Compare filenames
        return strcmp($filenameA, $filenameB);
    });

    # Return the last four filenames
    $selected = array_slice($jpgFiles, -12);
    rsort($selected);
    return $selected;
}

$directory = $sources.'/0ther';
$lastFourJpgs = findJpgFiles($directory);

foreach ($lastFourJpgs as $jpg) {
    _img(''.$jpg, 'latest art', 100, 100, 84);
}

?>
