<?php if ($root=="") exit;


echo '<div class="container">';
include($file_root.'core/mod-menu-lang.php');
echo '</div>';

$license_link = ''.$root.'/'.$lang.'/license/index.html';

echo '<div class="container container-sml">'."\n";
echo '  <section class="page">'."\n";

echo '  <img class="round-contrast" src="'.$root.'/'.$sources.'/0ther/website/hi-res/2021-07-14_author_by-David-Revoy.jpg" title="'._("Photo of the author, David Revoy with his cat named Noutti while drawing the first episodes of Pepper&Carrot.").'">'."\n";

echo '  <br> '."\n";
echo '  <h2>'._("About").'</h2> '."\n";

echo '  <p>'._("Hi, my name is David Revoy and I'm a French artist born in 1981. I'm self-taught and passionate about drawing, painting, cats, computers, Gnu/Linux open-source culture, Internet, old school RPG video-games, old mangas and anime, traditional art, Japanese culture, fantasy…").'</p>'."\n";
echo '  <p>'._("After more than 10 years of freelance in digital painting, teaching, concept-art, illustrating and art-direction, I decided to start my own project. I finally found a way to mix all my passions together, the result is Pepper&Carrot.").'</p> '."\n";
echo ''."\n";
echo '  <p>'._("My blog: ").' <a href="https://www.davidrevoy.com">www.davidrevoy.com</a><br/> '."\n";
echo '     '._("My email: ").' <a href="mailto:info@davidrevoy.com">info@davidrevoy.com</a></p> '."\n";

echo '  <p>'._("I'm working on this project since May 2014, and I received the help of many contributors and supporters on the way. ").''."\n";

echo '  '._("Here is the list: ").'</p>'."\n";

echo '  <br>'."\n";
echo '  <div class="credits">'."\n";

echo '  <h2 id="credits">'._("Credits:").'</h2>'."\n";

_print_global($lang);

if ( $lang != "en"){
_print_website_translation($lang);
}

echo '  <h3>'._("Hereva worldbuilding:").'</h3>'."\n";

_print_hereva($lang);

echo '  <h3>'._("Episodes:").' </h3>'."\n";


# Loop on the folders for headers
foreach($pc_episodes_list as $epdirectory) {
  _print_title($lang, $epdirectory);
  echo '    '."\n";
  _print_credits($lang, $epdirectory);
  echo ''."\n";
}

echo '  <h2>'._("Special thanks to:").'</h2><br/>'."\n";
echo '  <strong>'._("All patrons of Pepper&amp;Carrot! Your support made all of this possible.").'</strong><br/>'."\n";

echo '  <strong>'._("All translators on Pepper&amp;Carrot for their contributions:").'</strong><br/>'."\n";
# Loop on the folders for headers
$alltranslators = array();
foreach($pc_episodes_list as $key => $ep_directory) {
$lang_for_this_episode = $episodes_all_translations[$key];
  foreach($lang_for_this_episode as $langjson) {
    $pattern = ''.$sources.'/'.$ep_directory.'/lang/'.$langjson.'/info.json';
    $test = $pattern;
    if (file_exists($pattern)){
      $translatorinfos = json_decode(file_get_contents($pattern), true);
      if (isset($translatorinfos['credits'])) {
        if (isset($translatorinfos['credits']['translation'])) {
          foreach ($translatorinfos['credits']['translation'] as $translator) {
            $alltranslators[] = $translator;
          }
        }
      }
    }
  }
}

$result = array_unique($alltranslators);
$result = array_diff($result, array("original version"));
_print_translatorinfos($lang, $result, "LIST_ENUMERATOR", "DOT_ENDING");
echo '<br/><br/>';
echo ''."\n";

# Special thanks to projects:
echo '  '._("<strong>The Framasoft team</strong> for hosting our oversized repositories via their Gitlab instance Framagit.").''."\n";
echo '  <br/><br/>'."\n";
echo ''."\n";

echo '  '._("<strong>The Libera.Chat staff and Matrix.org staff</strong> for our #pepper&carrot community channel.").''."\n";
echo '  <br/><br/>'."\n";
echo ''."\n";

echo '  '._("<strong>All Free/Libre and open-source software</strong> because Pepper&Carrot episodes are created using 100&#37; Free/Libre software on a GNU/Linux operating system. The main ones used in production being:").'<br/>'."\n";
echo '  '._("- <strong>Krita</strong> for artworks (krita.org).").'<br/>'."\n";
echo '  '._("- <strong>Inkscape</strong> for vector and speechbubbles (inkscape.org).").'<br/>'."\n";
echo '  '._("- <strong>Blender</strong> for artworks and video editing (blender.org).").'<br/>'."\n";
echo '  '._("- <strong>Kdenlive</strong> for video editing (kdenlive.org).").'<br/>'."\n";
echo '  '._("- <strong>Scribus</strong> for the book project (scribus.net).").'<br/>'."\n";
echo '  '._("- <strong>Gmic</strong> for filters and effects (gmic.eu).").'<br/>'."\n";
echo '  '._("- <strong>ImageMagick</strong> & <strong>Bash</strong> for 90&#37; of automation on the project.").'<br/>'."\n";

echo ''."\n";


$technicalinfos = json_decode(file_get_contents($sources.'/project-global-credits.json'), true);
echo '  <strong>'._("And finally to all developers who interacted on fixing Pepper&Carrot specific bug-reports:").'</strong> <br/>'."\n";
_print_translatorinfos($lang, $technicalinfos['project-global-credits']['bug-fix-heroes'], "LIST_ENUMERATOR", "DOT_ENDING");
echo '  <br/><br/><strong>'._("... and anyone I've missed.").'</strong>'."\n";
echo '  <br/><br/>'."\n";
echo '  <br/><br/>'."\n";
echo ''."\n";

echo '  <h2>'._("License:").'</h2>'."\n";
echo '  <a href="https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).'"><img src="'.$root.'/core/img/ccby-large.jpg" style="margin-top: 25px;" alt="CC BY"/></a><br/>'."\n";
# A note here, because the Copyright © will probably rise eyebrows on a Free/Libre and Open-source Project.
# It is advised to declare a 'copyright holder' before also declaring releasing as Creative Commons for some copyright code of some countries.
;
# Note to translators: The %s placeholders are an opening and a closing link tag
echo '  '.sprintf(_('This work is licensed under a %sCreative Commons Attribution 4.0 International license%s'), '<strong><a href="https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).'">', '</a></strong>')."<br/>\n";
echo '  '._("Copyright © David Revoy").' '.date("Y").', '."\n";
echo '  <a href="https://www.peppercarrot.com">www.peppercarrot.com</a><br/><br/>'."\n";

echo '    </div>'."\n"; # -> class=credits
echo '  </section>'."\n";
echo '</div>'."\n";
echo ''."\n";

?>
