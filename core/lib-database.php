<?php
# lib-database.php
# -------------
# Lib-database contains various receipe to scan all episodes, langs and more.
# These loops cost CPU/disk access resources and affect server performances.
# For this reason, this methods here are all cached on disk ('cache/' at root).
# Lib-database returns a set of 4 useful arrays.
# Require a 'cache/' folder at root of website with write permissions.
# Require a database file based, like '0_sources/' of Pepper&Carrot.
# Usage: just include the file on the header of the code.
#
# @author: David Revoy
# @license: http://www.gnu.org/licenses/gpl.html GPL version 3 or higher


# Check if we can write in cache/ before starting.
if (!is_writable($cache.'/')) {
  echo '<b style="color: red;font-family:monospace;">Carrot ask write permission to "'.$cache.'/"</b>';
}

# Start with loading the main function
_load_databases();

# Main function:  load the databases if available or ask to regenerate them.
#                 the system listen to renderfarm changes via $sources/last_updated.txt'
function _load_databases() {

  global $debugmode;
  global $sources;
  global $cache;
  global $option;
  global $languages_info;
  global $isolang;
  global $languages_available;
  global $website_translation;
  global $pc_episodes_list;
  global $mft_episodes_list;
  global $webcomic_misc_list;
  global $webcomic_fan_list;
  global $webcomic_xyz_list;
  global $episodes_all_translations;
  global $mft_all_translations;
  global $misc_all_translations;
  global $fan_all_translations;
  global $all_percent;
  global $po_translation_percent;
  global $recent_artworks;

  # Put value '1' on index.php at root to display backstage info. 
  if ($debugmode > 1.9){
    # Delete the tokken: it forces the system to regenerate all each time.
    unlink($cache.'/_last_updated.txt');
  }

  # Prevents an error when comparing last_updated to a non existing file
  if (!file_exists($cache.'/_last_updated.txt')) {
    $cache_placeholder = fopen($cache.'/_last_updated.txt',"w");
    fwrite($cache_placeholder,"placeholder");
    fclose($cache_placeholder);
    chmod($cache.'/_last_updated.txt', 0620);
  }

  # Read the tokken that renderfarm update
  if (file_exists(''.$sources.'/last_updated.txt')) {
    # Compare it to a version we copied previously on our cache
    if (sha1_file(''.$sources.'/last_updated.txt') !== sha1_file($cache.'/_last_updated.txt')) {
      # They differ: regenerate databases
      _generate_databases();
      # Make a new copy of most recent tokken on our cache
      copy(''.$sources.'/last_updated.txt', 'cache/_last_updated.txt');
      chmod($cache.'/_last_updated.txt', 0620);
    }
  }
  
  # Load the databases saved on /cache
  # If the file doesn't exists, recreate them all
  if (file_exists($cache.'/_db_languages_info.txt')) {
    $languages_info = unserialize(file_get_contents($cache.'/_db_languages_info.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_isolang.txt')) {
    $isolang = unserialize(file_get_contents($cache.'/_db_isolang.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_languages_available.txt')) {
    $languages_available = unserialize(file_get_contents($cache.'/_db_languages_available.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_website_translation.txt')) {
    $website_translation = unserialize(file_get_contents($cache.'/_db_website_translation.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_pc_episodes_list.txt')) {
    $pc_episodes_list = unserialize(file_get_contents($cache.'/_db_pc_episodes_list.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_mft_episodes_list.txt')) {
    $mft_episodes_list = unserialize(file_get_contents($cache.'/_db_mft_episodes_list.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_webcomic-misc_list.txt')) {
    $webcomic_misc_list = unserialize(file_get_contents($cache.'/_db_webcomic-misc_list.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_webcomic-fan_list.txt')) {
    $webcomic_fan_list = unserialize(file_get_contents($cache.'/_db_webcomic-fan_list.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_webcomic_xyz_list.txt')) {
    $webcomic_xyz_list = unserialize(file_get_contents($cache.'/_db_webcomic_xyz_list.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_episodes_all_translations.txt')) {
    $episodes_all_translations = unserialize(file_get_contents($cache.'/_db_episodes_all_translations.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_mft_all_translations.txt')) {
    $mft_all_translations = unserialize(file_get_contents($cache.'/_db_mft_all_translations.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_misc_all_translations.txt')) {
    $misc_all_translations = unserialize(file_get_contents($cache.'/_db_misc_all_translations.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_fan_all_translations.txt')) {
    $fan_all_translations = unserialize(file_get_contents($cache.'/_db_fan_all_translations.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_all_percent.txt')) {
    $all_percent = unserialize(file_get_contents($cache.'/_db_all_percent.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_po_translation_percent.txt')) {
    $po_translation_percent = unserialize(file_get_contents($cache.'/_db_po_translation_percent.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_recent_artworks.txt')) {
    $recent_artworks = unserialize(file_get_contents($cache.'/_db_recent_artworks.txt'));
  } else {
    _generate_databases();
  }
}

# Generator of databases: the big array are created here and then saved to cache/
function _generate_databases() {

  global $root;
  global $cache;
  global $sources;

  # Database: a convertion of 0_sources/langs.json to PHP array.
  # ============================================================
  # eg. [en] => Array ( [translators] => Array ( [0] => Saffron [1] => Pepper [2] => Carrot )
  #                     [name] => English
  #                     [local_name] => English
  #                     [iso_code] => en
  #                     [iso_version] => 1
  #                     [font] => Array ( [family] => Lavi [size] => 36 ) )
  # Convert the JSON database made by renderfarm to a Php array
  if (file_exists($sources.'/langs.json')) {
    $languages_info = array();
    $languages_info = json_decode(file_get_contents(''.$sources.'/langs.json'), true);
    # Write to disk
    file_put_contents($cache.'/_db_languages_info.txt', serialize($languages_info));
    chmod($cache.'/_db_languages_info.txt', 0620);
  }

  # Database: all languages available.
  # ==================================
  if (file_exists($sources.'/langs.json')) {
    # eg. [0] => ar [1] => bn [2] => br [3] => ca [4] => cn [5] => cs [6] => da [7]
    $languages_available = array();
    # Convert the JSON database made by renderfarm to a Php array
    $lang_database = json_decode(file_get_contents(''.$sources.'/langs.json'), true);
    # Trim the database to only the first key: a list of ISO-639-1-like name
    $languages_available = array_keys($lang_database);
    sort($languages_available);
    # Write to disk
    file_put_contents($cache.'/_db_languages_available.txt', serialize($languages_available));
    chmod($cache.'/_db_languages_available.txt', 0620);
  }

  # Database: a build of the ISO official lang extracted from $languages_info
  # =========================================================================
  # eg. en => en, mx => es_MX, jb => jbo
  $isolang = array();
  # Loop threw all available Pepper&Carrot ISO language
  foreach($languages_available as $language) {
    # Decode the information available for the current language
    $langinfo = $languages_info[$language];

    # Leave empty the lang without iso_code and don't bug for that
    if (!isset($langinfo['iso_code']) || empty($langinfo['iso_code'])) {
      $result = "";
    } else {
      # We have an iso_code, use it:
      $result = $langinfo['iso_code'];
      # We have an iso_script, use it:
      if (isset($langinfo['iso_script']) || !empty($langinfo['iso_script'])) {
        $result .= '_'.$langinfo['iso_script'];
      }
      # We have an iso_locale country code, use it:
      if (isset($langinfo['iso_locale']) || !empty($langinfo['iso_locale'])) {
        $result .= '_'.$langinfo['iso_locale'];
      }
    }
    # Build the array
    # Note: a single liner to build (in $array) "$1" => "$2" 
    # $array[$1]=[$2]
    $isolang[$language] = $result;
  }
  # Write to disk
  file_put_contents($cache.'/_db_isolang.txt', serialize($isolang));
  chmod($cache.'/_db_isolang.txt', 0620);

  # Database: all website translation
  # =================================
  # eg. [0] => br [1] => ca [2] => cn [3] => cs [4] => de [5] => en [6] => eo [7]
  $website_translation = array();
  # Pattern to detect gettext compiled website translation (*.mo files)
  $mo_files = glob('locale/en_US.utf8/LC_MESSAGES/[a-z][a-z].mo');
  # Browse the result
  foreach($mo_files as $mo_file) {
      # Trim the path and keep the two letter ISO-639-1-like name
      $database_entry=basename($mo_file, '.mo');
      # Push it on the table
      array_push($website_translation, $database_entry);
  }
  sort($website_translation);
  # Write to disk
  file_put_contents($cache.'/_db_website_translation.txt', serialize($website_translation));
  chmod($cache.'/_db_website_translation.txt', 0620);


  # Database: all Pepper&Carrot episodes
  # ====================================
  # eg.  [0] => ep01_Potion-of-Flight [1] => ep02_Rainbow-potions [2] => ep03_The-secret-ingredients
  $pc_episodes_list = array();
  # Pattern to detect episode folder on 0_sources
  $all_episodes = glob(''.$sources.'/ep[0-9][0-9]*');
  # Browse the result
  foreach($all_episodes as $episode) {
    # Trim the path and keep directory name
    $database_entry=basename($episode);
    # Read the public status on info.json
    $episode_data = json_decode(file_get_contents(''.$sources.'/'.$database_entry.'/info.json'), true);
    $public_status = $episode_data["public"];
    if ($public_status !== 0){
      # Episode is public, push it on the table
      array_push($pc_episodes_list, $database_entry);
    }
  }
  sort($pc_episodes_list);
  # Write to disk
  file_put_contents($cache.'/_db_pc_episodes_list.txt', serialize($pc_episodes_list));
  chmod($cache.'/_db_pc_episodes_list.txt', 0620);


  # Database: all miniFantasyTheater episodes
  # =========================================
  # eg.  [0] => 001 [1] => 002 [2] => 003
  $mft_episodes_list = array();
  # Pattern to detect episode on 0_sources/miniFantasyTheater
  $all_episodes_info = glob(''.$sources.'/miniFantasyTheater/.[0-9][0-9][0-9].json');
  # Browse the result
  foreach($all_episodes_info as $episode_info) {
    # Trim the path and keep episode filename
    $database_entry=basename($episode_info);
    # Remove the jpg extension
    $database_entry= str_replace('.json', '', $database_entry);
    # Remove the leading dot
    $database_entry=ltrim($database_entry, '.');
    # Read the public status on info.json
    $episode_data = json_decode(file_get_contents($episode_info), true);
    $public_status = $episode_data["public"];
    if ($public_status !== 0){
      # Push it on the table
      array_push($mft_episodes_list, $database_entry);
    }
  }
  sort($mft_episodes_list);
  # Write to disk
  file_put_contents($cache.'/_db_mft_episodes_list.txt', serialize($mft_episodes_list));
  chmod($cache.'/_db_mft_episodes_list.txt', 0620);

  # Database: all webcomic-misc episodes
  # ====================================
  # eg.  [0] => 2011-08-01_Trophy-Hunting [1] => 2012-05-05_Heritage-en-couleur [2] => 2024-12-24_The-Magic-of-Christmas
  $webcomic_misc_list = array();
  # Pattern to detect episode folder on 0_sources/misc
  $all_misc_comics = glob(''.$sources.'/misc/[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_*');
  # Browse the result
  foreach($all_misc_comics as $misc_comic) {
    # Trim the path and keep directory name
    $database_entry=basename($misc_comic);
    # Read the public status on info.json
    $episode_data = json_decode(file_get_contents(''.$sources.'/misc/'.$database_entry.'/info.json'), true);
    $public_status = $episode_data["public"];
    if ($public_status !== 0){
      # Push it on the table
      array_push($webcomic_misc_list, $database_entry);
    }
  }
  sort($webcomic_misc_list);
  # Write to disk
  file_put_contents($cache.'/_db_webcomic-misc_list.txt', serialize($webcomic_misc_list));
  chmod($cache.'/_db_webcomic-misc_list.txt', 0620);

  # Database: all fancomics episodes
  # ====================================
  # eg.  [0] => Novels_by_Holger-Kraemer [1] => Pepper-and-Carrot-Mini_by_Nartance [2] => The-Storm-by-Jana-Kus-and-Juan-Jose-Segura
  $webcomic_fan_list = array();
  $all_fan_comics = glob(''.$sources.'/0ther/community/*_by_*');
  foreach($all_fan_comics as $fan_comic) {
    $database_entry=basename($fan_comic);
    array_push($webcomic_fan_list, $database_entry);
  }
  sort($webcomic_fan_list);
  # Write to disk
  file_put_contents($cache.'/_db_webcomic-fan_list.txt', serialize($webcomic_fan_list));
  chmod($cache.'/_db_webcomic-fan_list.txt', 0620);

  # Database: all webcomic XYZ episodes
  # ====================================
  # eg.  [0] => misc/2011-08-01_Trophy-Hunting [1] => miniFantasyTheater/003 [2] => ep38_The-Healer
  $webcomic_xyz_list = array();

  # Pattern to detect episode folder on 0_sources
  $all_episodes = glob(''.$sources.'/ep[0-9][0-9]*');
  # Browse the result
  foreach($all_episodes as $episode) {
    # Trim the path and keep directory name
    $database_entry=basename($episode);
    # Read the public status on info.json
    $episode_data = json_decode(file_get_contents(''.$sources.'/'.$database_entry.'/info.json'), true);
    $public_status = $episode_data["public"];
    if ($public_status == 0){
      # Episode is public, push it on the table
      array_push($webcomic_xyz_list, $database_entry);
    }
  }

  # Pattern to detect episode on 0_sources/miniFantasyTheater
  $all_episodes_info = glob(''.$sources.'/miniFantasyTheater/.[0-9][0-9][0-9].json');
  # Browse the result
  foreach($all_episodes_info as $episode_info) {
    # Trim the path and keep episode filename
    $database_entry=basename($episode_info);
    # Remove the jpg extension
    $database_entry= str_replace('.json', '', $database_entry);
    # Remove the leading dot
    $database_entry=ltrim($database_entry, '.');
    # Read the public status on info.json
    $episode_data = json_decode(file_get_contents($episode_info), true);
    $public_status = $episode_data["public"];
    if ($public_status == 0){
      # Push it on the table
      array_push($webcomic_xyz_list, 'miniFantasyTheater/'.$database_entry);
    }
  }
  
  # Pattern to detect episode folder on 0_sources/misc
  $all_misc_comics = glob(''.$sources.'/misc/[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_*');
  # Browse the result
  foreach($all_misc_comics as $misc_comic) {
    # Trim the path and keep directory name
    $database_entry=basename($misc_comic);
    # Read the public status on info.json
    $episode_data = json_decode(file_get_contents(''.$sources.'/misc/'.$database_entry.'/info.json'), true);
    $public_status = $episode_data["public"];
    if ($public_status == 0){
      # Push it on the table
      array_push($webcomic_xyz_list, 'webcomic-misc/'.$database_entry);
    }
  }
  sort($webcomic_xyz_list);
  # Write to disk
  file_put_contents($cache.'/_db_webcomic_xyz_list.txt', serialize($webcomic_xyz_list));
  chmod($cache.'/_db_webcomic_xyz_list.txt', 0620);


  # Database: all languages available per episode
  # =============================================
  # eg. [4] => Array ( [0] => br [1] => ca [2] => cn [3] => cs [4] => da )
  $episodes_all_translations = array();
  $all_episodes = glob(''.$sources.'/ep[0-9][0-9]*');
  # Browse episodes
  foreach($all_episodes as $episode_path) {
    # Open and reset a sub-table at each loop
    $episode_translations = array();
    # Check available translation in directory lang, pick only filtered [a-z][a-z]
    $all_lang_directories = glob(''.$episode_path.'/lang/[a-z][a-z]');
      foreach($all_lang_directories as $lang_directory) {
        # Filter directories only
        if(is_dir($lang_directory)) {
          # Trim the path, keep directory name
          $database_entry = basename($lang_directory);
          # Feed array with list of lang
          array_push($episode_translations, $database_entry);
        }
      }
      # Push for each episodes the sub-table array of lang
      array_push($episodes_all_translations, $episode_translations);
  }
  # Write to disk
  file_put_contents($cache.'/_db_episodes_all_translations.txt', serialize($episodes_all_translations));
  chmod($cache.'/_db_episodes_all_translations.txt', 0620);


  # Database: all languages available per MFT episode
  # =================================================
  # eg. [4] => Array ( [0] => br [1] => ca [2] => cn [3] => cs [4] => da )
  $mft_all_translations = array();
  foreach($mft_episodes_list as $id) {
    # Open and reset a sub-table at each loop
    $episode_translations = array();
    # Check available translation in directory lang, pick only filtered [a-z][a-z]
    $all_lang_directories = glob(''.$sources.'/miniFantasyTheater/lang/[a-z][a-z]');
      foreach($all_lang_directories as $lang_directory) {
        # Filter directories only
        if(is_file($lang_directory.'/'.$id.'.svg')) {
          # Trim the path, keep directory name
          $database_entry = basename($lang_directory);
          # Feed array with list of lang
          array_push($episode_translations, $database_entry);
        }
      }
      # Push for each episodes the sub-table array of lang
      array_push($mft_all_translations, $episode_translations);
  }
  # Write to disk
  file_put_contents($cache.'/_db_mft_all_translations.txt', serialize($mft_all_translations));
  chmod($cache.'/_db_mft_all_translations.txt', 0620);

  # Database: all languages available per Webcomic-Misc episode
  # ===========================================================
  # eg. [4] => Array ( [0] => br [1] => ca [2] => cn [3] => cs [4] => da )
  $misc_all_translations = array();
  foreach($webcomic_misc_list as $episode_path) {
    # Open and reset a sub-table at each loop
    $episode_translations = array();
    # Check available translation in directory lang, pick only filtered [a-z][a-z]
    $all_lang_directories = glob(''.$sources.'/misc/'.$episode_path.'/lang/[a-z][a-z]');
      foreach($all_lang_directories as $lang_directory) {
        # Filter directories only
        if(is_dir($lang_directory)) {
          # Trim the path, keep directory name
          $database_entry = basename($lang_directory);
          # Feed array with list of lang
          array_push($episode_translations, $database_entry);
        }
      }
      # Push for each episodes the sub-table array of lang
      array_push($misc_all_translations, $episode_translations);
  }
  # Write to disk
  file_put_contents($cache.'/_db_misc_all_translations.txt', serialize($misc_all_translations));
  chmod($cache.'/_db_misc_all_translations.txt', 0620);

  # Database: all languages available per Webcomic-Fan episode
  # ===========================================================
  # eg. [4] => Array ( [0] => br [1] => ca [2] => cn [3] => cs [4] => da )
  $fan_all_translations = array();
  foreach($webcomic_fan_list as $dir_path) {
    # Open and reset a sub-table at each loop
    $dir_translations = array();
    # Check available translation in directory lang, pick only filtered [a-z][a-z]
    $all_translated_files = glob(''.$sources.'/0ther/community/'.$dir_path.'/[a-z][a-z]_*');
      foreach($all_translated_files as $lang_translation) {
        if(is_file($lang_translation)) {
          # Trim the path, keep directory name
          $database_entry = basename($lang_translation);
          $database_entry = substr($database_entry, 0, 2);
          # Feed array with list of lang
          array_push($dir_translations, $database_entry);
          $dir_translations=array_unique($dir_translations);
        }
      }
      # Push for each episodes the sub-table array of lang
      array_push($fan_all_translations, $dir_translations);
  }
  # Write to disk
  file_put_contents($cache.'/_db_fan_all_translations.txt', serialize($fan_all_translations));
  chmod($cache.'/_db_fan_all_translations.txt', 0620);

  # Database: all percent completion per language
  # =============================================
  # eg. en => 100 fr => 20 cn => 83
  # Score description: 
  # - 84 = 84%
  # - 20 = 20%
  $all_percent = array();
  # Get the number of all episode
  $totalepisodecount = count($pc_episodes_list);
  # Browse language
  foreach($languages_available as $language) {
    # Reset this counter at each loops.
    $score_website = 0;
    $bonus = 0;
    $translationcompletion = 0;
    # Browse the list of episode (and get the key)
    foreach($pc_episodes_list as $key => $episode_directory) {
      # Create an array with available lang for the current episode key.
      $lang_for_this_episode = $episodes_all_translations[$key];
      # Compare the current language with the result of this array.
      if(in_array($language,$lang_for_this_episode)) {
        # Increment score when lang is found on this episode.
        $translationcompletion = $translationcompletion + 1;
      }
    }
    # Math to return the percent of completion: 
    $percent = ( $translationcompletion / $totalepisodecount ) * 100;
    # Trim floating results.
    $percent = round($percent, 0);

    # Feed the array with result
    $all_percent[$language] = $percent;
  }
  # Write to disk
  file_put_contents($cache.'/_db_all_percent.txt', serialize($all_percent));
  chmod($cache.'/_db_all_percent.txt', 0620);

  # Database: all percent of website translation completion
  # =======================================================
  # eg. en => 100 fr => 20 cn => 83
  $po_translation_percent = array();
  $path_to_po_percent_json = 'locale/en_US.utf8/LC_MESSAGES/po_translation_percent.json';
  # Convert the JSON database made by renderfarm to a Php array
  if (file_exists($path_to_po_percent_json)) {
    $po_translation_percent = json_decode(file_get_contents($path_to_po_percent_json), true);
  }
  # Write to disk
  file_put_contents($cache.'/_db_po_translation_percent.txt', serialize($po_translation_percent));
  chmod($cache.'/_db_po_translation_percent.txt', 0620);

  # Database: all recent artworks in 0_sources/0ther/
  # =================================================
  # eg. 
  $recent_artworks = array();
  $jpgFiles = [];
  $dir = $sources.'/0ther';
  $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir));
  # Loop through the files
  foreach ($iterator as $file) {
    # Check if the file is jpg in a low-res dir
    if ($file->isFile() && strtolower($file->getExtension()) === 'jpg' && strpos($file->getPathname(), '/low-res/') !== false) {
      $filename = $file->getPathname();
      # Check if the filename starts with an ISO date format (YYYY-MM-DD)
      if (preg_match('/^\d{4}-\d{2}-\d{2}/', basename($filename))) {
        $jpgFiles[] = $filename; # Add only the filename
      }
    }
  }
  # Sort all pictures found alphabetically
  usort($jpgFiles, function($a, $b) {
  # Extract filenames from paths
  $filenameA = basename($a);
  $filenameB = basename($b);
  # Compare filenames
  return strcmp($filenameA, $filenameB);
  });

  # Restrict to a number of item
  $recent_artworks = array_slice($jpgFiles, -14);
  $recent_artworks = array_reverse($recent_artworks);

  # Write to disk
  file_put_contents($cache.'/_db_recent_artworks.txt', serialize($recent_artworks));
  chmod($cache.'/_db_recent_artworks.txt', 0620);

}

# For testing and debug: print the array of the database
# (you'll have to call it manually by uncomenting its call on previous functions around line 65)
function _databases_debug() {

  global $languages_info;
  global $isolang;
  global $languages_available;
  global $website_translation;
  global $pc_episodes_list;
  global $mft_episodes_list;
  global $webcomic_misc_list;
  global $webcomic_fan_list;
  global $webcomic_xyz_list;
  global $episodes_all_translations;
  global $mft_all_translations;
  global $misc_all_translations;
  global $fan_all_translations;
  global $all_percent;
  global $po_translation_percent;
  global $recent_artworks;
  
  echo '<div style="color: #9cbf05; font: 11px monospace; background-color: #2e3436; width: 800px; margin: 0 auto; padding: 20px;">';

  echo '<details>';
  echo '<summary>[$languages_available]</summary>:<br/>';
  print_r($languages_available);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$pc_episodes_list]</summary>:<br/>';
  print_r($pc_episodes_list);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$mft_episodes_list]</summary>:<br/>';
  print_r($mft_episodes_list);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$webcomic_misc_list]</summary>:<br/>';
  print_r($webcomic_misc_list);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$webcomic_fan_list]</summary>:<br/>';
  print_r($webcomic_fan_list);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$webcomic_xyz_list]</summary>:<br/>';
  print_r($webcomic_xyz_list);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$all_percent]:</summary>';
  print_r($all_percent);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$po_translation_percent]</summary>:<br/>';
  print_r($po_translation_percent);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$website_translation]</summary>:<br/>';
  print_r($website_translation);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$isolang]</summary>:<br/>';
  print_r($isolang);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$languages_info]</summary>:<br/>';
  print_r($languages_info);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$episodes_all_translations]</summary>:<br/>';
  print_r($episodes_all_translations);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$mft_all_translations]</summary>:<br/>';
  print_r($mft_all_translations);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$misc_all_translations]</summary>:<br/>';
  print_r($misc_all_translations);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$fan_all_translations]</summary>:<br/>';
  print_r($fan_all_translations);
  echo '</details><br>';

  echo '<details>';
  echo '<summary>[$recent_artworks]</summary>:<br/>';
  print_r($recent_artworks);
  echo '</details><br>';

 
  echo '</div>';
}

?>
