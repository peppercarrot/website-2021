<?php if ($root=="") exit;

echo '<div class="container">'."\n";

# Include the language selection menu
include($file_root.'core/mod-menu-lang.php');

# Sidebar
# -------
echo '  <aside class="col sml-12 med-12 lrg-2">'."\n";
echo '    <div class="sidebar-img">'."\n";

# Array of directories available
$sources_directories = glob($sources.'/0ther/*');
sort($sources_directories);
$dirclass = '';

_sidebar_img_btn("webcomics", "peppercarrot");
_sidebar_img_btn("webcomics", "miniFantasyTheater");
_sidebar_img_btn("webcomics", "webcomics-misc");

echo '  </div>'."\n";
echo '  </aside>'."\n";
echo ''."\n";

# Pepper&Carrot
# -------------
if ($content == 'index' || $content == 'peppercarrot') {

  echo '<article class="col sml-12 med-12 lrg-10">'."\n";

  _header("peppercarrot");

  # Comics
  $all_episodes = array();
  $all_episodes = $pc_episodes_list; # → lib-database.php
  rsort($all_episodes);
  $all_episodes_count = count($all_episodes);
  # Comments
  $episode_comments = array();
  $episode_comments = json_decode(file_get_contents(''.$sources.'/comments.json'), true);

  foreach ($all_episodes as $key => $episode_directory) {

    # Load info metadata
    $episode_info = array();
    $episode_info = json_decode(file_get_contents(''.$sources.'/'.$episode_directory.'/info.json'), true);
    $episode_date = $episode_info["published"];

    # Extract a title
    $episode_titles = array();
    $episode_titles = json_decode(file_get_contents(''.$sources.'/'.$episode_directory.'/hi-res/titles.json'), true);

    # Get comment metadata
    $episode_number = preg_replace('/[^0-9.]+/', '', $episode_directory);
    $comments_info = $episode_comments['ep'.$episode_number];
    $comments_url = $comments_info["url"];
    $comments_number = $comments_info["nb_com"];

    # Test if a sample is translated or not, and manage fallback if it is not
    $class = '';
    $cover_path = ''.$sources.'/'.$episode_directory.'/low-res/'.$lang.''.$credits.'E'.$episode_number.'.jpg';
    if (!file_exists($cover_path)) {
      $cover_path = ''.$sources.'/'.$episode_directory.'/hi-res/en'.$credits.'E'.$episode_number.'.jpg';
      $locale_title = $episode_titles["en"];
      $class = 'notranslation';
    } else {
      $locale_title = $episode_titles[$lang];
      $class = 'translated';
    }
    $cover_path = ''.$sources.'/'.$episode_directory.'/low-res/gfx-only/gfx'.$credits.'E'.$episode_number.'.jpg';

    $peppercarrot_title = _return_title($lang, $episode_directory);

    # Display the thumbnail
    $cover_description = ''.$locale_title.' '._("(click to open the episode)").'';
    $episode_link = $root.'/'.$lang.'/webcomic/'.$episode_directory.'.html';
    echo '    <figure class="thumbnail '.$class.' col sml-12 med-6 lrg-4">'."\n";
    echo '      <a href="'.$episode_link.'">'."\n";
    echo '        '._img($root.'/'.$cover_path, $cover_description, 480, 399, 89).''."\n";
    echo '      </a>'."\n";
    echo '    <figcaption><a href="'.$episode_link.'">'.$peppercarrot_title.'</a><br><span class="caption-smaller">'.sprintf(_("Published on %s."), $episode_date).'</span></figcaption>'."\n";
    echo '    </figure>'."\n";
  }

# miniFantasyTheater
# ------------------
} elseif ($content == 'miniFantasyTheater') {
  
  echo '<article class="col sml-12 med-12 lrg-10">'."\n";

  _header("miniFantasyTheater");

  # Comics
  $all_comicStripsID = array();
  $all_comicStripsID = $mft_episodes_list; # → lib-database.php
  rsort($all_comicStripsID);
  $all_comicStrips_count = count($all_comicStripsID);

  foreach ($all_comicStripsID as $key => $comicStripID) {
    # Load .XXX.json (episode) metadata
    $info_ep = array();
    $info_ep = json_decode(file_get_contents(''.$sources.'/miniFantasyTheater/.'.$comicStripID.'.json'), true);
    $comicstrip_date = $info_ep["published"];

    # Load lang/??/XXX_info.json (translation) metadata
    # Also test if we have a translation or not
    $info_lang = array();
    $info_lang_file = ''.$sources.'/miniFantasyTheater/lang/'.$lang.'/'.$comicStripID.'_info.json';
    $class = 'translated';
    if (!file_exists($info_lang_file)) {
      $info_lang_file = ''.$sources.'/miniFantasyTheater/lang/en/'.$comicStripID.'_info.json';
      $class = 'notranslation';
    }
    $info_lang = json_decode(file_get_contents($info_lang_file), true);

    # Alt/title
    $comicStripID_without_leading_zero = ltrim($comicStripID, '0');
    $comicstrip_thumb_alt = $comicStripID_without_leading_zero.'. '.$info_lang["title"];
    $comicstrip_thumb_title = $comicstrip_thumb_alt.' '._("(click to open the episode)").'';

    # Display the thumbnail
    $comicstrip_link = $root.'/'.$lang.'/miniFantasyTheater/'.$comicStripID.'.html';
    $comicstrip_thumb=''.$sources.'/miniFantasyTheater/low-res/thumbnails/'.$comicStripID.'-thumb_miniFantasyTheater.jpg';
    echo '    <figure class="thumbnail '.$class.' col sml-6 med-4 lrg-3">'."\n";
    echo '      <a href="'.$comicstrip_link.'">'."\n";
    echo '        '._img($root.'/'.$comicstrip_thumb, $comicstrip_thumb_title, 350, 350, 86).''."\n";
    echo '      </a>'."\n";
    echo '    <figcaption><a href="'.$comicstrip_link.'">'.$comicstrip_thumb_alt.'</a><br><span class="caption-smaller">'.sprintf(_("Published on %s."), $comicstrip_date).'</span></figcaption>'."\n";
    echo '    </figure>'."\n";
  }

# Misc
# ----
} elseif ($content == 'misc') {
  echo '<article class="col sml-12 med-12 lrg-10">'."\n";

  _header("webcomics-misc");

# Comics
  rsort($webcomic_misc_list);  # → lib-database.php
  $webcomic_misc_list_count = count($webcomic_misc_list);
  foreach ($webcomic_misc_list as $key => $webcomic_misc_dir) {
      # Load /info.json (episode) metadata
      $info_ep = array();
      $info_ep = json_decode(file_get_contents(''.$sources.'/misc/'.$webcomic_misc_dir.'/info.json'), true);
      $webcomic_misc_date = $info_ep["published"];

      # Load /lang/??/info.json (translation) metadata
      # Also test if we have a translation or not
      $info_lang = array();
      $info_lang_file = ''.$sources.'/misc/'.$webcomic_misc_dir.'/lang/'.$lang.'/info.json';
      $class = 'translated';
      if (!file_exists($info_lang_file)) {
        $info_lang_file = ''.$sources.'/misc/'.$webcomic_misc_dir.'/lang/en/info.json';
        $class = 'notranslation';
      }
      $info_lang = json_decode(file_get_contents($info_lang_file), true);


      # Find a cover for the current translation
      $webcomic_misc_cover_search = array();
      $webcomic_misc_cover_search = glob($sources.'/misc/'.$webcomic_misc_dir.'/low-res/'.$lang.'_cover*');
      if (empty($webcomic_misc_cover_search)) {
        $webcomic_misc_cover_search = glob($sources.'/misc/'.$webcomic_misc_dir.'/low-res/en_cover*');
      }
      $webcomic_misc_cover = $webcomic_misc_cover_search[0];
      if (!file_exists($webcomic_misc_cover)) {
        $webcomic_misc_cover = str_replace('/'.$lang.'_', '/en_', $webcomic_misc_cover);
      }

      # Alt/title
      $webcomic_misc_title = $info_lang["title"];
      $webcomic_misc_thumb_title = $webcomic_misc_title.' '._("(click to open the episode)").'';

      $comicstrip_link = $root.'/'.$lang.'/webcomic-misc/'.$webcomic_misc_dir.'.html';
      echo '    <figure class="thumbnail '.$class.' col sml-12 med-6 lrg-4">'."\n";
      echo '      <a href="'.$comicstrip_link.'">'."\n";
      echo '        '._img($root.'/'.$webcomic_misc_cover, $webcomic_misc_thumb_title, 480, 399, 89).''."\n";
      echo '      </a>'."\n";
      echo '    <figcaption><a href="'.$comicstrip_link.'">'.$webcomic_misc_title.'</a><br><span class="caption-smaller">'.sprintf(_("Published on %s."), $webcomic_misc_date).'</span></figcaption>'."\n";
      echo '    </figure>'."\n";

    }
}
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '      </div>'."\n"; # -> <div class="gallery-page-container">, function header.
echo '    </div>'."\n"; # -> <div class="header-page">, function header
echo '  </article>'."\n";
echo '</div>'."\n";
?>
