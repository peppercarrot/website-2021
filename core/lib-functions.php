<?php
# lib-functions.php
# -----------------
# A collections of small functions that helps at repeating recuring task accross multiple pages.
#
# @author: David Revoy
# @license: http://www.gnu.org/licenses/gpl.html GPL version 3 or higher

# Useful CSS way to reset vertical
function _clearboth() {
  echo '<div style="clear:both;"></div>'."\n";
}

function _trim_leading_zero($string) {
  $string = ltrim($string, '0');
  return $string;
}

# Md transcript to HTML
function _read_md_transcript($file) {
  $content = file_get_contents($file);
  $Parsedown = new Parsedown();
  return $Parsedown->text($content);
}

# The Alt text for alt and title img elements
function _md_to_alt($file) {
  $content = file_get_contents($file);
  $md_cleaned = preg_replace('/^##.*\n/m', '', $content);
  $md_cleaned = preg_replace('/^\n/m', '', $md_cleaned, 1);
  $md_cleaned = preg_replace('/^> /m', '', $md_cleaned);
  $md_cleaned = preg_replace('/\*\*/m', '', $md_cleaned);
  $md_cleaned = preg_replace('/\_\_/m', '', $md_cleaned);
  return htmlspecialchars($md_cleaned, ENT_QUOTES, 'UTF-8');
}


# Print an ISO date (YYYY-MM-DD)
function _print_episode_date($file) {
  $episodeinfos = json_decode(file_get_contents($file), true);
  if (isset($episodeinfos['published'])) {
    return $episodeinfos['published'];
  }
}

# Build a gallery in continuity of the header page color
function _header($target, $type = 'normal') {
  global $root;
  global $content_info;
  $title = $content_info[$target][0];
  $text = $content_info[$target][1];
  $bg_color = "#CCCCCC";
  if (isset($content_info[$target][2])) {
    $bg_color = $content_info[$target][2];
  }
  $css_page_bg="background: linear-gradient(to bottom, ".$bg_color." 0px, rgba(62,62,62,1) 600px);";
  $css_bg="background: linear-gradient(to top, ".$bg_color." 0px, rgba(0,0,0, 0) 20px), url('".$root."/core/img/header-".$target.".jpg'); ";
  # Display
  echo '<div class="gallery-page-container">'."\n";
  echo '  <div class="header" style="'.$css_bg.'">'."\n";
  echo '    <div class="teaser">'."\n";
  echo '    <h2>'.$title.'</h2>'."\n";
  echo '      <span class="text-glow">'.$text.'</span>'."\n";
  echo '    </div> '."\n";
  echo '  </div>'."\n";
  echo '  <div class="header-page" style="'.$css_page_bg.'">'."\n";
  if ($type == "gallery"){
    _gallery("$target");
    echo '  </div>'."\n";
    echo '</div>'."\n";
  }
}

# Build a small header with only title and text; for pages with already heavy visual impact and thumbnails.
function _header_sml($target) {
  global $content_info;
  $title = $content_info[$target][0];
  $text = $content_info[$target][1];
  echo '<div class="header-sml">'."\n";
  echo '  <div class="title">'."\n";
  echo '    <h2>'.$title.'</h2>'."\n";
  echo '  </div> '."\n";
  echo '  <div class="text">'."\n";
  echo '  '.$text.''."\n";
  echo '  </div>'."\n";
  echo '</div>'."\n";
}

function _header_sml_fancomics($title, $text) {
  echo '<div class="header-sml-fancomics">'."\n";
  echo '  <div class="title">'."\n";
  echo '    <h2>'.$title.'</h2>'."\n";
  echo '  </div> '."\n";
  echo '  <div class="text">'."\n";
  echo '  '.$text.''."\n";
  echo '  </div>'."\n";
  echo '</div>'."\n";
}

# Count and return the number of low-res jpg for a directory
function _gallery_number($content) {
  global $sources;
  global $lang;
  global $root;
  $counter = 0;
  if(is_dir($sources.'/0ther/'.$content.'/low-res/')) {
    $all_thumbnails = glob($sources.'/0ther/'.$content.'/low-res/*.jpg');
    $counter = count($all_thumbnails);
  }
  return $counter;
}

# list all the jpg as thumbnails of a target directory
function _gallery($content) {
  global $sources;
  global $lang;
  global $root;
  # Browser mode
  # Content: list the files
  echo '    <ul id="gallery">'."\n";
  if(is_dir($sources.'/0ther/'.$content.'/low-res/')) {
    $all_thumbnails = glob($sources.'/0ther/'.$content.'/low-res/*.jpg');
    rsort($all_thumbnails);
    foreach ($all_thumbnails as $thumb) {
      $thumb_filename = basename($thumb);
      $thumb_fullpath = dirname($thumb);
      $thumb_option_link = urlencode(str_replace('.jpg', '', $thumb_filename));
      $cover_description = urldecode($thumb_option_link); #TODO better title/alt
      echo '      <li id="'.$thumb_option_link.'">'."\n";
      echo '        <a href="'.$root.'/'.$lang.'/viewer/'.$content.'__'.$thumb_option_link.'.html">'."\n";
      echo '          ';
        # 1800px wide is only for extra large wide image.
        _img($root.'/'.$sources.'/0ther/'.$content.'/low-res/'.$thumb_filename, $cover_description, 1800, 320, 45);
      echo ''."\n";
      echo '        </a>'."\n";
      echo '      </li>'."\n";
    }
  
  # Workaround: Adding 5 empty (but not empty for validation) <li> elements at the end.
  #             It workarounds in case of a gallery line ends with a single one artwork.
  #             In this unfortunate case, the artwork's thumbnail is stretched to the 
  #             full line and it doesn't look good: upscaled pixels, big crop.
  #             Adding five empty block forces it to take only 1/6 of a line. 
  #             What about the other case? When a line has many elements? 
  #             Well, in this case this workaround is transparent, and that's because of
  #             Flex (CSS): if many elements are on the last line, this &nbsp; naturally
  #             compress to a single block. So, all in all a cool wokaround, but one that's
  #             hard to understand.
  echo '      <li>&nbsp;</li>'."\n";
  echo '      <li>&nbsp;</li>'."\n";
  echo '      <li>&nbsp;</li>'."\n";
  echo '      <li>&nbsp;</li>'."\n";
  echo '      <li>&nbsp;</li>'."\n";
  }
  echo '    </ul>'."\n";
}

# Mainly used by webcomic-sources, to show a file with its size.
function _displayfile($file_path){
  global $root;
  if (file_exists($file_path)) {
    $filename = basename($file_path);
    $filename = str_replace('_by-David-Revoy', '', $filename);
    $fileweight = (filesize($file_path) / 1024) / 1024;
    echo '<a href="'.$root.'/'.$file_path.'" target="_blank" >'.$filename.' <em class="filesize">'.round($fileweight, 2).'MB </em></a><br />';
  } else {
    echo '[bug 404:'.$file_path.']';
  }
}


# Print a sidebar menu entry for the Wiki, Documentation
# and Files (last item of Artworks).
function _menu_item($label, $target, $icon) {
  # $label = i18n label
  # $target = 0_sources/0ther/"$target"
  # $icon = icon filename (in /core/img/ without svg extension), also will be used as a css class to decorate
  global $content;
  global $lang;
  global $root;
  $custom_style = ' '.$icon.' ';
  $alt = str_replace("sidebar-menu-", "", $icon);
  # Manage active status for the button
  preg_match('/.*\/(.*)/', $target, $activepage);
  if ($activepage[1] == $content){
    $dirclass = ' active';
  } else {
    $dirclass = '';
  }
  echo '      <a class="sidebar-btn'.$custom_style.''.$dirclass.'" href="'.$root.'/'.$lang.'/'.$target.'.html">'."\n";
  echo '        <img width="20" height="20" src="'.$root.'/core/img/'.$icon.'.svg" alt="'.$alt.'"/>&nbsp;'.$label.''."\n";
  echo '      </a>'."\n";
}

# Cards on Resources and Contributions
function _menu_card_item($title = 'Untitled', $picture_path = '', $description = 'Lorem Ipsum', $link = '#', $btn_text = 'Read More') {
  global $root;
  global $sources;
  global $mode;
  $css_class = '';
  if ($mode == "resources"){
    $css_class = ' card-btn-resources';
  }
  echo '  <article class="card">'."\n";
  echo '    <img src="'.$root.'/'.$sources.'/'.$picture_path.'" alt="'.$title.'"/>'."\n";
  echo '    <h3>'.$title.'</h3>'."\n";
    echo '  <p>'.$description.'</p>'."\n";
    echo '  <a href="'.$link.'" class="card-btn '.$css_class.'">'.$btn_text.'</a>'."\n";
  echo '  </article>'."\n";
}

# Get the third directory of a path
function _getThirdDirectory($path) {
  # eg. en/files/recent -> return 'recent'
  $parts = explode('/', $path);
  if (isset($parts[2])) {
      return $parts[2];
  } else {
      return null;
  }
}

# Build a header for pages with an aside bar
function _sidebar_img_btn($target_mode, $target_content) {
  global $root;
  global $lang;
  global $content;
  global $content_info;
  global $number_fanart;
  
  $label = $content_info[$target_content][0];

  $css_bg="background: url('".$root."/core/img/thumb_".$target_content.".jpg') no-repeat 50% 50%;";
  $css_smaller_class = '';
  # Exception for long label
  if (strlen($label) > 14) {
      $css_smaller_class = ' smaller';
  }
  # Exception for shorter URL to "webcomic-misc" 
  if ($target_content == "webcomics-misc") { 
    $target_content = "misc";
  }
  if ($target_content == "fan-art") { 
    $label = sprintf(ngettext('%d Fan-art', '%d Fan-art', $number_fanart),$number_fanart);
  }
  if ($content == $target_content) {
    $dirclass = ' active';
  } else {
    $dirclass = ' no-active';
  }
  echo '      <a id="'.$target_content.'" class="sidebar-img-btn '.$dirclass.''.$css_smaller_class.'" style="'.$css_bg.'" href="'.$root.'/'.$lang.'/'.$target_mode.'/'.$target_content.'.html">'."\n";
  echo '        '.$label.''."\n";
  echo '      </a>'."\n";
}

# Common error message for untranslated comics:
function _fallback_messsage() {
  global $fallbackmode;
  global $transladocumentationlink;

  if ( $fallbackmode == 1 ){
    echo '    <div class="notification">'."\n";
    echo '        '._("Oops! There is no translation available yet for this episode with the language you selected. The page will continue in English.").'<a href="'.$transladocumentationlink.'"> + '._("Add a translation").'</a>'."\n"; # -> mod-menu-lang.php
    echo '    </div>'."\n";
  }
}

# Common XYZ disclaimer
function _xyz_disclaimer() {
  global $public_status;

  if ( $public_status == 0 ){
    echo '    <div class="notification notification-warning">'."\n";
    echo '        Notice: This episode is in development and not publicly available. Access is limited to those with the link. Please do not share on social media.'."\n"; 
    echo '    </div>'."\n";
  }
}

################
# Related URLs #
################
# Related links is a feature for any type of episode
# Input: URLs in the info.json
# Output: A thumbnail, a title, a description (as on social media) using Open Metadata Graph

function _get_opengraph_data($url) {
    $html = file_get_contents($url);
    if ($html === false) {
        return null;
    }
    # Fix non UTF-8 documents
    # source: https://stackoverflow.com/a/76753521
    $html = mb_encode_numericentity(htmlspecialchars_decode(htmlentities($html, ENT_NOQUOTES, 'UTF-8', false),ENT_NOQUOTES), [0x80, 0x10FFFF, 0, ~0],'UTF-8');

    $dom = new DOMDocument();
    @$dom->loadHTML($html); // Suppress warnings for invalid HTML
    $metadata = [];
    $tags = $dom->getElementsByTagName('meta');

    foreach ($tags as $tag) {
        if ($tag->hasAttribute('property') && strpos($tag->getAttribute('property'), 'og:') === 0) {
            $property = $tag->getAttribute('property');
            $content = $tag->getAttribute('content');
            $metadata[$property] = $content;
        }
    }
    return $metadata;
}

function _list_related_url($info_json_file) {
  global $cache;
  global $lang;

  # Load the Urls from the info.json
  $path_to_json = dirname($info_json_file);
  $parent_dir = basename($path_to_json);
  $data = array();
  $data = json_decode(file_get_contents($info_json_file), true);
  $urls = $data['related-urls'];
  
  # Check if the first item exist
  if (!isset($urls[0]) || $urls[0] !== "") {

    # If it does, we display the box
    echo '<div class="webcomic-footer-box">'."\n";
    echo '    <h3>'._("Related links:").'</h3>'."\n";

    # Prepare target dir for downloading content
    $opengraph_data_dir = $cache.'/opengraph_data/';
    if (!is_dir($opengraph_data_dir)) {
      mkdir($opengraph_data_dir, 0755, true);
    }

    # For each related-urls in info.json do:
    if (is_array($urls) && count($urls) > 0) {
      foreach ($urls as $url) {
        $url = str_replace('https://www.peppercarrot.com/en', 'https://www.peppercarrot.com/'.$lang, $url);
        # Prepare the target path and filename
        $hash = md5($url);
        $short_hash = substr($hash, 0, 8);
        $cached_data = $opengraph_data_dir.'/'.$parent_dir.'_'.$short_hash.'.txt';
        # Load or Create the cache
        if (file_exists($cached_data)) {
          $cached_array = unserialize(file_get_contents($cached_data));
          $opengraph_data = $cached_array;
        } else {
          $opengraph_data = array();
          $opengraph_data = _get_opengraph_data($url);
          file_put_contents($cached_data, serialize($opengraph_data));
          chmod($cached_data, 0755);
        }
        # Display the result
        if (isset($opengraph_data['og:image'])) {
          $opengraph_thumbnail = $opengraph_data['og:image'];

        # Exception for Peertube and platform who returns other
        # og:image attribute (like width, size, format...) in
        # this case, the url is og:image:url
        } else if (isset($opengraph_data['og:image:url'])) {
          $opengraph_thumbnail = $opengraph_data['og:image:url'];

        # Placeholder blank image background.
        # TODO: maybe a link icon?
        } else {
          $opengraph_thumbnail = 'core/img/bg_dark.jpg';
        }

        if (isset($opengraph_data['og:title'])) {
          $opengraph_title = $opengraph_data['og:title'];
        } else {
          $opengraph_title = 'Untitled';
        }

        $short_url = substr($url, 0, 50);
        echo '<a href="'.$url.'" class="related-link-box">'."\n";
        # The _img function will cache the thumbnail for us
        echo '  '._img($opengraph_thumbnail, $opengraph_title, 120, 120, 86).''."\n";
        echo '  <h4>'.$opengraph_title.'</h4>'."\n";
        echo '  <p>'.$short_url.'[...]</p>'."\n";
        echo '</a>'."\n";
      }
    }

    echo '</div>'."\n";
  }
}

#############
# RSS Tools #
#############

# HTML entities encoding
function _rss_encode($string) {
  $encoded_string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
  return $encoded_string;
}

# Return a RFC822 date for the RSS
function _episode_rss_date($file) {
  $episodeinfos = json_decode(file_get_contents($file), true);
  if (isset($episodeinfos['published'])) {
    $iso_date = $episodeinfos['published'];
    $iso_date = $iso_date.'T18:30:00';
    $timestamp = strtotime($iso_date);
    $rss_date = date(DateTime::RFC822, $timestamp);
    return $rss_date;
  }
}

?>
