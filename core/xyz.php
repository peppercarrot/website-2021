<?php if ($root=="") exit;

echo '<div class="container">'."\n";

# Include the language selection menu
include($file_root.'core/mod-menu-lang.php');

echo ''."\n";

  echo '<section class="col sml-12 med-12 lrg-12">'."\n";

  _header("xyz");

  echo '<div class="webcomics-box">'."\n";

  # Comics
  $all_xyz_entries = array();
  $all_xyz_entries = $webcomic_xyz_list; # → lib-database.php
  $all_xyz_entries_count = count($all_xyz_entries);

  foreach ($all_xyz_entries as $key => $xyz_entry) {

    if (strpos($xyz_entry, 'ep') === 0) {
      $episode_link = 'webcomic/'.$xyz_entry.'.html';
    }
    if (strpos($xyz_entry, 'miniFantasyTheater') === 0) {
      $episode_link = ''.$xyz_entry.'.html';
    }
    if (strpos($xyz_entry, 'web') === 0) {
      $episode_link = ''.$xyz_entry.'.html';
    }

    echo '<a class="btn btn-xyz" href="'.$root.'/'.$lang.'/'.$episode_link.'">'.$episode_link.'</a><br>';
  }


echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '</div>'."\n";
echo '</section>'."\n";
echo '</div>'."\n";
?>
