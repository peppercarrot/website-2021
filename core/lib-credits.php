<?php

# lib-credits.php
# -------------
# Lib-credits contains function to ease printing the full credits for an episode
# Require also functions of lib-database.
# Usage: just include the file on the header of the code.
#
# @author: David Revoy, GunChleoc
#
# @license: http://www.gnu.org/licenses/gpl.html GPL version 3 or higher

# Print a tiny paragraph that include all the credits
# for a $lang and a $episode.

function _print_credits($lang, $epdirectory, $mft_comic_strip_ID = "") {

  global $sources;
  global $mode;
  global $languages_info;
  $projectpath = $sources.'/'.$epdirectory;

  if (empty($mft_comic_strip_ID)) {
    $json_file = $projectpath.'/info.json';
    $lang_json_file = $projectpath.'/lang/'.$lang.'/info.json';
  } else {
    $json_file = $projectpath.'/.'.$mft_comic_strip_ID.'.json';
    $lang_json_file = $projectpath.'/lang/'.$lang.'/'.$mft_comic_strip_ID.'_info.json';
  }
  
  # Debug path
  #echo '<br>';
  #echo $json_file.'<br>';
  #echo $lang_json_file.'<br>';

  if (file_exists($json_file)) {
    if (file_exists($lang_json_file)) {
      $foldername = basename($projectpath);
      # Read all infos from json on 0_sources:
      $episodeinfos = json_decode(file_get_contents($json_file), true);
      if (file_exists($projectpath.'/hi-res/titles.json')) {
        $episodetitle = json_decode(file_get_contents($projectpath.'/hi-res/titles.json'), true);
      }
      $translatorinfos = json_decode(file_get_contents($lang_json_file), true);
      $localname = $languages_info[$lang]['local_name'];

      # Art
      if (isset($episodeinfos['credits']['art'])) {
        echo '<br/>';
        echo ''._("Art:").' ';
        _print_translatorinfos($lang, $episodeinfos['credits']['scenario'], "LIST_ENUMERATOR", "DOT_ENDING").'';
      }
      # Scenario
      if (isset($episodeinfos['credits']['scenario'])) {
        # If it comes from episode source: new line; else: spaces
        if ($mode == "webcomic-sources") {
          echo '<br>'._("Scenario:").' ';
        } else {
          echo '&nbsp;&nbsp;&nbsp;'._("Scenario:").' ';
        }
        _print_translatorinfos($lang, $episodeinfos['credits']['scenario'], "LIST_ENUMERATOR", "DOT_ENDING").'';
      }
      # script-doctor
      if (isset($episodeinfos['credits']['script-doctor'])) {
        echo '<br/>';
        echo ''._("Script-doctor:").' ';
        _print_translatorinfos($lang, $episodeinfos['credits']['script-doctor'], "LIST_ENUMERATOR", "DOT_ENDING").'';
      }
      # inspiration
      if (isset($episodeinfos['credits']['inspiration'])) {
        echo '<br/>';
        echo ''._("Inspiration:").' ';
        _print_translatorinfos($lang, $episodeinfos['credits']['inspiration'], "LIST_ENUMERATOR", "DOT_ENDING").'';
      }
      # Beta-readers
      if (isset($episodeinfos['credits']['beta-readers'])) {
        echo '<br/>';
        echo ''._("Beta-readers:").' ';
        _print_translatorinfos($lang, $episodeinfos['credits']['beta-readers'], "LIST_ENUMERATOR", "DOT_ENDING").'';
      }
      if (isset($translatorinfos['credits'])) {
        if (isset($translatorinfos['credits']['translation'])) {
          if (in_array('original version', $translatorinfos['credits']['translation'])) {
            echo '<br/>';
            echo ''.$localname.' '._("(original version)").'';
          } else {
            echo '<br/>';
            echo ''._("Translation:").'&nbsp;['.$localname.']'.' ';
            _print_translatorinfos($lang, $translatorinfos['credits']['translation'], "LIST_ENUMERATOR", "DOT_ENDING", false).'';
          }
        }
        if (isset($translatorinfos['credits']['proofreading'])) {
          echo '<br/>';
          echo ''._("Proofreading:").' ';
          _print_translatorinfos($lang, $translatorinfos['credits']['proofreading'], "LIST_ENUMERATOR", "DOT_ENDING", false).'';
        }

        if (isset($translatorinfos['credits']['contribution'])) {
          echo '<br/>';
          echo ''._("Contribution:").' ';
          _print_translatorinfos($lang, $translatorinfos['credits']['contribution'], "LIST_ENUMERATOR", "DOT_ENDING", false).'';
        }

        if (isset($translatorinfos['credits']['notes'])) {
          echo '<br/>';
          # Reserved data field 'notes' for episode translation (info.json) that wasn't used (yet)
          echo ''._("Notes:").' ';
          _print_translatorinfos($lang, $translatorinfos['credits']['notes'], "SPACE_SEPARATOR", "EMPTY_ENDING").'';
        }
      }
    echo '<br/>'."\n";
    echo '<br/>'."\n";
    }
  }

}

# Print a tiny paragraph that include all the credits
# for a $lang and a $episode.
function _print_title($lang, $epdirectory) {

  global $sources;
  $projectpath = $sources.'/'.$epdirectory;

  if (file_exists($projectpath.'/info.json')) {
    if (file_exists($projectpath.'/lang/'.$lang.'/info.json')) {
      if (file_exists($projectpath.'/hi-res/titles.json')) {

        $foldername = basename($projectpath);
        # Read all infos from json on 0_sources:
        $episodeinfos = json_decode(file_get_contents($projectpath.'/info.json'), true);
        $episodetitle = json_decode(file_get_contents($projectpath.'/hi-res/titles.json'), true);
        $translatorinfos = json_decode(file_get_contents($projectpath.'/lang/'.$lang.'/info.json'), true);
        $allLangs = json_decode(file_get_contents('0_sources/langs.json'), true);
        $langinfo = $allLangs[$lang];
        $localname = $langinfo['local_name'];

        # Write the episode title in local $lang:
        echo '<strong>'.$episodetitle[$lang].'</strong>';
        # Date
        if (isset($episodeinfos['published'])) {
          echo ' <em style="font-size:0.8em;">('._("published on").' '.$episodeinfos['published'].')</em>';
        }

      }
    }
  }

}

# Return just the current episode title translated, or fallback to English
function _return_title($lang, $epdirectory) {
  global $sources;
  $projectpath = $sources.'/'.$epdirectory;
  if (file_exists($projectpath.'/hi-res/titles.json')) {
    $foldername = basename($projectpath);
    $episodetitle = json_decode(file_get_contents($projectpath.'/hi-res/titles.json'), true);
    if (array_key_exists($lang, $episodetitle)) {
      return $episodetitle[$lang];
    } else {
      return $episodetitle["en"];
    }
  }
}

# Print credits for the universe/wiki
# for a $lang and a $episode.
function _print_hereva($lang) {

  global $wiki;

  $technicalinfos = json_decode(file_get_contents($wiki.'/info.json'), true);
  echo ''._("Creation:").' ';
  _print_translatorinfos($lang, $technicalinfos['hereva-world-credits']['creation'], "LIST_ENUMERATOR", "DOT_ENDING");
  echo '<br/>';
  echo ''._("Lead Maintainer:").' ';
  _print_translatorinfos($lang, $technicalinfos['hereva-world-credits']['lead-maintainer'], "LIST_ENUMERATOR", "DOT_ENDING");
  echo '<br/>';
  # Writer as in content-writer not writer as a career.
  echo ''._("Writers:").' ';
  _print_translatorinfos($lang, $technicalinfos['hereva-world-credits']['writers'], "LIST_ENUMERATOR", "DOT_ENDING");
  echo '<br/>';
  echo ''._("Correctors:").' ';
  _print_translatorinfos($lang, $technicalinfos['hereva-world-credits']['correctors'], "LIST_ENUMERATOR", "DOT_ENDING");
  echo '<br/>';
  echo '<br/>';

}

# Print credits for website's translation
function _print_website_translation($lang) {

  global $languages_info;

  if (file_exists('po/'.$lang.'-credits.json')) {
    echo '  <h3>'._("Website translation:").'</h3>('.$languages_info[$lang]['local_name'].')'."\n";
    echo '  <br/>'."\n";

    $technicalinfos = json_decode(file_get_contents('po/'.$lang.'-credits.json'), true);
    if (isset($technicalinfos['website-translation-credits'])) {

      if (isset($technicalinfos['website-translation-credits']['translation'])) {
        echo ''._("Translation:").' ';
        _print_translatorinfos($lang, $technicalinfos['website-translation-credits']['translation'], "LIST_ENUMERATOR", "DOT_ENDING", false);
        echo '<br/>';
      }

      if (isset($technicalinfos['website-translation-credits']['proofreading'])) {
        echo ''._("Proofreading:").' ';
        _print_translatorinfos($lang, $technicalinfos['website-translation-credits']['proofreading'], "LIST_ENUMERATOR", "DOT_ENDING", false);
        echo '<br/>';
      }

      if (isset($technicalinfos['website-translation-credits']['contribution'])) {
        echo ''._("Contribution:").' ';
        _print_translatorinfos($lang, $technicalinfos['website-translation-credits']['contribution'], "LIST_ENUMERATOR", "DOT_ENDING", false);
        echo '<br/>';
        echo '<br/>';
      }

    }
  }
}

function _print_global($lang) {

  global $sources;

  $technicalinfos = json_decode(file_get_contents($sources.'/project-global-credits.json'), true);
  echo '<strong>'._("Technical maintenance and scripting:").' </strong><br/>';
  _print_translatorinfos($lang, $technicalinfos['project-global-credits']['technic-and-scripting'], "LIST_ENUMERATOR", "DOT_ENDING");
  echo '<br/>';
  echo '<br/>';
  echo '<strong>'._("General maintenance of the database of SVGs:").' </strong><br/>';
  _print_translatorinfos($lang, $technicalinfos['project-global-credits']['svg-database'], "LIST_ENUMERATOR", "DOT_ENDING");
  echo '<br/>';
  echo '<br/>';
  echo '<strong>'._("Website maintenance and new features:").' </strong><br/>';
  _print_translatorinfos($lang, $technicalinfos['project-global-credits']['website-code'], "LIST_ENUMERATOR", "EMPTY_ENDING");
  echo '<br/>';

}

# Print a translator name + optional link as description data item
# Names + links have the format 'Name <http|mailto ...>'
# Links can be omitted for just a name
function _print_translatorinfos($lang, $translatorinfos, $separators, $ending, $toSort = true) {
  # Sorts alphabetically by default; to override, add false to function call
  if ($toSort === true) { natcasesort($translatorinfos); }
  $number = count($translatorinfos);
  $counter = 0;

  // Define language-specific mappings
  $separatorMappings = [
    "LIST_ENUMERATOR" => [
      "en" => ", ",  // English (also for fallback)
      "jb" => " e ", // Lojban
      // Add more languages here as needed
    ],
    "SPACE_SEPARATOR" => [
      "en" => " ",  // English (also for fallback)
      "jb" => " e ", // Lojban
      // Add more languages here as needed
    ],
    // Add more separator types here as needed
  ];

  $endingMappings = [
    "DOT_ENDING" => [
      "en" => ".",   // English (fallback)
      "jb" => "",    // Lojban
      // Add more languages here as needed
    ],
    "EMPTY_ENDING" => [
      "en" => ".",   // English (also for fallback)
      "jb" => "",    // Lojban
      // Add more languages here as needed
    ],
    // Add more ending types here as needed
  ];

  // Apply language-specific mappings or use English fallback
  if (isset($separatorMappings[$separators][$lang])) {
    $separators = $separatorMappings[$separators][$lang];
  } elseif (isset($separatorMappings[$separators]['en'])) {
    $separators = $separatorMappings[$separators]['en'];
  } else {
    $separators = ", ";
  }

  if (isset($endingMappings[$ending][$lang])) {
    $ending = $endingMappings[$ending][$lang];
  } elseif (isset($endingMappings[$ending]['en'])) {
    $ending = $endingMappings[$ending]['en'];
  } else {
    $ending = ".";
  }

  foreach ($translatorinfos as $translatorinfo) {
    preg_match('/(.*)\s+\<((http|mailto).*)\>/', $translatorinfo, $matches);
    if (count($matches) == 4) {
      echo '<a href="' . $matches[2] . '">'. filter_var($matches[1], FILTER_SANITIZE_FULL_SPECIAL_CHARS) .'</a>';
    } else {
      preg_match('/(.*)\s+\<(.*@.*)\>/', $translatorinfo, $matches);
      if (count($matches) == 3) {
        echo '<a href="mailto:' . $matches[2] . '">'. filter_var($matches[1], FILTER_SANITIZE_FULL_SPECIAL_CHARS) .'</a>';
      }
      else {
        echo filter_var($translatorinfo, FILTER_SANITIZE_FULL_SPECIAL_CHARS).'';
      }
    }
    $counter = $counter + 1;
    if ($counter < $number) {
      echo $separators;
    } else {
      echo $ending;
    }
  }
}
