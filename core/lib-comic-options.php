<?php if ($root=="") exit;

$hdclass = '';
$sbsclass = '';
$offclass = '';
$spacer = '';

if (in_array("hd", $option)) {
  $hdclass = 'active'; 
}
if (in_array("sbs", $option)) {
  $sbsclass = 'active';
}
if  ( $lang == "en" ) {
  $offclass = 'off';
  $spacer = ' ';
}

echo '<div class="comic-options">'."\n";
echo '  <button class="options-btn"><img width="25" height="25" src="'.$root.'/core/img/comic-options.svg" alt="options"/></button>'."\n";
echo '    <div class="options-content">'."\n";
echo '    <a class="'.$hdclass.'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content.''._newoption("hd").'.html">'._("High definition").'</a>'."\n";
echo '    <a class="'.$sbsclass.''.$spacer.''.$offclass.'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content.''._newoption("sbs").'.html">'._("Side by side with English").'</a>'."\n";
echo '    </div>'."\n";
echo '</div>'."\n";

?>
