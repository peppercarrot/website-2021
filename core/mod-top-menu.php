<?php if ($root=="") exit;

echo '<header id="header">'."\n";
echo ' <div class="container">'."\n";
echo '  <div class="grid">'."\n";
echo ''."\n";

echo '  <div class="logobox col sml-hide med-hide lrg-show lrg-2 sml-text-left">'."\n";
echo '    <a href="'.$root.'/'.$lang.'/" title="Pepper and Carrot">'."\n";
if (file_exists('po/'.$lang.'.svg')) {
  echo '      <img class="logo" src="'.$root.'/po/'.$lang.'.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
} else {
  echo '      <img class="logo" src="'.$root.'/po/en.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
}
echo '    </a>'."\n";
echo '  </div>'."\n";

echo ''."\n";

echo '  <div class="topmenu col sml-12 lrg-10 sml-text-left lrg-text-right">'."\n";
echo '    <nav class="nav">'."\n";
echo '      <div class="responsive-menu">'."\n";
echo '        <label for="menu">'."\n";
echo '          <img class="burgermenu" src="'.$root.'/core/img/menu.svg" alt=""/>'."\n";
if (file_exists('po/'.$lang.'.svg')) {
  echo '          <img class="logo" src="'.$root.'/po/'.$lang.'.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
} else {
  echo '          <img class="logo" src="'.$root.'/po/en.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
}
echo '        </label>'."\n";
echo '        <input type="checkbox" id="menu">'."\n";
echo '        <ul class="menu expanded">'."\n";
# ------------ Homepage/webcomics:
              if ($mode == 'webcomics' || $mode == 'webcomic' || $mode == 'miniFantasyTheater' || $mode == 'webcomic-misc' ) { $class = 'active'; } else { $class = 'no-active'; }
echo '          <li class="'.$class.'" >'."\n";
echo '            <a href="'.$root.'/'.$lang.'/webcomics/peppercarrot.html">'._("Webcomics").'</a>'."\n";
echo '          </li>'."\n";
# ------------ Artworks:
              if ($mode == 'artworks' || $mode == 'files' ) { $class = 'active'; } else { $class = 'no-active'; }
echo '          <li class="'.$class.'" >'."\n";
echo '            <a href="'.$root.'/'.$lang.'/artworks/artworks.html">'._("Artworks").'</a>'."\n";
echo '          </li>'."\n";
# ------------ Fan-art:
              if ($mode == 'fan-art') { $class = 'active'; } else { $class = 'no-active'; }
echo '          <li class="'.$class.'" >'."\n";
echo '            <a href="'.$root.'/'.$lang.'/fan-art/fan-art.html">'._("Fan-art").'</a>'."\n";
echo '          </li>'."\n";
# ------------ Philosophy:
              if ($mode == 'philosophy') { $class = 'active'; } else { $class = 'no-active'; }
echo '          <li class="'.$class.'" >'."\n";
echo '            <a href="'.$root.'/'.$lang.'/philosophy/index.html">'._("Philosophy").'</a>'."\n";
echo '          </li>'."\n";
# ------------ Contribute:
              if ($mode == 'contribute' || $mode == 'chat' || $mode == 'wiki' || $mode == 'documentation') { $class = 'active'; } else { $class = 'no-active'; }
echo '          <li class="'.$class.'" >'."\n";
echo '            <a href="'.$root.'/'.$lang.'/contribute/index.html">'._("Contribute").'</a>'."\n";
echo '          </li>'."\n";
# ------------ Resources:
              if ($mode == 'resources' || $mode == 'wallpapers' ) { $class = 'active'; } else { $class = 'no-active'; }
echo '          <li class="'.$class.'" >'."\n";
echo '            <a href="'.$root.'/'.$lang.'/resources/index.html">'._("Resources").'</a>'."\n";
echo '          </li>'."\n";
# ------------ Donate:
              if ($mode == 'support') { $class = 'active'; } else { $class = 'no-active'; }
echo '          <li class="'.$class.'" >'."\n";
echo '            <a href="'.$root.'/'.$lang.'/support/index.html">'._("Support").'</a>'."\n";
echo '          </li>'."\n";
# ------------ About:
              if ($mode == 'about') { $class = 'active'; } else { $class = 'no-active'; }
echo '          <li class="'.$class.'" >'."\n";
echo '            <a href="'.$root.'/'.$lang.'/about/index.html">'._("About").'</a>'."\n";
echo '          </li>'."\n";
# ------------ Shop:
echo '          <li class="external">'."\n";
echo '            <a href="https://www.davidrevoy.com/static9/shop" target="blank">'._("Shop").'  <img src="'.$root.'/core/img/external-menu.svg" alt=""/></a>'."\n";
echo '          </li>'."\n";
# ------------ Blog:
echo '          <li class="external">'."\n";
echo '            <a href="https://www.davidrevoy.com/blog" target="blank">'._("Blog").' <img src="'.$root.'/core/img/external-menu.svg" alt=""/></a>'."\n";
echo '          </li>'."\n";
// # ------------ Wiki:
//               if ($mode == 'wiki') { $class = 'active'; } else { $class = 'no-active'; }
// echo '          <li class="'.$class.'" >'."\n";
// echo '            <a href="'.$root.'/'.$lang.'/wiki/index.html">'._("Wiki").'</a>'."\n";
// echo '          </li>'."\n";
// # ------------ License:
//               if ($mode == 'license') { $class = 'active'; } else { $class = 'no-active'; }
// echo '          <li class="'.$class.'" >'."\n";
// echo '            <a href="'.$root.'/'.$lang.'/license/index.html">'._("License").'</a>'."\n";
// echo '          </li>'."\n";
# ------------
echo '        </ul>'."\n";
echo '      </div>'."\n";
echo '    </nav>'."\n";
echo '  </div>'."\n";

echo '  </div>'."\n";
echo ' </div>'."\n";
_clearboth();
echo '</header>'."\n";
echo ''."\n";

# Top banner to display a message to the full website
#echo '  <div style="color: #1e1d34; padding: 10px; background-color: #8683e8; border: 1px solid #2d4f7f; text-align: center; ">'."\n";
#echo '<b>Breaking news</b>: The website has trouble displaying the comics, it\'s reported and Carrot is working on it! Thank you for your patience. <i>~ David</i>'."\n";
#echo '</div>'."\n";
#echo ''."\n";

?>
