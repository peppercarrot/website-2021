<?php if ($root=="") exit;

$path_all_episodes = $root.'/'.$lang.'/webcomics/peppercarrot.html';


# Background color
$background_color = "#FFF";
$info = array();
$info = json_decode(file_get_contents(''.$sources.'/'.$epdirectory.'/info.json'), true);
$background_color = $info["background-color"];
$public_status = $info["public"];

# Options
$containeroption = '';
$webcomicpageoption = '';
$wrapperoption = 'style="background:'.$background_color.';"';
$gif_width = '1100px';
$gif_padding = '32px';
if (in_array("hd", $option)) {
  $containeroption = 'style="max-width: none !important;"';
  $wrapperoption = 'style="max-width: none !important; background:'.$background_color.';"';
  $gif_width = '2265px';
  $gif_padding = '22px';
}
if (in_array("sbs", $option)) {
  $containeroption = 'style="max-width: none !important;"';
  $wrapperoption = 'style="max-width: none !important; background:'.$background_color.';"';
  $webcomicpageoption = 'style="display: grid; grid-template-columns: repeat(2, 1fr); gap: 10px; justify-items: center;"';
  $gif_width = '1100px';
  $gif_padding = '32px';
}

# Episode full catalog
$episode_index = $pc_episodes_list; # → lib-database.php
sort($episode_index);

# Start display
include($file_root.'core/mod-menu-lang.php');
echo '<div class="container webcomic" '.$containeroption.'>'."\n";

# Get the comic page
$allpages = glob(''.$sources.'/'.$epdirectory.'/low-res/'.$lang.''.$credits.'E[0-9][0-9]P[0-9][0-9].*');
if (empty($allpages)) {
  $allpages = glob(''.$sources.'/'.$epdirectory.'/low-res/en'.$credits.'E[0-9][0-9]P[0-9][0-9].*');
  $fallbackmode = 1;
  $comiclang = 'en';
} else {
  $fallbackmode = 0;
  $comiclang = $lang;
}
sort($allpages);

# Transcript:
function _transcript($page) {
  global $sources;
  global $comiclang;
  global $content;

  # Extract from $page only E[0-9][0-9]P[0-9][0-9]
  $pageid = basename($page);
  preg_match('/(E[0-9][0-9]P[0-9][0-9])/', $pageid, $matches);
  $pageid = $matches[0];
  # Construct pattern of target transcript file
  $transcriptpage = ''.$sources.'/'.$content.'/hi-res/html/'.$comiclang.'_'.$pageid.'.html';

  # Check if the file exists
  if (file_exists($transcriptpage)) {
      # Display transcript html
      readfile($transcriptpage);
  }
}

_xyz_disclaimer(); # → lib-function.php
_fallback_messsage(); # → lib-function.php

# Header
echo '<div class="webcomic-bg-wrapper" '.$wrapperoption.'>'."\n";

include($file_root.'core/lib-comic-options.php');

if (!empty($allpages)) {
  $titlepage = ''.$root.'/'.$allpages[0].'';
  if (file_exists($allpages[0])) {
    echo '  <div class="webcomic-page">'."\n";
    echo '    <img class="comicpage" src="'.$titlepage.'" alt="'._("Header").'">'."\n";
    echo '  </div>'."\n";
    echo ''."\n";
    unset($allpages[0]); # Remove the header title from the array of page, to not print it twice
  }

  # Navigation
  echo ''._navigation($epdirectory, $episode_index, $mode, $path_all_episodes).'';
  _clearboth();

  # Loop on comic pages
  foreach ($allpages as $key => $page) {
    $pagepath = ''.$root.'/'.$page.'';
    if (in_array("hd", $option)) {
      $pagepath = str_replace('/low-res/', '/hi-res/', $pagepath);
    }
    if (file_exists($page)) {
      $title_alt = sprintf(_("Page %d"), $key);
      $comic_alt = $header_title.', '.$title_alt;
      echo '  <div class="webcomic-page" '.$webcomicpageoption.'>'."\n";
      # Side by side: alter path for English
      if (in_array("sbs", $option)) { 
          $enpagepath = str_replace('/'.$lang.'_', '/en_', $pagepath);
      }
      # Gif: special rule to upscale them
      if (strpos($pagepath, 'gif') == true) {
        if (in_array("sbs", $option)) {
          # Side by side: display en gif
          echo '    <img style="max-width:'.$gif_width.';padding-bottom: '.$gif_padding.';" width="92%" src="'.$enpagepath.'" alt="'.$comic_alt.' title="'.$title_alt.'" "/>'."\n";
        }
        # display gif
        echo '    <img style="max-width:'.$gif_width.';padding-bottom: '.$gif_padding.';" width="92%" src="'.$pagepath.'" alt="'.$comic_alt.' title="'.$title_alt.'" "/>'."\n";
      } else {
      # Regular comic page
        # Side by side: display en page
        if (in_array("sbs", $option)) {
          echo '    <img src="'.$enpagepath.'" alt="'.$comic_alt.'" title="'.$title_alt.'" />'."\n";
        }
        # display comic page
        echo '    <img src="'.$pagepath.'" alt="'.$comic_alt.'" title="'.$title_alt.'" />'."\n";
      }
      echo '  </div>'."\n";
    }
  }
}

echo '</div>'."\n";

# Footer navigation
echo ''._navigation($epdirectory, $episode_index, $mode, $path_all_episodes).'';
echo ''."\n";
_clearboth();

# Transcript button
$allpages = array();
$allpages = glob(''.$sources.'/'.$epdirectory.'/low-res/en'.$credits.'E[0-9][0-9]P[0-9][0-9].*');
echo '<details>'."\n";
echo '<summary class="webcomic-details">'._("Transcript").'</summary>'."\n";
foreach ($allpages as $key => $page) {
  $page_number = sprintf(_("Page %d"), $key);
  if ($key != 0 ){
    echo '      <h2>'.$page_number.'</h2>'."\n";
  }
  echo '      '._transcript($page).''."\n";
}
echo '</details>'."\n";


# Footer support
echo '<div class="webcomic-footer-box">'."\n";
echo '    <h3>'._("Support my free(libre) and open-source webcomics on:").'</h3>'."\n";
_display_support_links("thumbnails","100","50","");
echo '</div>'."\n";

# Footer sources
echo '<div class="webcomic-footer-box">'."\n";
echo '    <h3 id="sources">'._("Source files:").'</h3>'."\n";
$artwork_sources = $sources.'/'.$epdirectory.'/zip/'.$epdirectory.'_art-pack.zip';
if (file_exists($artwork_sources)){
  echo '      '._("Artworks:").''."\n";
  $filename = basename($artwork_sources);
  $fileweight = (filesize($artwork_sources) / 1024) / 1024;
  echo '      <a href="'.$root.'/'.$artwork_sources.'">'.$filename.' <span class="small-info">(Krita KRA, '.round($fileweight, 2).'MB )</span></a>'."\n";
}
echo '    <br>'."\n";
$lang_sources = $sources.'/'.$epdirectory.'/zip/'.$epdirectory.'_lang-pack.zip';
if (file_exists($lang_sources)){
  echo '      '._("Speechbubbles:").''."\n";
  $filename = basename($lang_sources);
  $fileweight = (filesize($lang_sources) / 1024) / 1024;
  echo '      <a href="'.$root.'/'.$lang_sources.'">'.$filename.' <span class="small-info">(Inkscape SVG, '.round($fileweight, 2).'MB)</span></a>'."\n";
}
echo '    <br>'."\n";
$export_dir = $root.'/'.$sources.'/'.$epdirectory.'/hi-res/';
echo '      <a href="'.$export_dir.'">'._("Exports for printing").' <span class="small-info">(JPG, PNG, PDF...)</span></a>'."\n";
echo '    <br>'."\n";
$git_repository = "https://framagit.org/peppercarrot/webcomics";
echo '      <a href="'.$git_repository.'">'._("Git repository").'</a>'."\n";
echo '      <a class="btn btn-sources" href="'.$root.'/'.$lang.'/webcomic-sources/'.$epdirectory.'.html">'._("Show all the sources").'</a>'."\n";
echo '</div>'."\n";

# Related URLs
_list_related_url(''.$sources.'/'.$epdirectory.'/info.json');

# end
echo ''."\n";
echo '</div>'."\n";
echo ''."\n";
?>
