<?php if ($root=="") exit; 

# Initiate page
echo '<div class="container">'."\n";

# Include the webcomic sources 
include($file_root.'core/mod-menu-lang.php');

# Sidebar
# -------
echo '  <aside class="col sml-12 med-12 lrg-2">'."\n";
echo '    <div class="sidebar-img offset-sml">'."\n";

# Array of directories available
$sources_directories = glob($sources.'/0ther/*');
sort($sources_directories);
$dirclass = '';

_sidebar_img_btn("artworks", "artworks");
_sidebar_img_btn("artworks", "sketchbook");
_sidebar_img_btn("artworks", "framasoft");
_sidebar_img_btn("artworks", "misc");
_sidebar_img_btn("files", "files");

echo '    </div>'."\n";
echo '  </aside>'."\n";
echo ''."\n";

# Gallery thumbnails
# ------------------
echo '  <article class="col sml-12 med-12 lrg-10">'."\n";

_header($content, "gallery");

echo '  </article>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
