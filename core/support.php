<?php if ($root=="") exit;

echo '<div class="container">';
include($file_root.'core/mod-menu-lang.php');
echo '</div>';

echo '<div class="container container-sml">'."\n";
echo '  <section class="page">'."\n";

echo '      <h1>'._("Support").'</h1> '."\n";

echo '      <h2>'._("Why Your Support Is Important").'</h2> '."\n";

echo '        <p>'._("For over a decade, I've been sharing webcomics, artwork, tutorials, and resources online for free under free/libre licenses.")."\n";
echo '        '._("That's because I follow the free/libre and open-source software ethic and refuse to rely on copyrights, paywalls, advs, and proprietary software.")."\n";
# Note to translators: The %s placeholder is an opening <a> link tag.
echo '        '.sprintf(_("You can read more about my philosophy %s here</a>."), '<a href="'.$root.'/'.$lang.'/philosophy/index.html">')."</p>\n";

echo '        <p>'._("However, creating and sharing everything this way doesn't quite pay the bills. That's why I need to rely on your donations. All this free content I've been able to produce over the years has been made possible largely thanks to the contributions from a small but dedicated percentage of supporters, whose generosity benefits to all other readers.").'</p>'."\n";

echo '        <p>'._("If you're able to join them, I would truly appreciate your support. My income depends on these contributions, and your donation will help me continue creating in the best possible conditions.").'</p>'."\n";

echo '        <span style="float:right;">− '._("David Revoy").'</span><br>'."\n";

$illustration_A_alt_text=_("Pepper and Carrot are shopping for groceries, carrying bags of fruit and looking happy.");
echo '<img class="borderless" src="'.$root.'/core/img/support_pepper-and-carrot-groceries.png" alt="'.$illustration_A_alt_text.'" title="'.$illustration_A_alt_text.'">';

echo '      <h2>'._("The Various Ways to Support").'</h2> '."\n";

echo '        '._("You might be wondering why there are so many options to support my work. Over the years, I've signed up for various platforms designed to support artists. I did this primarily because potential new donors requested that I create profiles on their favorite platforms. Additionally, I believe that having multiple channels is a more robust solution in case one platform unexpectedly closes or encounters issues.")."\n";

echo '<div class="frame">'."\n";
echo '  <a href="https://liberapay.com/davidrevoy/">'."\n";
echo '    <img src="'.$root.'/core/img/patronage_liberapay_sq.svg" alt="Liberapay logo" title="Liberapay">'."\n";
echo '  </a>'."\n";
echo '  <h3>'._("Liberapay").'</h3> '."\n";
echo '  '._("A service designed specifically for free and libre projects, Liberapay has no transaction fees, and it allows you to support me on a monthly basis. This is my favorite option.")."\n";
echo '  <a href="https://liberapay.com/davidrevoy/">'."\n";
echo '    → liberapay.com/davidrevoy/'."\n";
echo '  </a>'."\n";
echo '</div>'."\n";

echo '<div class="frame">'."\n";
echo '  <a href="https://www.patreon.com/davidrevoy">'."\n";
echo '    <img src="'.$root.'/core/img/patronage_patreon_sq.svg" alt="Patreon logo" title="Patreon">'."\n";
echo '  </a>'."\n";
echo '  <h3>'._("Patreon").'</h3> '."\n";
# &#37; stands for the % symbol.
echo '  '._("A popular U.S.-based patronage service, Patreon takes a 5&#37; fee for its platform. My profile is currently set up to receive your support only when I release a new episode of Pepper&Carrot (which takes me more than three months these days). However, the platform has announced that this option will be removed in November 2025, leaving only the monthly subscription support option. Everyone will be contacted before I make the change so you can adjust.")."\n";
echo '  <a href="https://www.patreon.com/davidrevoy">'."\n";
echo '    → patreon.com/davidrevoy'."\n";
echo '  </a>'."\n";
echo '</div>'."\n";

echo '<div class="frame">'."\n";
echo '  <a href="https://www.tipeee.com/pepper-carrot">'."\n";
echo '    <img src="'.$root.'/core/img/patronage_tipeee_sq.svg" alt="Tipeee logo" title="Tipeee">'."\n";
echo '  </a>'."\n";
echo '  <h3>'._("Tipeee").'</h3> '."\n";
# &#37; stands for the % symbol.
echo '  '._("Tipeee is a European alternative to Patreon, also with a 5&#37; fee going to the platform. For a long time, it was the only platform that was able to process payments in Euro and offered more translations (Français, Deutsch, Italiano, and Español). I will only receive your support on this service when I post a new episode of Pepper&Carrot, but I may switch to a monthly basis when Patreon removes the \"per creation\" option in November 2025.")."\n";
echo '  <a href="https://www.tipeee.com/pepper-carrot">'."\n";
echo '    → tipeee.com/pepper-carrot'."\n";
echo '  </a>'."\n";
echo '</div>'."\n";


echo '      <h2>'._("Direct donations").'</h2> '."\n";

echo '<div class="frame">'."\n";
echo '  <a href="https://paypal.me/davidrevoy">'."\n";
echo '    <img src="'.$root.'/core/img/patronage_paypal_sq.svg" alt="Paypal logo" title="Paypal">'."\n";
echo '  </a>'."\n";
echo '  <h3>'._("Paypal").'</h3> '."\n";
echo '  '._("A well-known service that facilitates secure online money transfers in many currencies and is available in most countries.")."\n";
echo '  <a href="https://paypal.me/davidrevoy">'."\n";
echo '    → paypal.me/davidrevoy'."\n";
echo '  </a>'."\n";
echo '</div>'."\n";


echo '<div class="frame" id="iban">'."\n";
echo '    <img src="'.$root.'/core/img/patronage_iban_sq.svg" alt="Iban" title="IBAN">'."\n";
echo '  <h3>'._("IBAN, Wire Transfer:").'</h3> '."\n";
echo '  '._("This option is free within Europe, and you can choose to make a one-time or recurring payment directly from your bank account. It's also protected by bank secrecy. My account is located in France, so please check your bank's documentation for any fees if you're outside of Europe.").''."\n";
echo '<pre>'."\n";
echo '<strong>IBAN:</strong> FR6230002040630000090557K69'."\n";
echo '<strong> RIB:</strong> 30002040630000090557K69'."\n";
echo '<strong> BIC:</strong> CRLYFRPP'."\n";
echo '</pre>'."\n";
echo '</div>'."\n";

echo '<div class="frame">'."\n";
echo '  <h3>'._("Mail address").'</h3> '."\n";
echo '  '._("If you'd like to surprise me, nothing beats a postcard, handwritten letter, or a box. I'm not sharing my home address due to past online threats and unwanted visitors. Instead, I'm using a PO box in a dental office. I have a notification when something is deposited on it and it works well.")."\n";
echo '<br>';
echo '  '._("Here's the address:")."\n";
echo '<pre>'."\n";
echo 'Cabinet Dentaire (pour David REVOY)'."\n";
echo '135 Avenue des Mourets'."\n";
echo '82000 MONTAUBAN'."\n";
echo '<strong>FRANCE</strong>'."\n";
echo '</pre>'."\n";
echo '</div>'."\n";

echo '      <h2>'._("Contact").'</h2> '."\n";

echo ' '._("If you have any question, I'm around. You can send me a PM on social media (links in the footer) or send me an email to:")."\n";
echo '        <a href="mailto:info@davidrevoy.com">info@davidrevoy.com</a><br> '."\n";
echo '<br>';
echo ' <div style="text-align:center;font-weight: 700;">'._("Thank you!").'</div>'."\n";

$illustration_B_alt_text=_("Pepper and Carrot, sleeping relaxed on the grass.");
echo '<img class="borderless" src="'.$root.'/core/img/support_merci.png" alt="'.$illustration_B_alt_text.'" title="'.$illustration_B_alt_text.'">';

echo '<br><br>';

echo '  </section>'."\n";
echo '</div>'."\n";
echo ''."\n";

?>
