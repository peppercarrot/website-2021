<?php if ($root=="") exit;


# Include the language selection menu and credit engine
include($file_root.'core/mod-menu-lang.php');

echo '<div class="container container-sml">'."\n";
echo '  <article class="page">'."\n";

_img($root.'/'.$sources.'/0ther/misc/low-res/2016-03-08_fairy-on-rpg-dice-20_by-David-Revoy.jpg', _("Photo of the author, David Revoy with his cat named Noutti while drawing the first episodes of Pepper&Carrot."), 1200, 400, 82);

# Display the page
$file = "TERMS-OF-SERVICE-AND-PRIVACY.md";
$contents = file_get_contents($file);
$Parsedown = new Parsedown();
echo $Parsedown->text($contents);

echo '  </article>'."\n";
echo '</div>'."\n";
echo ''."\n";

?>
