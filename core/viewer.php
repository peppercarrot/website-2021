<?php if ($root=="") exit;

# $pattern , $attribution and $title are defined in index.php at root
# This way, the title of the html page can be also defined and a
# thumbnail of the viewer can be generated for social-medias.

# Main container black, overlay even top-bar.
echo '  <div id="viewer">'."\n";

# Extract the date
$picture_date = str_replace('_by-David-Revoy', '', $option_get);
$picture_date = substr($picture_date, 0, 10);

# Build an array with all pictures from the folder for navigation next/prev buttons.
$all_pictures = glob($sources.'/0ther/'.$content.'/low-res/*.jpg');
sort($all_pictures);
$picture_index = array();
foreach ($all_pictures as $key => $picture) {
  $picture = basename($picture);
  $picture = str_replace('.jpg', '', $picture);
  array_push($picture_index, $picture);
}
sort($picture_index);

# Guess where the high resolution path
# pattern by default
$picture_hires = $sources.'/0ther/'.$content.'/hi-res/'.$option_get.'.jpg';
if(!file_exists($picture_hires)){
  # if default not found, check if hi-res is a png
  $picture_hires = $sources.'/0ther/'.$content.'/hi-res/'.$option_get.'.png';
  if(!file_exists($picture_hires)){
    # if png not found, try png without attribution.
    $picture_hires = str_replace('_by-David-Revoy', '', $picture_hires);
  }
}
# Extract dimension and size on disk
if(file_exists($picture_hires)){
  $image_size = getimagesize($picture_hires);
  $fileweight = '('.round((filesize($picture_hires) / 1024) / 1024, 2).'MB)';
}

if ($content == "artworks" OR $content == "sketchbook" OR $content == "misc" OR $content == "framasoft" OR $content == "fediverse") {
  $previous_gallery = "artworks";
} else if ($content == "fan-art" OR $content == "cosplay") {
  $previous_gallery = "fan-art";
} else {
  $previous_gallery = "files";
}

# Viewer mode
# -----------

  # Container for the header (max-width to group center the buttons)
  echo '<div class="viewheader">'."\n";

  # Button to toggle switch Source and License
  echo '  <a class="viewdownload'.$source_button_class.'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content_for_url.'__'.$option_get.'.html">'."\n";
  echo '    '._("source and license").''."\n";
  echo '  </a>'."\n";


  # Close button
  $path_all_thumbnails = $root.'/'.$lang.'/'.$previous_gallery.'/'.$content.'.html';
  echo '  <a class="viewcloseallbutton" title="'._("Return to the gallery of thumbnails.").'" href="'.$path_all_thumbnails.'">'."\n";
  echo '    <img alt="X" width="24" height="24" src="'.$root.'/core/img/close.svg"/>';
  echo '  </a>'."\n";

  echo ''._navigation($option_get, $picture_index, $mode, $path_all_thumbnails).'';

  echo '  </div>'."\n";

  _clearboth();

  # Container for both image and sources panel
  echo '<div class="viewimagecontainer">'."\n";

  # Image (big)
  ;
  # %1$s = artwork title, %2$s = author
  $picture_description = sprintf(_('%1$s by %2$s'), $title, $attribution);
  echo '  <a href="'.$root.'/'.$picture_hires.'">'."\n";
  echo '    <img title="'.$picture_description.'" src="'.$root.'/'.$pattern.'"/>';
  echo '  </a>'."\n";

  # Image footer
  # ------------
  echo '  <div class="ViewFooterInfo">'."\n";

  # Prepare
  $license_type = "cc-by";
  $current_media_URL = $root.'/'.$lang.'/'.$mode.'/'.$content.'__'.$option_get.'.html';
  if ( $content == 'framasoft' ){
    $attribution = $attribution.', <a href="https://framasoft.org">framasoft.org</a>';
  }
  if ( $content == 'fan-art' ){
    $license_type = "fan-art";
  }
  # Test if we have a customized license first.
  # It's a Json file with the same name as the picture containing:
  # license: A title for the license
  # url: the link of the license
  # text: A part (html) that will be displayed: useful for attributions, or more links.
  $JsonFileName = str_replace($attribution_part[1],'',$option_get);
  if ( $content == 'fan-art' ){
    # Exception for the look-up of the JSON file in Fan-art: 
    # the Attribution 'by_author' in the filename is part of the source too, so don't remove it.
    $JsonFileName = $option_get;
  }
  $JsonLicense = $sources.'/0ther/'.$content.'/'.$JsonFileName.'.json';
  if(file_exists($JsonLicense)){
    # Found. Load the Json and load the new variable to display a customized license.
    $license_type = "custom";
    $custom_license_info = array();
    $custom_license_info = json_decode(file_get_contents(''.$sources.'/0ther/'.$content.'/'.$JsonFileName.'.json'), true);
    $attribution = $custom_license_info['attribution'];
    $license_type = $custom_license_info['license'];
  }

  # %1$s = artwork title, %2$s = author
  $title_author = sprintf(_('%1$s by %2$s'), '<a href="'.$current_media_URL.'">"'.$title.'"</a>', $attribution)."\n";
  $title_author_markdown = sprintf(_('%1$s by %2$s'), '['.$title.']('.$current_media_URL.')', $attribution)."\n";
  # A variation into Markdown format for the 'Source panel'

  switch ($license_type) {
    case "cc-by":
      $license = $title_author;
      $license .= '− <a href="https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).'">CC-BY 4.0</a>';
      $license_markdown = $title_author_markdown;
      $license_markdown .= ' − [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).')';
      break;
    case "fan-art":
      $license = ''.$title_author.' - '._("Created on:").' '.$picture_date.'<div class="ViewFooterDisclaimer">';
      $license .= sprintf(_("This picture is fan-art made by %s. It is reposted on the fan-art gallery of Pepper&Carrot with permission."),"<strong>".$attribution."</strong>").' <br/>';
      $license .= '<strong>'._("Do not reuse this picture for your project unless you obtain author's permissions.").'</strong></div>';
      $license_markdown = '';
      break;
    case "cc-by-sa":
      $license = $title_author;
      $license .= '− <a href="https://creativecommons.org/licenses/by-sa/4.0/'.sprintf(_("deed.%s"), $lang).'">CC-BY-SA 4.0</a>';
      $license_markdown = $title_author_markdown;
      $license_markdown .= ' − [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/'.sprintf(_("deed.%s"), $lang).')';
      break;
    case "cc-by-nc-sa":
      $license = $title_author;
      $license .= '− <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/'.sprintf(_("deed.%s"), $lang).'">CC-BY-NC-SA 4.0</a>';
      $license_markdown = $title_author_markdown;
      $license_markdown .= ' − [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/'.sprintf(_("deed.%s"), $lang).')';
      break;
    default:
      $license = $title_author;
      $license .= '− <a href="https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).'">CC-BY 4.0</a>';
      $license_markdown = $title_author_markdown;
      $license_markdown .= ' − [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).')';
  }

  # Display:
  echo $license;
  echo '  </div>'."\n";

  echo '  <div class="ViewFooterLinks">'."\n";

  # Download: don't propose download options for fan-arts.
  if ( $content != 'fan-art' ){
    echo '  '._("Created on:").' '.$picture_date.'';
    if(file_exists($picture_hires)){
      echo ' − ';
      $image_size = getimagesize($picture_hires);
      $fileweight = ','.round((filesize($picture_hires) / 1024) / 1024, 2).'MB';
      echo '  <a href="'.$root.'/'.$picture_hires.'">'."\n";
      echo '  '._("Download the high resolution picture").', '.$image_size[0].'x'.$image_size[1].'px', $fileweight.'';
      echo '  </a> − '."\n";
    }
    $source_zip_file = $sources.'/0ther/'.$content.'/zip/'.$option_get.'.zip';
    if(file_exists($source_zip_file)){
      $fileweight = ','.round((filesize($source_zip_file) / 1024) / 1024, 2).'MB';
      echo '  <a href="'.$root.'/'.$source_zip_file.'">'."\n";
      echo '  '._("Download the source file (zip)").$fileweight.'';
      echo '  </a>'."\n";
    }
  }
  echo '  </div>'."\n";



  if ($viewer_mode == 'show-sources') {
    # Sources panel:
    # --------------
    # Container
    echo '  <div id="viewsrccontainer">'."\n";

    # Close button
    echo '  <a class="viewsrcclosebutton" title="'._("Return to the gallery of thumbnails.").'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content_for_url.'__'.$option_get.'.html">'."\n";
    echo '    <img alt="X" width="20" height="20" src="'.$root.'/core/img/close.svg"/>';
    echo '  </a><br/><br/>'."\n";

    # Thumbnail
    echo '<div class="viewsrcthumb">'."\n";
    _img(''.$root.'/'.$pattern, $picture_description, 560, 250, 87);
    echo '</div>'."\n";
    echo '  <br/>';

    # Image infos
    # Make a better category label
    $category = $content;
    # Check if we have a i18n label for it
    if(isset($content_info[$content][0])) {
      $category = $content_info[$content][0];
    }
    echo $license.'<br/>';
    echo '  <br/>';

    echo '  '._("Original size:").' '.$image_size[0].'x'.$image_size[1].'px <br/>';
    echo '  '._("Created on:").' '.$picture_date.'<br/>';
    echo '  '._("Category:").' '.$category.'<br/>';

    if ( $content != 'fan-art' ){
      echo '  <br/>';
      $html_ready_copypaste = str_replace(array("\n", "\r\n"), ' ', htmlentities($license));
      $md_ready_copypaste = str_replace(array("\n", "\r\n"), ' ', htmlentities($license_markdown));
      echo 'Html: <input type="text" value="'.$html_ready_copypaste.'" style="display: inline-block; width:70%;">'."\n";
      echo '  <br/>';
      echo 'Md: <input type="text" value="'.$md_ready_copypaste.'" style="display: inline-block; width:70%;">'."\n";
      echo '  <br/>';
    }

    echo '  <br/>';

    # Links to files
    if(file_exists($picture_hires)){
      $image_size = getimagesize($picture_hires);
      $fileweight = ''.round((filesize($picture_hires) / 1024) / 1024, 2).'MB';
      echo '  <a class="viewsrcdownloadbutton" style="width:80%" href="'.$root.'/'.$picture_hires.'">'."\n";
      echo '  '._("Download the high resolution picture").', '.$fileweight.'';
      echo '  </a><br/>'."\n";
    }
    $source_zip_file = $sources.'/0ther/'.$content.'/zip/'.$option_get.'.zip';
    if(file_exists($source_zip_file)){
      $fileweight = ', '.round((filesize($source_zip_file) / 1024) / 1024, 2).'MB';
      echo '  <a class="viewsrcdownloadbutton" style="width:80%" href="'.$root.'/'.$source_zip_file.'">'."\n";
      echo '  '._("Download the source file (zip)").$fileweight.'';
      echo '  </a>'."\n";
    }

    # License
    echo '  <br/>';
    echo '  <br/>';
    echo '<div class="viewsrclicensebox">';
    echo '<strong>'._("License:").'</strong><br/>';

    $goodpracticelink = ''.$root.'/'.$lang.'/documentation/120_License_best_practices.html';

    switch ($license_type) {
    case "cc-by":
      $license_content = '<a href="https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).'"><img alt="cc-by" src="'.$root.'/core/img/cc-by.jpg" style="margin-top: 10px;"/></a>';
      $license_content .= '<br/>'._("Attribution to").' <strong>'.$attribution.'</strong>';
      $license_content .= '<br/><br/>';
      $license_content .= ''.sprintf(_("More information and good practice for attribution can be found <a href=\"%s\">on the documentation</a>."),$goodpracticelink).'<br/>';
      break;
    case "fan-art":
      $license_content = '<img alt="?" src="'.$root.'/core/img/nolicense.jpg" style="margin-top: 10px;"/><br/>'.'';
      $license_content .= sprintf(_("This picture is fan-art made by %s. It is reposted on the fan-art gallery of Pepper&Carrot with permission."),"<strong>".$attribution."</strong>").'';
      $license_content .= '<br/>'._("Do not reuse this picture for your project unless you obtain author's permissions.").'';
      break;
    case "cc-by-sa":
      $license_content = '<a href="https://creativecommons.org/licenses/by-sa/4.0/'.sprintf(_("deed.%s"), $lang).'"><img alt="cc-by-sa" src="'.$root.'/core/img/cc-by-sa.jpg" style="margin-top: 10px;"/></a>';
      $license_content .= '<br/>'._("Attribution to").' <strong>'.$attribution.'</strong>';
      $license_content .= '<br/><br/>';
      $license_content .= ''.sprintf(_("More information and good practice for attribution can be found <a href=\"%s\">on the documentation</a>."),$goodpracticelink).'<br/>';
      break;
    case "cc-by-nc-sa":
      $license_content = '<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/'.sprintf(_("deed.%s"), $lang).'"><img alt="cc-by-nc-sa" src="'.$root.'/core/img/cc-by-nc-sa.jpg" style="margin-top: 10px;"/></a>';
      $license_content .= '<br/>'._("Attribution to").' <strong>'.$attribution.'</strong>';
      $license_content .= '<br/><br/>';
      $license_content .= ''.sprintf(_("More information and good practice for attribution can be found <a href=\"%s\">on the documentation</a>."),$goodpracticelink).'<br/>';
      break;
    default:
      $license_content = '<a href="https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).'"><img alt="cc-by" src="'.$root.'/core/img/cc-by.jpg" style="margin-top: 10px;"/></a>';
      # Usually seen as "Attribution to David Revoy"
      $license_content .= '<br/>'._("Attribution to").' <strong>'.$attribution.'</strong>';
      $license_content .= '<br/><br/>';
      $license_content .= ''.sprintf(_("More information and good practice for attribution can be found <a href=\"%s\">on the documentation</a>."),$goodpracticelink).'<br/>';
  }

  # Display:
  echo $license_content;

    echo '</div>';
    echo '</div>';
  }

echo '  </div>'."\n";
echo '  </div>'."\n";
?>
