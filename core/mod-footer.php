<?php if ($root=="") exit; 


function _social_buttons(){
global $root;
echo '  '._("Follow the author on:").''."\n";
echo '  <div class="social">'."\n";
echo '    <a rel="me" href="https://framapiaf.org/@davidrevoy" target="_blank" title="Mastodon">'."\n";
echo '      <img width="40" height="40" src="'.$root.'/core/img/s_masto.svg" alt="Masto"/>'."\n";
echo '    </a>'."\n";
echo '    <a rel="me" href="https://bsky.app/profile/davidrevoy.com" target="_blank" title="Bluesky">'."\n";
echo '      <img width="40" height="40" src="'.$root.'/core/img/s_bs.svg" alt="Masto"/>'."\n";
echo '    </a>'."\n";
echo '    <a href="https://www.facebook.com/pages/Pepper-Carrot/307677876068903" target="_blank" title="Facebook">'."\n";
echo '      <img width="40" height="40" src="'.$root.'/core/img/s_fb.svg" alt="Fbook"/>'."\n";
echo '    </a>'."\n";
echo '    <a href="https://www.instagram.com/deevadrevoy/" target="_blank" title="Instagram">'."\n";
echo '      <img width="40" height="40" src="'.$root.'/core/img/s_insta.svg" alt="Insta"/>'."\n";
echo '    </a>'."\n";
echo '    <a href="https://www.youtube.com/@DavidRevoy" target="_blank" title="Youtube">'."\n";
echo '      <img width="40" height="40" src="'.$root.'/core/img/s_yt.svg" alt="Ytb"/>'."\n";
echo '    </a>'."\n";
echo '    <a href="https://www.davidrevoy.com/feed/en/rss" target="_blank" title="RSS (blog posts)">'."\n";
echo '      <img width="40" height="40" src="'.$root.'/core/img/s_rss.svg" alt="RSS"/>'."\n";
echo '    </a>'."\n";
echo '    <br/>'."\n";
echo '    <br/>'."\n";
echo '    <img width="20" height="20" src="'.$root.'/core/img/ico-email.svg" alt="Mail icon"/>Email: <a href="mailto:info@davidrevoy.com">info@davidrevoy.com</a>'."\n";
echo '    <br/>'."\n";
echo '    <br/>'."\n";
echo '  '._("Join community chat rooms:").'<br/>'."\n";
echo '    <a href="https://libera.chat/">'."\n";
echo '       '._("IRC: #pepper&carrot on libera.chat").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://matrix.to/#/%23peppercarrot:matrix.org">'."\n";
echo '       Matrix'."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://telegram.me/+V76Ep1RKLaw5ZTc0">'."\n";
echo '       Telegram'."\n";
echo '    </a><br/>'."\n";
echo '    <br/>'."\n";
echo '  </div>'."\n";
}


_clearboth();

echo '<footer id="footer">';

echo '  <div class="container">';

# To get social medias button on top in one column, responsive.
echo '  <div class="col sml-12 med-hide">'."\n";
_social_buttons();
echo '    <br/>'."\n";
echo '  </div>'."\n";

echo '  <div class="col sml-12 med-4">'."\n";
echo '    <a href="'.$root.'/'.$lang.'/">'."\n";
echo '       '._("Homepage").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/webcomics/index.html">'."\n";
echo '       '._("Webcomics").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/artworks/artworks.html">'."\n";
echo '       '._("Artworks").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/fan-art/fan-art.html">'."\n";
echo '       '._("Fan-art").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/philosophy/index.html">'."\n";
echo '       '._("Philosophy").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/resources/index.html">'."\n";
echo '       '._("Resources").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/contribute/index.html">'."\n";
echo '       '._("Contribute").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://www.davidrevoy.com/static9/shop">'."\n";
echo '       '._("Shop").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://www.davidrevoy.com/blog">'."\n";
echo '       '._("Blog").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/about/index.html">'."\n";
echo '       '._("About").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/license/index.html">'."\n";
echo '       '._("License").''."\n";
echo '  </div>'."\n";

echo '  <div class="col sml-12 med-4">'."\n";
echo ''."\n";
echo '    <a rel="me" href="https://framagit.org/peppercarrot">'."\n";
echo '       Framagit'."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/wiki/">'."\n";
echo '       '._("Wiki").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://www.davidrevoy.com/tag/making-of">'."\n";
echo '       '._("Making-of").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://www.davidrevoy.com/tag/brush">'."\n";
echo '       '._("Brushes").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/wallpapers/index.html">'."\n";
echo '       '._("Wallpapers").''."\n";
echo '    </a><br/>'."\n";
echo '    <br/>'."\n";
_display_support_links("","120","60","<br>");

echo '    </a><br/><br/>'."\n";


echo '    <a href="https://weblate.framasoft.org/projects/peppercarrot/website/">'."\n";
echo '       '._("Translate the website on Framasoft's Weblate").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/tos/index.html">'."\n";
echo '       '._("Terms of Services and Privacy").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/documentation/409_Code_of_Conduct.html">'."\n";
echo '       '._("Code of Conduct").''."\n";
echo '    </a><br/>'."\n";
echo '  </div>'."\n";

echo '  <div class="col sml-6 med-4 sml-hide med-show">'."\n";
_social_buttons();
echo '  </div>'."\n";
echo '  <div style="clear:both;"></div>'."\n";

echo '  </div>'."\n";

echo '</footer>';

echo '</body>'."\n";
echo '</html>'."\n";
?>
