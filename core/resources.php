<?php if ($root=="") exit; 

# Include the webcomic sources 
include($file_root.'core/mod-menu-lang.php'
);

echo '<div class="container container-med">'."\n";
echo ' <main class="card-container">'."\n";

# _menu_card_item(
# - Title
# - Img link
# - Description
# - Link
# - button label

_menu_card_item(
  _("Free Tutorials and Making-of"), 
  '/0ther/website/hi-res/2021-07-14_goodies-D-tutorial-makingof_by-David-Revoy.jpg', 
  _("I love sharing my knowledge! I create video tutorials, articles, and behind-the-scenes content during production. You can find everything posted on my blog and video channels."), 
  'https://www.davidrevoy.com/categorie3/tutorials-brushes-extras',
  _("All Tutorials and Making-Of on the Blog") 
);
_menu_card_item(
  _("Free Brushes and Resources"), 
  '/0ther/website/hi-res/2021-07-14_goodies-B-brush_by-David-Revoy.jpg', 
  _(" I contribute to the free and open-source digital painting software I love by creating and sharing new brushes, textures, and themes with the community."), 
  'https://www.davidrevoy.com/tag/brush',
  _("View All Brushes and Resources on the Blog") 
);
_menu_card_item(
  _("Free Avatar Generators"), 
  '/0ther/website/hi-res/2021-07-14_goodies-C-avatar-generator_by-David-Revoy.jpg', 
  _("It all started as a solution to a privacy issue for my blog, and then I began creating avatar generators. Feel free to use them for your avatars!"), 
  'https://www.peppercarrot.com/extras/html/2016_cat-generator/',
  _("Launch the Avatar Generators") 
);
_menu_card_item(
  _("Free Wallpapers"), 
  '/0ther/website/hi-res/2021-07-14_goodies-A-wallpapers_by-David-Revoy.jpg', 
  _("While you can easily find high-resolution images throughout the website's galleries, I wanted to provide a curated list of wallpapers specifically designed for optimal use."), 
  $root.'/'.$lang.'/wallpapers/index.html',
  _("Visit the Wallpaper Gallery") 
);
_menu_card_item(
  _("Free Install Guides for Linux and Tablets"), 
  '/0ther/website/hi-res/2021-07-14_goodies-E-linux-install-guide_by-David-Revoy.jpg', 
  _("Creating professional artwork using only Free/Libre and Open Source software can be challenging, often requiring advanced computer knowledge to set up a workstation correctly. To bridge this gap, I publish comprehensive installation guides on my blog, along with hardware tests."), 
  'https://www.davidrevoy.com/index.php?tag/linux',
  _("View All Guides on the Blog") 
);
_menu_card_item(
  _("Free Pepper&Carrot Fonts"), 
  '/0ther/website/hi-res/2021-07-14_goodies-F-fonts_by-David-Revoy.jpg', 
  _("The Pepper&Carrot team, including translators and proofreaders, exclusively uses libre fonts. Unfortunately, there weren't many available — especially for various languages. To address this, we started creating, collecting, and maintaining a repository of fonts for the project. Today, many of our fonts are advanced and high quality, and I'm excited to share them with you!"), 
  'https://framagit.org/peppercarrot/fonts',
  _("Visit the Fonts repository")
);

echo '  </main>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
