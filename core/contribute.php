<?php if ($root=="") exit;

# Include the webcomic sources 
include($file_root.'core/mod-menu-lang.php');

# Chat:
echo '<div class="container container-sml">'."\n";
  $ccbylink = 'https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang);
  $chatroom = $root.'/'.$lang.'/chat/index.html';
  echo '<div class="contribute-top">'."\n";
  echo '  <img src="'.$root.'/core/img/contribute-chat.svg"/ alt="A speechbubble">'."\n";
  echo ''.sprintf(_("Thanks to the <a href=\"%s\">Creative Commons license</a>, you can contribute Pepper&Carrot in many ways."), $ccbylink).''."\n";
  echo ''.sprintf(_("Join the <a href=\"%s\">Pepper&Carrot chat room</a> to share your projects!"),$chatroom).'';
  echo '<br/><small>'._("(Note: English language is used across our documentations, wiki and channels.)").'</small>'."\n";
  echo '</div>'."\n";
  echo '<br/>'."\n";
echo '</div>'."\n";

echo '<div class="container container-med">'."\n";
echo ' <main class="card-container">'."\n";

# _menu_card_item(
# - Title
# - Img link
# - Description
# - Link
# - button label


_menu_card_item(
  _("The Pepper&Carrot Wiki"),
  '/0ther/website/hi-res/2021-07-14_contribute-E-wiki_by-David-Revoy.jpg',
  _("Join us in developing the wiki that houses additional information about the lore, characters, and everything needed to bring the world of Hereva to life. This wiki serves as a central resource for coordinating adaptations and expansions of the lore."),
  $root.'/'.$lang.'/wiki/index.html',
  _("Visit the Wiki")
  );

_menu_card_item(
  _("Your Derivations"),
  '/0ther/website/hi-res/2021-07-14_contribute-A-derivation_by-David-Revoy.jpg',
  _("Have you created an exciting derivation of the webcomic, such as a movie, video game, novel, or something else? I'd love to showcase your Pepper&Carrot projects on the blog!"),
  'https://www.davidrevoy.com/tag/derivation/',
  _("Explore All Derivations on the Blog")
  );

_menu_card_item(
  _("Translations and Corrections"),
  '/0ther/website/hi-res/2021-07-14_contribute-C-translation_by-David-Revoy.jpg',
  _("The Pepper&Carrot website and the comics hosted on it are designed to be multilingual and support various languages. All the resources and comprehensive documentation are available to help you start translating or making corrections."),
  $root.'/'.$lang.'/documentation/010_Translate_the_comic.html',
  _("Access the Documentation")
  );

_menu_card_item(
  _("Fan-art"),
  '/0ther/website/hi-res/2021-07-14_contribute-B-fan-art_by-David-Revoy.jpg',
  _("I would love to see your fan art! Drawings, 3D models, sculptures, fan comics, cosplays, scenarios, fan fiction, and more are all welcome here. The website even features a galleries to showcase your creations."),
  $root.'/'.$lang.'/fan-art/fan-art.html',
  _("Contribute to the Fan Art Galleries")
  );

_menu_card_item(
  _("Press, blog and Social Media"),
  '/0ther/website/hi-res/2024-12-18_contribute-press_by-David-Revoy.jpg',
  _("I have no marketing or communication budget, so I'm counting on your help! Writing articles, creating blog posts, building communities, and sharing links on social media are all valuable contributions. I've even created a Press gallery with ready-made assets to assist you."),
  $root.'/'.$lang.'/files/press.html',
  _("Access the Press Assets")
  );

_menu_card_item(
  _("Beta-reading new episodes"), 
  "/0ther/website/hi-res/2021-07-14_contribute-D-beta-reading_by-David-Revoy.jpg", 
  _("One to two weeks before publicly releasing a new episode, I often post a thread for translators, proofreaders, and curious readers to provide feedback on the upcoming episode. I usually announce this period on my blog. All you need is a Framagit account to participate!"), 
  "https://framagit.org/peppercarrot/webcomics/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name%5B%5D=future%20episode",
  _("Read Previous Beta Discussions")
  );

_menu_card_item(
  _("Contribute Financially"), 
  "/0ther/website/hi-res/2024-12-18_contribute-financially_by-David-Revoy.jpg", 
  _("I've detailed everything about funding on the Philosophy and Support pages. Additional funds will help me address common domestic challenges (like paying bills) and allow me to dedicate more time to creating the comic. Any donation, no matter how small —whether one-time or recurring— will make a significant difference, as will purchases from the shop."), 
  $root.'/'.$lang.'/support/index.html',
  _("Become a Patron")
  );

_menu_card_item(
  _("Other contributions ideas"), 
  "/0ther/website/hi-res/2024-12-18_contribute-various_by-David-Revoy.jpg", 
  _("There are many ways to contribute! Developers can help fix the painting software I use, musicians can create inspiring themes for Pepper&Carrot, journalists can share news about Pepper&Carrot in newspapers or on TV, and printers can produce posters and merchandise featuring Pepper&Carrot. The possibilities are endless!"), 
  $root.'/'.$lang.'/chat/index.html',
  _("Share Your Ideas in Chat")
  );

echo '  </main>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
