<?php if ($root=="") exit;
$fallbackmode = 0;

# variable:
$comics_list_url = $root.'/'.$lang.'/webcomics/misc.html';
$comics_path = $sources.'/misc';
$comics_list = $webcomic_misc_list; # → lib-database.php
$dirname = $webcomic_misc_dir; # → ../index.php
$title = $info_lang["title"]; # → ../index.php

# transcript
$transcript = ''.$comics_path.'/'.$webcomic_misc_dir.'/lang/'.$lang.'/transcript.md';
if (!file_exists($transcript)) {
  $transcript = ''.$comics_path.'/'.$webcomic_misc_dir.'/lang/en/transcript.md';
}

# Load metadatas
# info.json
$info = array();
$info = json_decode(file_get_contents(''.$comics_path.'/'.$content.'/info.json'), true);
$published = $info["published"];
$background_color = $info["background-color"];
$license = $info["license"];
$public_status = $info["public"];

$intro_md = $info_lang["intro"]; # → ../index.php
$Parsedown = new Parsedown();
$intro = $Parsedown->text($intro_md);

# Options
$containeroption = '';
$webcomicpageoption = '';
$wrapperoption = 'style="background:'.$background_color.';"';
$gif_width = '1100px';
$gif_padding = '32px';
if (in_array("hd", $option)) {
  $containeroption = 'style="max-width: none !important;"';
  $wrapperoption = 'style="max-width: none !important; background:'.$background_color.';"';
  $gif_width = '2265px';
  $gif_padding = '22px';
}
if (in_array("sbs", $option)) {
  $containeroption = 'style="max-width: none !important;"';
  $wrapperoption = 'style="max-width: none !important; background:'.$background_color.';"';
  $webcomicpageoption = 'style="display: grid; grid-template-columns: repeat(2, 1fr); gap: 10px; justify-items: center;"';
  $gif_width = '1100px';
  $gif_padding = '32px';
}

# Start display:
include($file_root.'core/mod-menu-lang.php');
echo '<div class="container webcomic" '.$containeroption.'>'."\n";

# Get the comic page
$all_pages = array();
$all_pages = glob($comics_path.'/'.$dirname.'/low-res/'.$lang.'_P[0-9][0-9]*'); # pattern to get all comic pages to display
if (empty($all_pages)) {
  $all_pages = glob($comics_path.'/'.$dirname.'/low-res/en_P[0-9][0-9]*');
  $fallbackmode = 1;
}

# Alt text
$alt_text = _md_to_alt($transcript); # → lib-function.php

_xyz_disclaimer(); # → lib-function.php
_fallback_messsage(); # → lib-function.php

echo '<div class="webcomic-bg-wrapper-misc">'."\n";
include($file_root.'core/lib-comic-options.php');

# Header
echo '<h2>'.$title.'</h2>'."\n";
echo '<div class="timestamp">'.sprintf(_("Published on %s."), $published).'</div>'."\n";
echo $intro."\n";

# Navigation
sort($comics_list);
echo ' '._navigation($content, $comics_list, $mode, $comics_list_url).'';
_clearboth();

echo '</div>'."\n";

# Loop on comic pages
echo '<div class="webcomic-bg-wrapper" '.$wrapperoption.'>'."\n";
foreach ($all_pages as $key => $page) {
  echo '  <div class="webcomic-page" '.$webcomicpageoption.'>'."\n";
  # Option: High Definition
  if (in_array("hd", $option)) {
    $page = str_replace('/low-res/', '/hi-res/', $page);
  }
  # Option: Side by side
  if (in_array("sbs", $option)) {
    $enpage = str_replace('/'.$lang.'_', '/en_', $page);
    echo '    <img src="'.$root.'/'.$enpage.'" alt="'.$page.'" title="'.$page.'">'."\n";
  }
  echo '    <img src="'.$root.'/'.$page.'" alt="'.$page.'" title="'.$page.'">'."\n";
  echo '  </div>'."\n";
}
echo '</div>'."\n";

# Footer navigation
echo ' '._navigation($content, $comics_list, $mode, $comics_list_url).'';
echo ''."\n";
_clearboth();

# Transcript button
$transcript = file_get_contents($transcript);
if (strpos($transcript, $wip_transcript_string) == false) {
  $Parsedown = new Parsedown();
  $html_transcript = $Parsedown->text($transcript);
  echo '<details>'."\n";
  echo '<summary class="webcomic-details">'._("Transcript").'</summary>'."\n";
  echo $html_transcript;
  echo '</details>'."\n";
}

# Footer support
echo '<div class="webcomic-footer-box">'."\n";
echo '    <h3>'._("Support my free(libre) and open-source webcomics on:").'</h3>'."\n";
_display_support_links("thumbnails","100","50","");
echo '</div>'."\n";

# Footer sources
echo '<div class="webcomic-footer-box">'."\n";
echo '    <h3>'._("Source files:").'</h3>'."\n";
$artwork_sources = ''.$comics_path.'/'.$dirname.'/zip/'.$dirname.'_art-pack.zip';
if (file_exists($artwork_sources)){
  echo '      '._("Artworks:").''."\n";
  $filename = basename($artwork_sources);
  $fileweight = (filesize($artwork_sources) / 1024) / 1024;
  echo '      <a href="'.$root.'/'.$artwork_sources.'">'.$filename.' <span class="small-info">(Krita KRA, '.round($fileweight, 2).'MB )</span></a>'."\n";
}
echo '    <br>'."\n";
$lang_sources = ''.$comics_path.'/'.$dirname.'/zip/'.$dirname.'_lang-pack.zip';
if (file_exists($lang_sources)){
  echo '      '._("Speechbubbles:").''."\n";
  $filename = basename($lang_sources);
  $fileweight = (filesize($lang_sources) / 1024) / 1024;
  echo '      <a href="'.$root.'/'.$lang_sources.'">'.$filename.' <span class="small-info">(Inkscape SVG, '.round($fileweight, 2).'MB)</span></a>'."\n";
}
echo '    <br>'."\n";
$export_dir = $root.'/'.$comics_path.'/'.$dirname.'/hi-res/';
echo '      <a href="'.$export_dir.'">'._("Exports for printing").' <span class="small-info">(JPG, PNG, PDF...)</span></a>'."\n";
echo '    <br>'."\n";
$git_repository = "https://framagit.org/peppercarrot/webcomics";
echo '      <a href="'.$git_repository.'">'._("Git repository").'</a>'."\n";
echo '</div>'."\n";

# Footer license
echo '<div class="webcomic-footer-box">'."\n";
echo '    <h3>'._("License:").'</h3>'."\n";
_display_cc_links("$license", '<a href="#url" title="#name_lrg">#img</a><br><a href="#url" title="#name_lrg">#name_lrg</a>');
echo '    <br>'."\n";
echo '    <br>'."\n";
echo '    '._("Attribution to:").''."\n";
$sources_path = 'misc/'.$dirname.'';
_print_credits($lang, $sources_path);
_display_cc_links("$license", '<span class="small-info">#notice</span>');
echo '</div>'."\n";

# Related URLs
_list_related_url(''.$comics_path.'/'.$content.'/info.json');


# end
echo ''."\n";
echo '</div>'."\n";
echo ''."\n";
?>
