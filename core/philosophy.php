<?php if ($root=="") exit;

$license_link = ''.$root.'/'.$lang.'/license/index.html';
$img_width = 827;

echo '<div class="container">';
include($file_root.'core/mod-menu-lang.php');
echo '</div>';

echo '<div class="container container-sml">'."\n";
echo '  <section class="page">'."\n";

echo '  <h1>'._("Philosophy").'</h1> '."\n";
# ---
echo '  <h2>'._("Supported by patrons").'</h2> '."\n";

echo '  <p>'._("The Pepper&Carrot project is entirely supported by the generosity of patrons from around the world. By contributing financially, each patron plays a vital role in enabling the creation of new content, and in return, they have the option to receive a special credit at the end of future episodes. Thanks to this system, Pepper&Carrot can stay independent and never have to resort to advertising or any marketing pollution.").'</p> '."\n";


_img($root.'/'.$sources.'/0ther/wiki/hi-res/2015-02-09_philosophy_01-support_by-David-Revoy.jpg', _("Pepper and Carrot receiving money from the audience."), $img_width, 740, 82, 'round-contrast');

# ---

echo '  <br> '."\n";
echo '  <h2>'._("100&#37; free(libre), forever, no paywall").'</h2> '."\n";

echo '  <p>'._("All the content I produce about Pepper&Carrot is on this website or on my blog, free(libre) and available to everyone. I respect all of you equally: with or without money. All the goodies I make for my patrons are also posted here. Pepper&Carrot will never ask you to pay anything or to get a subscription to get access to new content.").'</p> '."\n";

_img($root.'/'.$sources.'/0ther/wiki/hi-res/2015-02-09_philosophy_03-paywall_by-David-Revoy.jpg', _("Carrot, locked behind a paywall."), $img_width, 740, 82, 'round-contrast');

# ---

echo '  <br> '."\n";
echo '  <h2>'._("Open-source and permissive").'</h2> '."\n";

echo '  <p>'._("I want to give people the right to share, use, build and even make money upon the work I've created. All pages, artworks and content were made with Free(Libre) Open-Sources Software on GNU/Linux, and all sources are on this website (Sources and License buttons). Commercial usage, translations, fan-art, prints, movies, video-games, sharing, and reposts are encouraged. You just need to give appropriate credit to the authors (artists, correctors, translators involved in the artwork you want to use), provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the authors endorse you or your use. More information can be read about it here: ").'</p>'."\n";

# End of link for language selection of https://creativecommons.org/licenses/by/4.0/
# %s stands for the 2 language code letters, in some cases it doesn't match the licence.
# If that's the case, you can replace %s with a static code, e.g. deed.es or deed.pt
# Doing this may cause an error in some editors, ignore it.
echo '  <a class="btn btn-philosophy" href="https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).'">'._("Creative Commons Attribution 4.0 International license.").'</a>'."\n";

_img($root.'/'.$sources.'/0ther/wiki/hi-res/2015-02-09_philosophy_04-open-source_by-David-Revoy.jpg', _("Example of derivatives possible."), $img_width, 740, 82, 'round-contrast');

# ---

echo '  <br> '."\n";
echo '  <h2>'._("Quality entertainment for everyone, everywhere").'</h2> '."\n";

echo '  <p>'._("Pepper&Carrot is a comedy/humor webcomic suited for everyone, every age. No mature content, no violence. Free(libre) and open-source, Pepper&Carrot is a proud example of how cool free-culture can be. I focus a lot on quality, because free(libre) and open-source doesn't mean bad or amateur. Quite the contrary.").'</p> '."\n";

_img($root.'/'.$sources.'/0ther/wiki/hi-res/2015-02-09_philosophy_05-everyone_by-David-Revoy.jpg', _("Comic pages around the world."), $img_width, 740, 82, 'round-contrast');

# ---

echo '  <br> '."\n";
echo '  <h2>'._("Let's change comic industry!").'</h2> '."\n";

echo '  <p>'._("Without intermediary between artist and audience you pay less and I benefit more. You support me directly. No publisher, distributor, marketing team or fashion police can force me to change Pepper&Carrot to fit their vision of 'the market'. Why couldn't a single success 'snowball' to a whole industry in crisis? We'll see…").'</p> '."\n";


_img($root.'/'.$sources.'/0ther/wiki/hi-res/2015-02-09_philosophy_06-industry-change_by-David-Revoy.jpg', _("Diagram: on the left-hand side, Carrot is losing money with many middle-men. On the right-hand side, the result is more balanced."), $img_width, 740, 82, 'round-contrast');

echo '  <br> '."\n";

echo '    <a class="btn btn-philosophy" href="'.$root.'/'.$lang.'/support/index.html">'._("Become a patron").'</a>'."\n";

# ---

echo '  <br> '."\n";
echo '<h2>'._("A Decade Later... Has the industry changed?").'</h2>'."\n";

echo '  <p>'._("As I look back on the lines I wrote on the homepage ten years ago − \"A free(libre) and open-source webcomic supported directly by its patrons to change the comic book industry!\" − I'm struck by how much the industry has changed. Was Pepper&Carrot a catalyst for some of these shifts? The internet was a vastly different place back then. Webcomics relied on a single business model: selling merchandise and accepting one-time donations via PayPal. The concept of recurring patronage was still in its infancy.").'</p>'."\n";

echo '  <p>'._("I'm proud to say that Pepper&Carrot was one of the first webcomics to join the Patreon initiative, pioneered by YouTubers who sought to revolutionize the way artists were supported. Today, patronage platforms are the norm, and it's rare to find an artist without one.").'</p>'."\n";

echo '  <p>'._("However, not all of my innovations have taken off. My decision to use only free, libre, and open-source software licenses, and to create the entire comic using these tools, remains a rare approach. Many artists have opted for a more traditional model, where they create copyrighted materials and rely on patronage to support their work. While this model often provides \"free access\" to content, I believe it's a flawed system because it doesn't guarantee it.").'</p>'."\n";

echo '  <p>'._("In my model, readers don't just fund the creation of content – they also gain guaranteed, irrevocable access to it, along with the right to reuse and modify it, even commercially. This approach ensures that the work remains free and open, rather than being locked behind paywalls or proprietary licenses. In short, what is funded by the audience should belong to the audience.").'</p>'."\n";

echo '  <p>'._("As I look around at the current state of the industry, I'm more convinced than ever that my approach was the right one. Proprietary software is increasingly plagued by privacy issues, and features (e.g. Blockchain/NFTs/AI) that nobody wants except investors. Meanwhile, artists are struggling with copyright issues, being owned by publishers, platforms, and facing restrictions on where they can publish their work.").'</p>'."\n";

echo '  <p>'._("That's why my philosophy remains unchanged. I still believe that using Creative Commons licenses and free, libre, and open-source software is the key to true freedom and creativity. My tagline − \"A free(libre) and open-source webcomic supported directly by its patrons to change the comic book industry!\" − remains a guiding principle for me, even if it's still a minority view.").'</p>'."\n";

echo '  <p>'._("I know that I'm just a drop in the ocean, but I'm committed to continuing to push for a more open, more free, and more creative industry. I'm proud to be part of a movement that's slowly but surely making progress, and I'm grateful to my patrons for supporting me on this journey.").'</p>'."\n";

echo '        <span style="float:right;">− '._("David Revoy").'</span><br>'."\n";

echo '<br>';

echo '<br>';
_clearboth();
echo '  </section>'."\n";
echo '</div>'."\n";
echo ''."\n";

?>
