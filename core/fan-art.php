<?php if ($root=="") exit;

# Initiate page
echo '<div class="container">'."\n";

# Include the webcomic sources
include($file_root.'core/mod-menu-lang.php');

$number_fanart = _gallery_number("fan-art");
$chatroom = $root.'/'.$lang.'/chat/index.html';

# Check and clean our options:
$option_get = implode("-", $option);
$option_clean = str_replace('~~', '/', $option_get);
$option_clean = str_replace('..', '', $option_clean);

# Sidebar
# -------
echo '  <aside class="col sml-12 med-12 lrg-2">'."\n";
echo '    <div class="sidebar-img">'."\n";
# Array of directories available
$sources_directories = glob($sources.'/0ther/*');
sort($sources_directories);

$dirclass = '';

_sidebar_img_btn("fan-art", "fan-art");
_sidebar_img_btn("fan-art", "fan-comics");
_sidebar_img_btn("fan-art", "cosplay");
_sidebar_img_btn("fan-art", "fan-fiction");

echo '    </div>';
echo '  </aside>'."\n";

# Container start for all
echo '  <article class="col sml-12 med-12 lrg-10">'."\n";


# FAN FICTION
# ------------
if ($content == 'fan-fiction') {

  $repositoryURL = "https://framagit.org/peppercarrot/scenarios";
  $datapath = $scenario;

  echo '<div class="page fan-fiction-page">'."\n";

  $pattern = $datapath.'/'.$option_clean.'.md';
  if (file_exists($pattern)) {

    # Start edit buttons
    echo '     <div class="edit" >'."\n";

    echo '       <div class="button moka" >'."\n";
    echo '        <a href="'.$repositoryURL.'/commits/master/'.$option_clean.'.md" target="_blank" title="'._("External history link to see all changes made to this page").'" >'."\n";
    echo '          <img width="16" height="16" src="'.$root.'/core/img/history.svg" alt=""/>'."\n";
    echo '            '._("View history").''."\n";
    echo '        </a>'."\n";
    echo '       </div>'."\n";

    echo '       <div class="button moka" >'."\n";
    echo '        <a href="'.$repositoryURL.'/edit/master/'.$option_clean.'.md" target="_blank" title="'._("Edit this page with an external editor").'" >'."\n";
    echo '          <img width="16" height="16" src="'.$root.'/core/img/edit.svg" alt=""/>'."\n";
    echo '            '._("Edit").''."\n";
    echo '        </a>'."\n";
    echo '       </div>'."\n";

    echo '     </div>'."\n";

    # Display the page
    $contents = file_get_contents($pattern);
    $Parsedown = new Parsedown();
    echo $Parsedown->text($contents);
    echo '	    <br><br>'."\n";

  } else {

    echo '<h1>'.$content_info["fan-fiction"][0].'</h1>';
    # Fan Fiction main menu
    echo '<p>'.$content_info["fan-fiction"][1].'</p>';

    # Fan-fictions subtitles categories
    $fanfiction_folders = array('01_novels'=>_("Novels"),
                                '02_Scenarios-for-new-episodes'=>_("Scenarios for new episodes"),
                                '03_community-improvisations'=>_("Community improvisations"),
                                '04_Temporary_Drafts'=>_("Temporary Drafts")
                          );

    foreach ($fanfiction_folders as $dirname => $title) {

      echo '<br>'."\n";
      echo ''."\n";
      $title_id = urlencode($title);
      echo '<section>'."\n";
      echo '<h2 id='.$title_id.'>'.$title.'</h2>'."\n";

      $search = glob($datapath."/".$dirname."/*.md");
      if (!empty($search)){
        foreach ($search as $file) {

          # clean path into filename without extension
          $txtlink = basename($file);
          $txtlink = preg_replace('/\\.[^.\\s]{2,4}$/', '', $txtlink);

          # Split prefix (in case of a French novel, right now)
          $prefix = "";
          if (strpos($txtlink, 'fr~') !== false) {
            # Prefix for the novels written by Nartance
            $prefix = '<span class="notes">('._("In French").')</span>';
          }

          # Extract attribution for the author
          preg_match('/.*([-_]by[-_].*)/', $txtlink, $txtattribution_part);
          $txtattribution = str_replace('-', ' ', $txtattribution_part[1]);
          $txtattribution = str_replace('by', '', $txtattribution);
          $txtattribution = str_replace('_', ' ', $txtattribution);
          $txtattribution = str_replace('  ', '', $txtattribution);

          $filelabel = str_replace($txtattribution_part[1], '', $txtlink);
          $filelabel = str_replace('fr~', '', $filelabel);
          $filelabel = str_replace('--', ', ', $filelabel);
          $filelabel = str_replace('_by', '"', $filelabel);
          $filelabel = str_replace('_', ' ', $filelabel);
          $filelabel = str_replace('-', ' ', $filelabel);
          $filelabel = preg_replace('/\\.[^.\\s]{2,4}$/', '', $filelabel);
          $filelabel = ucfirst($filelabel);

          if (substr($file, 0, 1) === '_') {
            # exclude system file starting with '_'.

          } else {

            # filter
            if ($file === 'README.md'){
                # in case of README
            } else if ($file === 'template.md'){
              # filter template, do nothing

            } else {
              # display the result
              $path_to_page = ''.$root.'/'.$lang.'/fan-art/fan-fiction__'.$dirname.'~~'.$txtlink.'.html';
              # %1$s = artwork title, %2$s = author
              echo sprintf(_('%1$s by %2$s'), '<div class="scenariolist">'.$prefix.'&nbsp;<a href="'.$path_to_page.'" title="">'.$filelabel.'</a><span class="notes">', $txtattribution)."</span></div>\n";
            }

          }
        }
      }
      echo '</section>'."\n";
      echo ''."\n";
    }
  }
  echo '  <br>'."\n";
  echo '  </div>'."\n";
  echo '  </article>'."\n";

} else if ($content == 'fan-comics') {

  # FAN COMICS
  # ----------

  $pathcommunityfolder = $sources.'/0ther/community';
  $community_subdir_check = $pathcommunityfolder.'/'.$option_clean.'/00_cover.jpg';
  $community_episode_check = $pathcommunityfolder.'/'.$option_clean.'.jpg';

  # Fan Comics Episode listing
  # =========================
  if (file_exists($community_subdir_check)) {

    echo '<div class="fan-comic-page">';
    $folderpath = dirname($community_subdir_check);
    $foldername = basename($folderpath);

    # Display the title of the project and markdown:
    $foldernameclean = str_replace('_', ' ', $foldername);
    $foldernameclean = str_replace('-', ' ', $foldernameclean);
    $foldernamereplace = explode(" by ", $foldernameclean, 2);
    $foldernameclean = '<h2>'.sprintf(_('%1$s by %2$s'), $foldernamereplace[0].'</h2><span class="font-size: 0.6em;">', $foldernamereplace[1]).'</span>';

    $hide = array('.', '..');
    $mainfolders = array_diff(scandir($folderpath), $hide);
    if (file_exists($folderpath.'/'.$lang.'_infos.md')) {
      $contents = file_get_contents($folderpath.'/'.$lang.'_infos.md');
    } else {
      $contents = file_get_contents($folderpath.'/en_infos.md');
    }
    $Parsedown = new Parsedown();
    _header_sml_fancomics($foldernameclean, $Parsedown->text($contents));


    # Display episodes
    echo '<section class="col sml-12 med-12 lrg-10 sml-centered sml-text-center" style="padding:0 0;">';
    # We look at English P01: first page of each fan-art webcomic; and in English for the thumbnail (the default minimal language to post an episode)
    $search = glob($folderpath.'/en*P01*.jpg');
    rsort($search);
    # we loop on found episodes
    if (!empty($search)){
      foreach ($search as $filepath) {
        # check if translation exists
        $filepathlocale = str_replace('en_', $lang.'_', $filepath);
        if (file_exists($filepathlocale)) { $filepath = $filepathlocale; }
        # episode number extraction
        $filename = basename($filepath);
        $filenameoption = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
        $fullpath = dirname($filepath);
        $filenameclean = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
        $filenameclean = substr($filenameclean, 11); // remove 11 first characters
        $filenameclean =  substr($filenameclean, 0, 2); // keeps two numbers
        $filenameclean = str_replace('_', ' ', $filenameclean);
        $filenameclean = str_replace('-', ' ', $filenameclean);

        echo '<a href="'.$root.'/'.$lang.'/fan-art/fan-comics__'.$foldername.'~~'.$filenameoption.'.html" >';
        echo '<figure class="thumbnail col sml-6 med-4 lrg-4">';
         _img($filepath, $filename, 400, 400, 82);
        echo '<figcaption class="fan-comic-box-detail" >';
        echo '  Episode '.$filenameclean.'';
        echo '</figcaption>';
        echo '</figure>';
      }
    }
    echo '</section>';

  _clearboth();
  echo '</div>';
  # Fan Comics display comics
  # =========================
  } else if (file_exists($community_episode_check)) {

    $jpg_file = basename($community_episode_check);
    $jpg_file_notranslation = substr($jpg_file, 1);
    $folderpath = dirname($community_episode_check);
    $foldername = basename($folderpath);
    $thisepisode_number = '01';

    # Background color
    $css_color = '';
    $background_color = "#FFF";
    $info = array();
    $info = json_decode(file_get_contents($folderpath.'/info.json'), true);
    $background_color = $info["background-color"];
    $css_color = 'style="background:'.$background_color.';"';

    echo '<div class="fan-comic-page" '.$css_color.'>';

    # Nex-prev buttons
    $episode_index = glob($folderpath.'/'.$lang.'_*E[0-9][0-9]P01*.jpg');
    if (empty($episode_index)){
      $episode_index = glob($folderpath.'/en_*E[0-9][0-9]P01*.jpg');
    }
    $episode_index_formated = array();
    foreach ($episode_index as $episodefullpath) {
      $picture = basename($episodefullpath);
      $picture_withoutext = preg_replace('/\\.[^.\\s]{3,4}$/', '', $picture);
      $folderpath = dirname($episodefullpath);
      $foldername = basename($folderpath);
      $item = $foldername.'~~'.$picture_withoutext;
      array_push($episode_index_formated, $item);
    }
    $current_option = str_replace('/en_', '/'.$lang.'_', $option_get);
    $path_all_thumbnails = $root.'/'.$lang.'/fan-art/fan-comics__'.$foldername.'.html';
    echo ''._navigation($current_option, $episode_index_formated, "fan-art", $path_all_thumbnails).'';


    # Display the pages
    # Pepper-and-Carrot-Mini_by_Nartance~~en_PCMINI_E22P01_by-Nartance.html
    if (preg_match('/[a-zA-Z\~-]+([0-9]+)P[0-9][0-9][a-zA-Z-]*/', $option_get, $results)) {
      $thisepisode_number = $results[1];
    }


    $pages_to_display = glob($folderpath.'/'.$lang.'_*E'.$thisepisode_number.'P*.jpg');
    if (empty($pages_to_display)){
      $pages_to_display = glob($folderpath.'/en_*E'.$thisepisode_number.'P*.jpg');
    }
    foreach ($pages_to_display as $page) {
      echo '<div style="text-align:center;">';
      echo '  <img src="'.$root.'/'.$page.'">';
      echo '</div>';
    }
    echo ''._navigation($current_option, $episode_index_formated, "fan-art", $path_all_thumbnails).'';

    _clearboth();
    echo '</div>';
  } else {
  # Fan Comics Main menu
  # ====================

    _header($content);
    $hide = array('.', '..');
    $mainfolders = array_diff(scandir($pathcommunityfolder), $hide);
    sort($mainfolders);

    # we loop on found episodes
    foreach ($mainfolders as $folderpath) {
      # Name extraction
      $filename = basename($folderpath);
      $filenameclean = str_replace('_', ' ', $filename);
      $filenameclean = str_replace('-', ' ', $filenameclean);
      $filenameclean = str_replace('featured', '', $filenameclean);
      $filenamereplace = explode(" by ", $filenameclean, 2);
      $filenameclean = sprintf(_('%1$s by %2$s'), $filenamereplace[0].'</a><br/><span class="fan-comic-box-detail">', $filenamereplace[1]);
      $filenamezip = str_replace('jpg', 'zip', $filename);
      echo '<figure class="thumbnail col sml-6 med-4 lrg-4">';
      echo '  <div class="fan-comic-box">';
      echo '    <a href="'.$root.'/'.$lang.'/fan-art/fan-comics__'.$folderpath.'.html" >';
                  _img(''.$root.'/'.$pathcommunityfolder.'/'.$folderpath.'/00_cover.jpg', $filename, 400, 400, 82);
      echo '    </a><br/>';
      echo '    <figcaption>';
      echo '      <a href="'.$root.'/'.$lang.'/fan-art/fan-comics__'.$folderpath.'.html" >';
      echo '        '.$filenameclean.'';
      echo '      </a>';
      echo '    </figcaption>';
    echo '    </div>'."\n";
      echo '</figure>';
    }
    _clearboth();
    echo '    <a class="btn btn-fan-comics" href="'.$root.'/'.$lang.'/documentation/090_Fan_comic_Submission_Guidelines.html">'._("Fan-comics Submission Guidelines").'</a>'."\n";
    _clearboth();
    echo '  </div>'."\n"; # -> _header
    echo '</div>'."\n"; # -> _header
  }


} else if ($content == 'cosplay') {
# COSPLAY GALLERY
# ---------------

_header("cosplay", "gallery");

} else {
# FAN-ART GALLERY
# ---------------

_header("fan-art", "gallery");

}


echo '  </article>'."\n";
_clearboth();
echo '</div>'."\n"; # Container
echo ''."\n";
?>
