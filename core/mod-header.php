<?php if ($root=="") exit;

# This string is used in header, logo info and metadata.
$pepper_and_carrot = _("Pepper&amp;Carrot");
# Add the same suffix for all title in URL
$header_title_full = $header_title.' - '.$pepper_and_carrot.'';
# Meta description of the website, this is what search engines like Google show.
$header_description = _("Official homepage of Pepper&amp;Carrot, a free(libre) and open-source webcomic about Pepper, a young witch and her cat, Carrot. They live in a fantasy universe of potions, magic, and creatures.");
$header_keywords = 'david, revoy, deevad, open-source, comic, webcomic, creative commons, patreon, pepper, carrot, pepper&amp;carrot, libre, artist';

# Display HTML header
# -------------------
echo '<!DOCTYPE html>'."\n";
# $isolang is part of lib-database, and convert a P&C $lang to IETF's BCP 47
# Recommended here: 
echo '<html lang="'.$isolang[$lang].'">'."\n";

# Ascii comment: Carrot welcome the reader of the code
echo '<!--'."\n";
echo '       /|_____|\     ____________________________________________________________'."\n";
echo '      /  \' \' \'  \    |                                                          |'."\n";
echo '     < ( .  . )  >   |  Oh? You read my code? Welcome!                          |'."\n";
echo '      <   °◡    >   <   Full sources on framagit.org/peppercarrot/website-2021  |'."\n";
echo '        \'\'\'|  \      |__________________________________________________________|'."\n";
echo ''."\n";
echo ' Version:'.$version.''."\n";;
echo '-->'."\n";

echo '<head>'."\n";
echo '  <meta charset="utf-8" />'."\n";
echo '  <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0" />'."\n";
echo '  <meta property="og:title" content="'.$header_title_full.'"/>'."\n";
echo '  <meta property="og:description" content="'.$header_description.'"/>'."\n";
echo '  <meta property="og:type" content="article"/>'."\n";
echo '  <meta property="og:site_name" content="'.$pepper_and_carrot.'"/>'."\n";
echo '  <meta property="og:image" content="'.$social_media_thumbnail_URL.'"/>'."\n";
echo '  <meta property="og:image:type" content="image/jpeg" />'."\n";
echo '  <meta name="fediverse:creator" content="@davidrevoy@framapiaf.org">'."\n";
echo '  <title>'.$header_title_full.'</title>'."\n";
echo '  <meta name="description" content="'.$header_description.'" />'."\n";
echo '  <meta name="keywords" content="'.$header_keywords.'" />'."\n";
echo '  <link rel="icon" href="'.$root.'/core/img/favicon.png" />'."\n";
echo '  <link rel="stylesheet" href="'.$root.'/core/css/framework.css?v='.$version.'" media="screen" />'."\n";
echo '  <link rel="stylesheet" href="'.$root.'/core/css/theme.css?v='.$version.'" media="screen" />'."\n";
echo '  <link rel="alternate" type="application/rss+xml" title="RSS (blog posts)" href="https://www.davidrevoy.com/feed/en/rss" />'."\n";

# Load a css exception rules (language specific, optional):
$css_lang = 'core/css/'.$lang.'.css';
if (file_exists($css_lang)) {
  echo '  <link rel="stylesheet" href="'.$root.'/'.$css_lang.'?v='.$version.'" media="screen" />'."\n";
}

# Keyboard arrow navigation Js script:
if ($mode == "webcomic" OR  $mode == "viewer" OR $mode == "fan-art") {
  echo '  <script async="" src="'.$root.'/core/js/navigation.js?v='.$version.'"></script>'."\n";
}

echo '</head>'."\n";
echo ''."\n";

if ($mode == 'webcomic' || $mode == 'miniFantasyTheater' || $mode == 'webcomic-misc' ) {
  $body_bg = 'background: #3c3c3c url(\''.$root.'/core/img/paper-ingres-dark-shade-A.png\') repeat;';
} else {
  $body_bg =  'background: #121d27 url(\''.$root.'/core/img/bg_ep7.jpg\') repeat-x top center;'; 
}

echo '<body style="'.$body_bg.'">'."\n";

include($file_root.'core/mod-top-menu.php');

?>
