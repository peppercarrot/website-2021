<?php if ($root=="") exit;

echo '<div class="container">'."\n";
echo '  <div class="paper">'."\n";

echo '    <div class="coverArt">'."\n";

include($file_root.'core/mod-menu-lang.php');

echo '      <section class="col sml-12">'."\n";

echo '        <div class="coverbox col sml-12">'."\n";
echo '          <div class="textbox">'."\n";

if (file_exists('po/'.$lang.'.svg')) {
  echo '              <img width="410" height="88" class="biglogo" src="'.$root.'/po/'.$lang.'.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
} else {
  echo '              <img width="410" height="88" class="biglogo" src="'.$root.'/po/en.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
}

echo '              <div class="subtext">'."\n";
echo '                <p>'."\n";
echo '                '._("A free(libre) and open-source webcomic supported directly by its patrons to change the comic book industry!").''."\n";
echo '                </p>'."\n";
echo '               <a class="btn btn-moreinfo" href="'.$root.'/'.$lang.'/philosophy/index.html" title="'._("More information").'">'."\n";
echo '                 '._("More information").''."\n";
echo '               </a>'."\n";
echo '                <a class="btn btn-readfirst" href="'.$root.'/'.$lang.'/webcomic/'.$pc_episodes_list[0].'.html" title="'._("Read from the beginning").'">'."\n";
echo '                  '._("Read from the beginning").''."\n";
echo '                </a>'."\n";
echo '              </div>'."\n";
echo '           </div>'."\n";
echo '           <img id="signature" src="'.$root.'/core/img/David-Revoy_logo.svg" alt="David Revoy" title="David Revoy" />';
echo '          </div>'."\n";
echo '      </section>'."\n";
echo '      <div style="clear:both"></div>'."\n";
echo '    </div>'."\n"; #coverArt

echo ''."\n";


echo '    <div class="grid">'."\n";
# Latest episode
echo '    <section class="col sml-12 med-6 lrg-4">'."\n";
echo '    <div class="home-box">'."\n";
echo '      <h3 style="margin-top: 0; margin-bottom: 0.2rem; display: block; text-align: center; font-size: 1.6rem;">'._("Latest episode").'</h3>'."\n";
echo ''."\n";

# Array of all episodes (copied from database to sort it backward, newer episode on top)
$all_episodes = $pc_episodes_list;
rsort($all_episodes);

$all_episodes_count = count($all_episodes);
$episodes_homepage = array();
$episodes_homepage = array_slice($all_episodes, 0, 1);
foreach ($episodes_homepage as $key => $episode_directory) {
  $episode_number = preg_replace('/[^0-9.]+/', '', $episode_directory);
  $cover_path = ''.$sources.'/'.$episode_directory.'/low-res/'.$lang.''.$credits.'E'.$episode_number.'.jpg';
  $episode_link = $root.'/'.$lang.'/webcomic/'.$episode_directory.'.html';
  $cover_description = ''.sprintf(_('Click to read episode %d.'), $episode_number).'';
  # In case the cover is not available in the current language, fallback to English.
  if (!file_exists($cover_path)) {
    $cover_path = ''.$sources.'/'.$episode_directory.'/hi-res/en'.$credits.'E'.$episode_number.'.jpg';
  }
  echo '        <figure class="recent-episode col sml-12">'."\n";
  echo '          <a href="'.$episode_link.'">'."\n";
  echo '        ';
  _img($root.'/'.$cover_path, $cover_description, 480, 399, 89);
  echo ''."\n";
  echo '          </a>'."\n";
  echo '        </figure>'."\n";
}
echo ''."\n";

echo '          <a class="btn btn-more" href="'.$root.'/'.$lang.'/webcomics/peppercarrot.html">'.sprintf(ngettext('Show %d episode', 'Show all %d episodes', $all_episodes_count), $all_episodes_count).'</a>'."\n";
echo '    </div>'."\n";
echo '    </section>'."\n";
echo ''."\n";

# Recent News
# -----------
echo '  <section class="col sml-12 med-6 lrg-4">'."\n";
echo '  <div class="home-box">'."\n";
echo '    <h3>'._("Recent blog-posts").'</h3>'."\n";
if ($lang == "en") {
  echo '     <span class="note"><br></span>'."\n";
} else {
  echo '     <span class="note">'._("(Note: in English only)").'</span>'."\n";
}
echo '     <ul>'."\n";
# Mini RSS reader cached
$rss_url = "https://www.davidrevoy.com/feed/rss";
$cache_filename = $cache.'/_blog-rss.txt';
$cache_limit_in_mins = 60; # 60 default

# Workaround in case of missing cache file
if (!file_exists($cache_filename)) {
  $cache_placeholder = fopen($cache_filename,"w");
  fwrite($cache_placeholder,"RSS cache was missing, rebuilding it... be patient.");
  fclose($cache_placeholder);
  chmod($cache_filename, 0620);
}

if (file_exists($cache_filename)) {
  $secs_in_min = 60;
  $diff_in_secs = (time() - ($secs_in_min * $cache_limit_in_mins)) - filemtime($cache_filename);
  # check if the cached file is older than our limit
  if ( $diff_in_secs < 0 ) {
    # It isn't, so display it
    $list = file_get_contents($cache_filename);
    # display inner
    echo $list;
  } else {
    # Cache is older now
    # Generate the RSS reader cache for the blog and display it
    $rss_feeds = simplexml_load_file($rss_url);
    $feed_count=0;
    $output_to_cache = array();
    if(!empty($rss_feeds)){
      foreach ($rss_feeds->channel->item as $item) {
        # Read the many information
        $title = htmlspecialchars($item->title, ENT_QUOTES | ENT_SUBSTITUTE);
        #$description = $item->description;
        $link = $item->link;
        $date = $item->pubDate;
        $formated_date = date('d M Y',strtotime($date));

      if($feed_count>=9) break;
        $output_to_cache[] = '  <li>'."\n";
        $output_to_cache[] = '    '.$formated_date.' : <a href="'.$link.'" title="'.$title.'">'.$title.'</a>'."\n";
        $output_to_cache[] = '  </li>'."\n";
        $feed_count++;
      }
    }

    $file = fopen ( $cache_filename, 'w' );
    fwrite ( $file, implode('',$output_to_cache));
    fclose ( $file );
    chmod($cache_filename, 0620);

    $list = file_get_contents($cache_filename);
    echo $list;
  }
}

echo '      </ul>'."\n";
echo '    <a class="btn btn-more" href="https://www.davidrevoy.com/blog">'._("Go to the blog").'</a>'."\n";
echo '  </div>'."\n";
echo '  </section>'."\n";

# Support
# -------
echo '  <section class="col sml-12 med-6 lrg-4">'."\n";
echo '  <div class="home-box bg-patronage">'."\n";
echo '    <h3>'._("Support Pepper&Carrot on").'</h3>'."\n";
echo '    <br>'."\n";
_display_support_links("thumbnails","120","60","");

echo '    <a class="btn btn-more" href="'.$root.'/'.$lang.'/support/index.html">'._("More information").'</a>'."\n";
echo '  </div>'."\n";
echo '  </section>'."\n";
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '</div>'."\n";

echo '    </div>'."\n";

echo '</div>'."\n";
echo '      <div style="clear:both"></div>'."\n";
echo '</div>'."\n";
?>

