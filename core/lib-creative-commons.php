<?php
# lib-creativecommons.php
# ---------------------
# A centralisation for the URLs pattern for all creative commons licenses of the website
# @author: David Revoy
# @license: http://www.gnu.org/licenses/gpl.html GPL version 3 or higher

function _display_cc_links($license, $format = '<a href="#url" title="#name_lrg">#img</a><br><a href="#url" title="#name_lrg">#name_sml</a><br>#notice') {

  # $license: cc-by | cc-by-sa | cc-by-nc-sa | fan-art
  # $format : A pattern with #keywords that will be replaced by the functions. It allows user to change the HTML presentation of the various data blocks.

  global $root;
  global $lang;
  $cc_good_practice_link = $root.'/'.$lang.'/documentation/120_License_best_practices.html';
  $cc_good_practice = sprintf(_("More information and good practice for attribution can be found <a href=\"%s\">on the documentation</a>."),$cc_good_practice_link);
  $ccby_url = 'https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).'';
  $ccby_img = '<img alt="cc-by" src="'.$root.'/core/img/cc-by.jpg"/>';
  $ccby_name_sml = 'CC BY 4.0';
  $ccby_name_lrg = _("Creative Commons Attribution 4.0 International");
  $ccby_notice = $cc_good_practice;

  # dictionary
  switch ($license) {

    case "cc-by":
      $url = $ccby_url;
      $img = $ccby_img;
      $name_sml = $ccby_name_sml;
      $name_lrg = $ccby_name_lrg;
      $notice = $ccby_notice;
      break;

    case "cc-by-sa":
      $url = 'https://creativecommons.org/licenses/by-sa/4.0/'.sprintf(_("deed.%s"), $lang).'';
      $img = '<img alt="cc-by-sa" src="'.$root.'/core/img/cc-by-sa.jpg"/>';
      $name_sml = 'CC BY-SA 4.0';
      $name_lrg = _("Creative Commons Attribution Share-alike 4.0 International");
      $notice = $cc_good_practice;
      break;

    case "fan-art":
      $url = 'https://en.wikipedia.org/wiki/Fan_art#Copyright';
      $img = '<img alt="?" src="'.$root.'/core/img/nolicense.jpg"/>';
      $name_sml = _("Fan-art");
      $name_lrg = _("Copyrighted Fan-art");
      $notice = _("This creation is fan art. It is reposted here with permission.").'<br>'._("Unless you have permission from the author(s), do not reuse this work in your project.");
      break;

    case "cc-by-nc-sa":
      $url = 'https://creativecommons.org/licenses/by-nc-sa/4.0/'.sprintf(_("deed.%s"), $lang).'';
      $img = '<img alt="cc-by-nc-sa" src="'.$root.'/core/img/cc-by-nc-sa.jpg"/>';
      $name_sml = 'CC BY-NC-SA 4.0';
      $name_lrg = _("Creative Commons Attribution Non-Commercial Share-alike 4.0 International");
      $notice = $cc_good_practice;
      break;

    default:
      $url = $ccby_url;
      $img = $ccby_img;
      $name_sml = $ccby_name_sml;
      $name_lrg = $ccby_name_lrg;
      $notice = $ccby_notice;
      break;
  }

  # Display: substitute the template
  $output = str_replace('#url',$url, $format);
  $output = str_replace('#name_lrg',$name_lrg, $output);
  $output = str_replace('#img',$img, $output);
  $output = str_replace('#name_lrg',$name_lrg, $output);
  $output = str_replace('#name_sml',$name_sml, $output);
  $output = str_replace('#notice',$notice, $output);

  echo $output;
}

?>
