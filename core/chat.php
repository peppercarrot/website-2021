<?php if ($root=="") exit; 

echo '<div class="container">'."\n";
  include($file_root.'core/mod-menu-lang.php');
echo '</div>'."\n";

_clearboth();

echo '<div class="container container-sml">'."\n";
echo '  <article class="page">'."\n";

echo '    <h1>Chat rooms</h1>';

echo '      <strong>English</strong> is the language used for collaborating on Pepper&Carrot. That\'s also the language used on the official chat rooms.<br/><br/> The main channel uses <strong>the Matrix protocol</strong>. A chat client named Element is available for free and will launch inside your web-browser without requiring any installation once you click the button under. But you\'ll have to create an account to interact:';

$matrix_room_url = "https://matrix.to/#/#peppercarrot:matrix.org";

# The code line under will HTML validate the page
# because apparently # symbol part of the Matrix URL
# is an illegal symbol, but if I do that, it also
# breaks the adress and Matrix can't find it anymore...
# So, let ignore validation on this and use what's work.
#$matrix_room_url = urlencode($matrix_room_url);
echo '        <a class="btn btn-philosophy" href="'.$matrix_room_url.'" title="Pepper&amp;Carrot official room on #peppercarrot:matrix.org">&nbsp;&nbsp;&nbsp;Pepper&amp;Carrot chat room on [Matrix]&nbsp;&nbsp;&nbsp;</a>';

echo '        <br/>Many other clients exists for the Matrix protocol, for your desktop computer and your smartphone. <a href="https://matrix.org/clients">You\'ll find a complete list here</a><br/>';
echo '	<br/><br/><b>Alternatives</b>:<br/>- the <strong>IRC</strong> channel (#pepper&amp;carrot on <a href="https://libera.chat/">libera.chat</a>), bridged. <br/> - the <a href="https://telegram.me/+V76Ep1RKLaw5ZTc0" title="A community Telegram channel"> <strong>Telegram</strong> channel</a> (managed by the community), bridged.<br/><br/>';

echo '        <span style="display: block; font-size:1.1rem; opacity: 0.7; text-align: center;">Note: the content of this channel is public and the log is saved.<br/> Please read <strong><a href="'.$root.'/'.$lang.'/documentation/409_Code_of_Conduct.html">our Code of Conduct</a></strong> before interacting.</span>';
echo '<br/>';
 _img($root.'/'.$sources.'/0ther/sketchbook/low-res/2020-04-17_podcast_by-David-Revoy.jpg', 'Pepper communicates on a microphone.', 900, 500, 82);
 
echo '  <br><br>'."\n";
echo '  </article>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
