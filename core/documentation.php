<?php

# The documentation calls lib-wiki. The core of lib-wiki is also used for the wiki.

$repositoryURL = "https://framagit.org/peppercarrot/documentation";
$mode_wiki = 'documentation';
$datapath = $documentation;
$wikitheme = "wikidoc";
$wikiicons = "";

include(dirname(__FILE__).'/lib-wiki.php');

?>
