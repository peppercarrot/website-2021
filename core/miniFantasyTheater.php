<?php if ($root=="") exit;
$fallbackmode = 0;

# variable:
$comics_list_url = $root.'/'.$lang.'/webcomics/miniFantasyTheater.html';
$comics_path = $sources.'/miniFantasyTheater';
$comics_list = $mft_episodes_list; # → lib-database.php
$title = $header_title; # → ../index.php
$suffix = "miniFantasyTheater";
$license = "cc-by-sa";

# Load metadatas
# info.json
$info = array();
$info = json_decode(file_get_contents(''.$comics_path.'/.'.$content.'.json'), true);
$published = $info["published"];
$public_status = $info["public"];

# lang/??/XXX_transcript.md
$transcript = ''.$comics_path.'/lang/'.$lang.'/'.$comicStripID.'_transcript.md';
if (!file_exists($transcript)) {
  $transcript = ''.$comics_path.'/lang/en/'.$comicStripID.'_transcript.md';
}

# Options
$resolution = 'low-res';
$containeroption = '';
$webcomicpageoption = '';
$hdclass = '';
if (in_array("hd", $option)) {
  $resolution = 'hi-res';
  $containeroption = 'style="max-width: none !important;"';
}
if (in_array("sbs", $option)) {
  $containeroption = 'style="max-width: none !important;"';
  $webcomicpageoption = 'style="display: grid; grid-template-columns: repeat(2, 1fr); gap: 10px; justify-items: center;"';
}

# Start display:
include($file_root.'core/mod-menu-lang.php');
echo '<div class="container webcomic miniFantasyTheater" '.$containeroption.'>'."\n";
include($file_root.'core/lib-comic-options.php');

# Get the comic page
$page = $comics_path.'/'.$resolution.'/'.$lang.'_'.$content.'_'.$suffix.'.jpg'; # pattern to get all comic pages to display
if (!file_exists($page)) {
  $page = $comics_path.'/'.$resolution.'/en_'.$content.'_'.$suffix.'.jpg';
  $fallbackmode = 1;
}

# Alt text
$alt_text = _md_to_alt($transcript); # → lib-function.php

_xyz_disclaimer(); # → lib-function.php
_fallback_messsage(); # → lib-function.php

# Header
echo '<h2>'.$title.'</h2>';
echo '<div class="timestamp">'.sprintf(_("Published on %s."), $published).'</div>';

# Navigation
sort($comics_list);
echo ' '._navigation($content, $comics_list, $mode, $comics_list_url).'';
_clearboth();


# Loop on comic pages
echo '<div class="webcomic-bg-wrapper-mft" '.$containeroption.'>'."\n";
echo '  <div class="webcomic-page" '.$webcomicpageoption.'>'."\n";
# Side by side option (sbs)
if (in_array("sbs", $option)) {
  $enpage = str_replace('/'.$lang.'_', '/en_', $page);
  echo '    <img src="'.$root.'/'.$enpage.'" alt="'.$enpage.'" title="'.$title.'" />'."\n";
}
echo '    <img src="'.$root.'/'.$page.'" alt="'.$page.'" title="'.$title.'">'."\n";
echo '  </div>'."\n";

echo '</div>'."\n";

# Footer navigation
echo ' '._navigation($content, $comics_list, $mode, $comics_list_url).'';
echo ''."\n";
_clearboth();

# Transcript button
$transcript = file_get_contents($transcript);
if (strpos($transcript, $wip_transcript_string) == false) {
  $Parsedown = new Parsedown();
  $html_transcript = $Parsedown->text($transcript);
  echo '<details>'."\n";
  echo '<summary class="webcomic-details">'._("Transcript").'</summary>'."\n";
  echo $html_transcript;
  echo '</details>'."\n";
}
_clearboth();

# Footer support
echo '<div class="webcomic-footer-box">'."\n";
echo '    <h3>'._("Support my free(libre) and open-source webcomics on:").'</h3>'."\n";
_display_support_links("thumbnails","100","50","");
echo '</div>'."\n";

# Footer sources
echo '<div class="webcomic-footer-box">'."\n";
echo '    <h3>'._("Source files:").'</h3>'."\n";
$artwork_sources = ''.$comics_path.'/zip/'.$content.'.zip';
if (file_exists($artwork_sources)){
  echo '      '._("Artworks:").''."\n";
  $filename = basename($artwork_sources);
  $fileweight = (filesize($artwork_sources) / 1024) / 1024;
  echo '      <a href="'.$root.'/'.$artwork_sources.'">'.$filename.' <span class="small-info">(Krita KRA, '.round($fileweight, 2).'MB )</span></a>'."\n";
}
echo '    <br>'."\n";
$lang_sources = ''.$comics_path.'/zip/MiniFantasyTheater_lang-pack.zip';
if (file_exists($lang_sources)){
  echo '      '._("Speechbubbles:").''."\n";
  $filename = basename($lang_sources);
  $fileweight = (filesize($lang_sources) / 1024) / 1024;
  echo '      <a href="'.$root.'/'.$lang_sources.'">'.$filename.' <span class="small-info">(Inkscape SVG, '.round($fileweight, 2).'MB)</span></a>'."\n";
}
echo '    <br>'."\n";
$export_dir = $root.'/'.$comics_path.'/hi-res/';
echo '      <a href="'.$export_dir.'">'._("Exports for printing").' <span class="small-info">(JPG, PNG, PDF...)</span></a>'."\n";
echo '    <br>'."\n";
$git_repository = "https://framagit.org/peppercarrot/webcomics";
echo '      <a href="'.$git_repository.'">'._("Git repository").'</a>'."\n";
echo '</div>'."\n";

# Footer license
echo '<div class="webcomic-footer-box">'."\n";
echo '    <h3>'._("License:").'</h3>'."\n";
_display_cc_links("$license", '<a href="#url" title="#name_lrg">#img</a><br><a href="#url" title="#name_lrg">#name_lrg</a>');
echo '    <br>'."\n";
echo '    <br>'."\n";
echo '    '._("Attribution to:").''."\n";
_print_credits($lang, 'miniFantasyTheater', $content);
_display_cc_links("$license", '<span class="small-info">#notice</span>');
echo '</div>'."\n";

# Related URLs
_list_related_url(''.$comics_path.'/.'.$content.'.json');

# end
echo ''."\n";
echo '</div>'."\n";
echo ''."\n";
?>
