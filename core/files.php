<?php if ($root=="") exit; 



# Initiate page
echo '<div class="container">'."\n";
include($file_root.'core/mod-menu-lang.php');

# Sidebar
# =======
echo '  <aside class="sidebar col sml-12 med-12 lrg-2">'."\n";

# Index:
_menu_item(_("Home"), "files/files", "ico-home");
# Tools:
_menu_item($content_info['recent'][0],           "files/recent",           "sidebar-menu-block");
_menu_item(_("All Comic Panels"),                "files/all-comic-panels", "sidebar-menu-block");
_menu_item(_("All episodes"),                    "files/episodes",         "sidebar-menu-block");
# Galleries:
_menu_item($content_info['book-publishing'][0],  "files/book-publishing",  "sidebar-menu-dir");
_menu_item($content_info['eshop'][0],            "files/eshop",            "sidebar-menu-dir");
_menu_item($content_info['press'][0],            "files/press",            "sidebar-menu-dir");
_menu_item($content_info['vector'][0],           "files/vector",           "sidebar-menu-dir");
_menu_item($content_info['wiki'][0],             "files/wiki",             "sidebar-menu-dir");
# Back button:
_menu_item(_("Back to Artworks"),                 "artworks/artworks",     "sidebar-menu-back");

echo '  </aside>'."\n";

# Content
# =======
echo ''."\n";
  echo '<article class="col sml-12 med-12 lrg-10">'."\n";
  echo '    <div class="files">'."\n";

# Generated galleries
# -------------------
if(is_dir($sources.'/0ther/'.$content.'/low-res/')) {
  _header_sml($content);
  _gallery($content);

# Episodes sources
# ----------------
} else if ( $content == "episodes" ) {
  _header_sml($content);
  $all_episodes = $pc_episodes_list;
  asort($all_episodes);
  echo '    <ul id="gallery">'."\n";
  foreach ($all_episodes as $episode_directory) {
    $episode_number = preg_replace('/[^0-9.]+/', '', $episode_directory);
    $cover_path = ''.$sources.'/'.$episode_directory.'/low-res/gfx-only/gfx'.$credits.'E'.$episode_number.'.jpg';
      echo '      <li>'."\n";
      #echo '        <a href="'.$root.'/'.$sources.'/'.$episode_directory.'/">'."\n";
      echo '        <a href="'.$root.'/'.$lang.'/webcomic-sources/'.$episode_directory.'__files.html">'."\n";
      echo '        '._img($cover_path, $episode_directory, 450, 320, 40).''."\n";
      echo '        <span class="file-label">'.$episode_directory.'</span>'."\n";
      echo '        </a>'."\n";
      echo '      </li>'."\n";
  }
  echo '    <li> </li>'."\n";
  echo '    <li> </li>'."\n";
  echo '    <li> </li>'."\n";
  echo '    <li> </li>'."\n";
  echo '    <li> </li>'."\n";
  echo '    </ul>'."\n";

# All comic panels (ex. "overview")
# ---------------------------------
} else if ( $content == "all-comic-panels" ) {
  _header_sml($content);
  $overviewpagecount = 0;
  $all_episodes = $pc_episodes_list;
  echo '    <ul id="gallery">'."\n";
  foreach ($all_episodes as $episode_directory) {
    $episode_number = preg_replace('/[^0-9.]+/', '', $episode_directory);
    $search = glob($sources.'/'.$episode_directory.'/low-res/gfx-only/gfx_Pepper-and-Carrot_by-David-Revoy_E[0-9][0-9]P[0-9][0-9].*');
    $key_set = array_keys($search);
    $last_page = end($key_set);
    echo '  <li class="file-all-comic-panels-li">'."\n";
    echo '    <div class="col sml-12 med-12 lrg-12">'."\n";
    # beautify name
    $episode_titles = array();
    $episode_titles = json_decode(file_get_contents(''.$sources.'/'.$episode_directory.'/hi-res/titles.json'), true);
    # By default, title of episode is in English
    $humanfoldername = $episode_titles["en"];
    # but if a translation exist
    if(isset($episode_titles[$lang])) {
      # we will prefer using it
      $humanfoldername = $episode_titles[$lang];
    }
    echo '      <h2 id="'.$episode_directory.'">'.$humanfoldername.'</h2>'."\n";
    echo '    </div>'."\n";
    echo '    <div>'."\n";
    if (!empty($search)) {
      foreach ($search as $key => $filepath) {
        # Exclude the last page of the loop
        if( $key !== $last_page) {
          # weak workaround for excluding page 00 header
          $filepath = str_replace('P00.jpg', 'Pnon-exist.jpg', $filepath);
          # extracting from the path the filename and path itself
          $filename = basename($filepath);
          $fullpath = dirname($filepath);
          if (file_exists($filepath)) {
            # Our page is existing, it exclude the renamed P00.jpg, start the tag
            echo '      <figure class="thumbnail file-all-comic-panels-thumb col sml-6 med-3 lrg-2">'."\n";
            echo '        <a href="'.$root.'/'.$sources.'/'.$episode_directory.'/hi-res/gfx-only/'.$filename.'" title="   '.$humanfoldername.'" >'."\n";
            # it's a real page:
            $overviewpagecount = $overviewpagecount + 1;
            # display a thumbnail
            echo '          ';
            _img($root.'/'.$filepath, $humanfoldername, 159, 225, 88).''."\n";
            echo '        </a>'."\n";
            # Add a page count caption
            echo '        <figcaption class="file-all-comic-panels">'.$overviewpagecount.'</figcaption>'."\n";
            echo '      </figure>'."\n";
          }
        }
      }
    }
    echo '    </div>'."\n";
    echo '  </li>'."\n";
  }
  echo '</ul>'."\n";

# Recent files
# ------------
} else if ( $content == "recent" ) {
  _header_sml("recent");
  echo '    <ul id="gallery">'."\n";
  foreach ($recent_artworks as $thumb) {
    $variable_content = _getThirdDirectory($thumb);
    $thumb_filename = basename($thumb);
    $thumb_fullpath = dirname($thumb);
    $thumb_option_link = urlencode(str_replace('.jpg', '', $thumb_filename));
    $cover_description = urldecode($thumb_option_link);
    echo '      <li id="'.$thumb_option_link.'">'."\n";
    echo '        <a href="'.$root.'/'.$lang.'/viewer/'.$variable_content.'__'.$thumb_option_link.'.html">'."\n";
    echo '          ';
      _img($root.'/'.$sources.'/0ther/'.$variable_content.'/low-res/'.$thumb_filename, $cover_description, 450, 320, 50);
    echo ''."\n";
    echo '        </a>'."\n";
    echo '      </li>'."\n";
  }
  echo '    </ul>'."\n";

# Index
# -----
} else {
  _header_sml("files");
  echo '    <img src="'.$root.'/core/img/illu-files.jpg" alt="'._("An illustration of a rider on a Yak, transporting goods. He is crossing a strange, dark valley. A number of creatures emerge from the shadows around him, computer files in hand.").'">'."\n";
}

_clearboth();
echo '    </div>'."\n";
echo '  </article>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
