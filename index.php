<?php

# Design of a URL for peppercarrot2021:
# =====================================
# https://wwww.peppercarrot.com/{$lang}/{$mode}/{$content}__{$option}.html
# ...or same URL without URL rewrite:
# https://www.peppercarrot.com/index.php?lang=en?mode=webcomic?content=ep01_Potion-of-Flight?option=hd-transcript
# Every URL starting by two small case letter and a slash (eg "en/") will trigger website engine to process the URL.
# $mode: loads a specific engine to read a type content (eg. page|wiki|webcomic)
# $option is cut after two underscore delimiter "__" and word in it are split into an array by "-" delimiter.
# (see .htaccess to see how rewrite rule distribute them)

# Design for language detection:
# ==============================
# If the URL contains a ISO language, apply the language.
# If a cookie exists, extract and apply language.
# If no cookie found or URL, apply webbrowser default language.
# If no language applied, fallback to English.

# Root of the files of the website is equal to full path to directory where this index.php is.
$file_root = dirname(__FILE__).'/';

# Init:
$version = "202503b";
$starttime = microtime(true);
$lang = "";
$mode = "";
$content = "";
$option = "";
# Trigger a 404 if equal to 1
$you_shall_not_pass = 0;
$not_found = 0;
$test = 0;
# Series title and author info
$credits = '_Pepper-and-Carrot_by-David-Revoy_';

# Config (see configure.php.new)
if (file_exists($file_root.'configure.php')) {
  include_once($file_root.'configure.php');
} else {
  http_response_code(404);
  echo "<pre><strong>Pepper&amp;Carrot website error: configure.php not found.</strong> <br/>";
  echo "Copy configure.php.new to configure.php and make any changes you need for your server setup.</pre>";
  die();
}

# Loading variable from URL
if (isset($_GET['lang'])) {
  $lang = htmlspecialchars($_GET['lang']);
}
if (isset($_GET['mode'])) {
  $mode = htmlspecialchars($_GET['mode']);
}
if (isset($_GET['content'])) {
  $content = htmlspecialchars($_GET['content']);
}
if (isset($_GET['option'])) {
  $option = htmlspecialchars($_GET['option']);
}

# Security: sanitize data the page receives from the URL
$lang = filter_var($lang, FILTER_SANITIZE_URL);
$lang = preg_replace('/[^a-z\._-]/', '', $lang);
$mode = filter_var($mode, FILTER_SANITIZE_URL);
$mode = preg_replace('/[^A-Za-z0-9\._-]/', '', $mode);
$content = filter_var($content, FILTER_SANITIZE_URL);
$content = preg_replace('/[^A-Za-z0-9\._-]/', '', $content);
$option = str_replace(['<','>','!','$','&','(',')','@','^','?','*','{','}','/','\\'], '', $option);

# Manage $lang:
# -------------
# Load our favorite language from saved cookie 'lang', if it exists.
# (Note: this type of cookie is totally allowed by RGPD without requiring user consent).
if(isset($_COOKIE['lang'])) {
  $favlang = $_COOKIE['lang'];
} else {
  $favlang = '';
}

# Rules for undefined language in URL (homepage)
# Note: passing "xx" transform URLs into undefined language
if (empty($lang) OR $lang == "xx") {
  # Check if we have a cookie:
  if(isset($_COOKIE['lang'])) {
    $lang = $_COOKIE['lang'];
  } else {
    # Guess the language of the browser:
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
  }
}

# Default mode if undefined
if (empty($mode)) {
  $mode = "homepage";
};

# If still empty after that, hardcode English
if (empty($lang)) {
  $lang = "en";
}

# Load Gettext (with a hack):
# In order to load arbitrary *.mo file on disk while still using vanilla Gettext, we need a hack.
# All $lang.mo files are stored in 'locale/en_US.utf8/LC_MESSAGES/' folder.
# All $lang.po files are stored in 'po' folder.
# Without this hack, language validation by gettext would depend on locale installed at a server level.
# (eg. no Lojban, requirement to get permission to install and activate more than 50 locale on a server, etc...)
$locale="en_US.utf8";
setlocale(LC_ALL, 'en_US.utf8');
putenv("LANGUAGE=$locale");
putenv("LC_ALL=$locale");
putenv("LANG=$locale");
putenv("LC_MESSAGES=$locale");
bindtextdomain ("$lang", "locale");
textdomain ("$lang");

# Manage $options in URL:
# -----------------------
# Explode option into an array (eg. "hd-transcript" > ["hd", "transcript"])
$option = explode("-", rtrim($option, "/"));

# Get all options from the URL and input $keyword among them.
function _newoption($keyword){
  global $option;
  # Create a copy of the array to preserve the original
  $newoption = $option;
  # We found the keyword
  if (in_array($keyword,$newoption)) {
    # Remove the option, so it create a toggle switch
    $target = array_search($keyword,$newoption);
    if($target !== FALSE){
        unset($newoption[$target]);
    }
  } else {
    # Insert the option
    array_push($newoption,$keyword);
  }
  # Remove empty item in the array
  $newoption = array_filter($newoption);
  # Build our URL
  if(empty($newoption)) {
    $fullnewoptions = '';
  } else {
    $listofnewoptions = implode('-', $newoption);
    $fullnewoptions = '__'.$listofnewoptions;
  }
  return $fullnewoptions;
}

# Manage cookies:
# ---------------
# Save a cookie of $lang (if previous page sent 'c' option, a feature of 'save button', in mod-header.php)
if ($mode == "setup") {
  setcookie("lang", $lang, time() + (86400 * 365), "/"); # 86400 = 1 day
  $target = array_search("c",$option);
  if($target !== FALSE){
    unset($option[$target]);
  }
  #Refresh faster this variable for debug and menu:
  $favlang = $lang;
}

# Manage future generation of links:
# ----------------------------------
$extension = '';
# If on peppercarrot.com, make links like peppercarrot.com/fr/, this pattern save a cookie.
if (!empty($mode)) {
  $printmode = ''.$mode.'/';
  $printcontent = '';
  $setoption = '';
}
# If on peppercarrot.com, hide engine (don't show peppercarrot.com/fr/homepage URL)
if ($mode == "homepage") {
  $printmode = '';
  $printcontent = '';
  $setoption = '';
}
# If on a page like peppercarrot.com/fr/mode/content_option1-2-3.html, plug a 'c' option to save a cookie.
if (!empty($content)) {
  $printcontent = $content;
  $extension = '.html';
  $setoption = _newoption("");
}

# variables
$wip_transcript_string = "Work in progress"; # if this expression is found in transcript, we don't display the transcript.

# Load databases, libraries and image tool:
# ------------------------------
include($file_root.'core/lib-parsedown.php');
include($file_root.'core/lib-database.php');
include($file_root.'core/lib-credits.php');
require($file_root.'core/lib-functions.php');
include($file_root.'core/lib-navigation.php');
include($file_root.'core/lib-image.php');
include($file_root.'core/lib-support-links.php');
include($file_root.'core/lib-creative-commons.php');

# All array on top debug
if ($debugmode > 2.9){
  _databases_debug();
}

# Redirections:
# -------------
# Compatibility with legacy URLs:
# Check if $mode matches a legacy URL, and extract episode number at same time on $result
if (preg_match('/article[0-9][0-9][0-9]episode-([0-9]+)[a-z-]*/', $mode, $results)) {
  # Substract 1 from the result, because array of episode start at zero for ep01.
  $episode_requested = $results[1] - 1;
  $redirection = $root.'/'.$lang.'/webcomic/'.$pc_episodes_list[$episode_requested].'.html';
  header('Location: '.$redirection.'');
}
# Send to webcomic mode every obvious failed attempt to type url
if (preg_match('/webcomic*ep([0-9]+)[a-z-]*/', $mode, $results)) {
  # Substract 1 from the result, because array of episode start at zero for ep01.
  $episode_requested = $results[1] - 1;
  $redirection = $root.'/'.$lang.'/webcomic/'.$pc_episodes_list[$episode_requested].'.html';
  header('Location: '.$redirection.'');
}

# rewrite of 2024 
$redirects = [
    'files/framasoft.html'  => 'artworks/framasoft.html',
    'files/artworks.html'   => 'artworks/artworks.html',
    'files/sketchbook.html' => 'artworks/sketchbook.html',
    'files/misc.html'       => 'artworks/misc.html',
    'license/index.html'    => 'about/index.html#credits',
    'fan-art/artworks.html' => 'fan-art/fan-art.html',
    'webcomics/index.html' => 'webcomics/peppercarrot.html'
];
foreach ($redirects as $redirects_input => $redirects_output) {
  if ( $mode.'/'.$content.'.html'== $redirects_input ) {
      header('Location: '.$root.'/'.$lang.'/'.$redirects_output);
      exit;
  }
}

# Change variable depending of $mode:
# -----------------------------------
if ($mode == "webcomic" ) {
  # Copy $content (extracted from URL on index.php) to our new $epdirectory
  $epdirectory = $content;
  # Test if $epdirectory $content matches the pattern of an episode
  # clean $content__$option on the fly by passing only the matched result (trim trailing '_', $option, .html)
  if (preg_match('/([a-z\-]+[0-9]+_[^_]+)/', $epdirectory, $matches)) {
    # Select the match: an 'episode directory'-like string
    $epdirectory = $matches[1];
    # Extract episode number to work later with array stored in database
    # Substract 1, because episode array starts at zero (eg. [0] => ep01_Potion-of-Flight [1] => ep02_Rainbow-potions))
    if (preg_match('/[a-z-]+([0-9]+)[a-z-]*/', $epdirectory, $results)) {
      $episode_number = $results[1] - 1;
    }
    # Test if the episode directory exists.
    if(is_dir(''.$sources.'/'.$epdirectory)) {
      # Success: URL request a valid episode.
      # Load the translation title (from titles.json file stored in episode, extracted by renderfarm)
      $json_titles = json_decode(file_get_contents(''.$sources.'/'.$epdirectory.'/hi-res/titles.json'));
      if(isset($json_titles->{$lang})) {
        $header_title = $json_titles->{$lang};
      }
      # If the episode is not translated in the target language, the field will be empty and lead to a 404. We need to fill it in English.
      if (empty($header_title)) {
        $header_title = $json_titles->{'en'};
      }
      # Social media thumbnail, a picture that display when the URL is cited.
      # Number of episode is incremented +1 because this is real number (not array starting by 0)
      $social_media_thumbnail_URL = ''.$root.'/'.$sources.'/'.$epdirectory.'/hi-res/gfx-only/gfx_Pepper-and-Carrot_by-David-Revoy_E'.($episode_number + 1).'.jpg';
    } else {
      # Directory doesn't exist, let's try to fix it based on number
      if (!empty($pc_episodes_list[$episode_number])){
        # Format ep6 to ep06 (leading 0, always two digits)
        $episode_number = sprintf('%01s', $episode_number);
        $redirection_episode = $root.'/'.$lang.'/webcomic/'.$pc_episodes_list[$episode_number].'.html';
        header('Location: '.$redirection_episode.'');
      } else {
        # We tried everything, the episode requested probably never existed.
        $not_found = 1;
      }
    }
  }

} else if ($mode == "miniFantasyTheater" ) {
  # Copy $content (extracted from URL on index.php) to our new $epdirectory
  $comicStripID = $content;
  # Test if the MFT comic strip exists.
  if(is_file(''.$sources.'/miniFantasyTheater/.'.$comicStripID.'.json')) {

    # Load lang/??/XXX_info.json (translation) metadata
    $info_lang = array();
    $lang_json_file = ''.$sources.'/miniFantasyTheater/lang/'.$lang.'/'.$content.'_info.json';
    if (!file_exists($lang_json_file)) {
      $lang_json_file = ''.$sources.'/miniFantasyTheater/lang/en/'.$content.'_info.json';
    }
    $info_lang = json_decode(file_get_contents($lang_json_file), true);

    # Success: URL request a valid episode.
    $comicStripID_without_leading_zero = ltrim($comicStripID, '0');
    $episode_number = (int)$comicStripID;
    $episode_number = $episode_number - 1;
    $header_title = $comicStripID_without_leading_zero.'. '.$info_lang["title"];

    # Social media thumbnail
    $social_media_thumbnail_URL = ''.$root.'/'.$sources.'/miniFantasyTheater/low-res/thumbnails/'.$comicStripID.'-thumb_miniFantasyTheater.jpg';
  } 

} else if ($mode == "webcomic-misc" ) {
  $webcomic_misc_dir = $content;
  # Test if the variable is indeed a directory in misc.
  if(is_dir(''.$sources.'/misc/'.$webcomic_misc_dir.'')) {

    $episode_number = array_search($webcomic_misc_dir, $webcomic_misc_list);

    # title
    # Load lang/??/XXX_info.json (translation) metadata
    $info_lang = array();
    $lang_json_file = ''.$sources.'/misc/'.$webcomic_misc_dir.'/lang/'.$lang.'/info.json';
    if (!file_exists($lang_json_file)) {
      $lang_json_file = ''.$sources.'/misc/'.$webcomic_misc_dir.'/lang/en/info.json';
    }
    $info_lang = json_decode(file_get_contents($lang_json_file), true);

    # Success: URL request a valid episode.
    $header_title = $info_lang["title"];

    # Social media thumbnail
    $social_media_thumbnail_URL = ''.$root.'/'.$sources.'/misc/'.$webcomic_misc_dir.'/lang/cover.jpg';
  } 

} else if ($mode == "webcomics" ) {

  if ($content == "peppercarrot"){
    $header_title = _("Pepper&Carrot"); 
    $social_media_thumbnail_URL = $root.'/core/img/og_peppercarrot.jpg';
  }
  if ($content == "miniFantasyTheater"){
    $header_title = _("MiniFantasyTheater"); 
    $social_media_thumbnail_URL = $root.'/core/img/og_miniFantasyTheater.jpg';
  }
  if ($content == "misc"){
    $header_title = _("Webcomics Misc"); 
    $social_media_thumbnail_URL = $root.'/core/img/og_webcomic-misc.jpg';
  }
  

} else if ($mode == "viewer" ) {

# An exception for viewer to build a better thumbnail for the 'meta property="og:image"'
# when someone share a viewer link on social medias
  # Get the $mode (folder name in 0_sources/0ther)
  # if we detect a leading '-src' in the string, switch to display source mode and delete the indicator.
  $viewer_mode = 'regular';
  $content_for_url = $content.'-src';
  $source_button_class = '';
  if (preg_match('/[a-z-]+(-src)/', $content, $results)) {
    $content = str_replace($results[1], '', $content);
    $viewer_mode = 'show-sources';
    $content_for_url = $content;
    $source_button_class = ' active';
  }
  # Get picture from $option in URL (picture filename without extension)
  $option_get = implode("-", $option);
  $pattern = ''.$sources.'/0ther/'.$content.'/low-res/'.$option_get.'.jpg';
  
  # Check if the picture in the viewer exist before continuing
  if (file_exists($pattern)) {

    # Build a title from the filename
    # Extract attribution for the author
    preg_match('/.*([-_]by[-_].*)/', $option_get, $attribution_part);
    $attribution = str_replace('-', ' ', $attribution_part[1]);
    $attribution = str_replace('by', '', $attribution);
    $attribution = str_replace('_', ' ', $attribution);
    $attribution = str_replace('  ', '', $attribution);
    # Extract the Title
    $title = str_replace($attribution_part[1], '', $option_get);
    $title = substr($title, 11);
    $title = str_replace('_', ' ', $title);
    $title = str_replace('-', ' ', $title);
    $title = ucfirst($title);

    # Generate the string for the browser's windows title, for the viewer.
    # At the end of the browser's window in the art slideshow viewer, e.g. "Pepper by Jordan Stillman aka jstillma2 - Viewer - Pepper&Carrot"
    $viewer_string = _('Viewer');
    # %1$s = artwork title, %2$s = author
    $header_title = sprintf(_('%1$s by %2$s'), $title, $attribution).' - '.$viewer_string;
    # Define the thumbnail for social medias
    $social_media_thumbnail_URL = ''.$root.'/'.$pattern.'';

  } else {
    # The pattern of the file requested by the viewer is unknown to the website.
    # Return the 404 picture for the header of the html
    $social_media_thumbnail_URL = ''.$root.'/0ther/artworks/low-res/2019-10-17_chicory_by-David-Revoy.jpg';
  }

} else if ($mode == "fan-art"){ 
  $option_string = implode("-", $option);

  if ($content == "fan-art"){
    $header_title = _("Fan-art"); 
    $social_media_thumbnail_URL = $root.'/core/img/og_fan-art.jpg';
  }

  if ($content == "fan-comics"){
    if ($option_string != "") {
      # Create a clean title from $options
      $clean_title = explode('~~', $option_string)[0];
      $clean_title = str_replace('_', ' ', $clean_title);
      $clean_title = str_replace('-', ' ', $clean_title);
      $title_array = explode(" by ", $clean_title, 2);
      $clean_title = sprintf(_('%1$s by %2$s'), $title_array[0], $title_array[1]);
    }

    $option_to_path = str_replace('~~', '/', $option_string);
    $pathcommunityfolder = $sources.'/0ther/community';
    # '~~' is used only in comic page, it's an episode reader page
    if (strpos($option_string, '~~') != false) {
      # Path to the comic page
      $social_media_thumbnail_URL = $root.'/'.$pathcommunityfolder.'/'.$option_to_path.'.jpg';
      $header_title = $clean_title;
    } else {
      if ($option_string != "") {
        # Non empty option: the catalog of an author
        $social_media_thumbnail_URL = $root.'/'.$pathcommunityfolder.'/'.$option_to_path.'/00_cover.jpg';
        $header_title = $clean_title;
      } else {
        # Empty option: Fan-comics catalog
        $header_title = _("Fan comics");
        $social_media_thumbnail_URL = $root.'/core/img/og_fan-comics.jpg';
      }
    }
  }

  if ($content == "fan-fiction"){
    $header_title = _("Fan Fiction"); 
    $social_media_thumbnail_URL = $root.'/core/img/og_fan-fiction.jpg';
  }

} else {
  # All other mode are pages, a bit more static.
  # Send to social media a generic cover
  $social_media_thumbnail_URL = ''.$root.'/'.$sources.'/0ther/artworks/low-res/2016-11-14_vertical-cover-book-two_screen_by-David-Revoy.jpg';
  # In other case, redirect mode to better Gettext translated labels
  ;
  if ($mode == "artworks")        { $header_title = ''._("Artworks"); };
  if ($mode == "wiki")            { $header_title = $content.' - '._("Wiki").''; };
  if ($mode == "documentation")   { $header_title = $content.' - '._("Documentation").''; };
  if ($mode == "homepage")        { $header_title = _("Homepage"); };
  if ($mode == "files")           { $header_title = _("Files"); };
  if ($mode == "resources")       { $header_title = _("Resources"); };
  if ($mode == "contribute")      { $header_title = _("Contribute"); };
  if ($mode == "wallpapers")      { $header_title = _("Wallpapers"); };
  if ($mode == "setup")           { $header_title = _("Website setup"); };
  if ($mode == "about")           { $header_title = _("About"); };
  if ($mode == "support")         { $header_title = _("Support"); };
  if ($mode == "license")         { $header_title = _("License"); };
  if ($mode == "chat")            { $header_title = _("Chat rooms"); };
  if ($mode == "tos")             { $header_title = _("Terms of Services and Privacy"); };
  if ($mode == "xyz")             { $header_title = "XYZ"; };
  if ($mode == "philosophy")      { $header_title = _("Philosophy"); };
  if ($mode == "webcomic-sources"){ $header_title = _("Webcomic Sources"); };
}

# OpenGraph screenshot loader
# ===========================
# It use pre-saved screenshots, stored in a cache directory.
# Bash script: regenerate_opengraph_screenshots_in_cache.sh
$actual_url = (empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$tail_url = str_replace($root.'/'.$lang.'/', '', $actual_url);
if ( $tail_url == "" ) {
  $tail_url="homepage";
}
$opengraph_cached_img = str_replace(['/', '.'], ['--', '__'], $tail_url);
$opengraph_cached_img = "cache/opengraph_img/".$opengraph_cached_img.".jpg";
if (file_exists($opengraph_cached_img)) {
  $social_media_thumbnail_URL = ''.$root.'/'.$opengraph_cached_img;
}

# Most part (to not say 'pages' of the website) are generated with these component:
# - an ID (array key here), often a directory name with content on the website 
#   (eg 'fan-art' for https://www.peppercarrot.com/0_sources/0ther/fan-art/ )
# - a Title, exposed to the translation system
# - (optional) A rich description, to inform user what the page is about. Around 200 characters each
#   Also exposed to translation system.
# - (optional) A color code (CSS compatible) to manipulate the graphical impact and uniqueness of the page.
# Note: A WIP refactoring is set to move everything to this stucture here as modern HTML header
# require a title, description right before the content of the page is even displayed.
$content_info = array(
  'peppercarrot' => array(
    _("Pepper&Carrot"),
    _("A webcomic that follows the adventures of a young witch named Pepper and her mischievous cat, Carrot. A comedic journey for all ages that embraces creative thinking in a parodic fantasy world."),
    "#31343b"
  ),
  'miniFantasyTheater' => array(
    _("MiniFantasyTheater"),
    _("A webcomic series featuring short stories set in the enchanting world of Pepper&Carrot. With its playful humor and whimsical tales, this collection of gag strips is perfect for audiences of all ages."),
    "#434654"
  ),
  'webcomics-misc' => array(
    _("Misc"),
    _("Discover a unique assortment of one-shot webcomic stories by the same author, featuring a variety of format, styles, themes, and techniques that differ from the main series."),
    "#383e46"
  ),

  'artworks' => array(
    _("Artworks"),
    _("This gallery is exclusively dedicated to finished and colored illustrations of Pepper&Carrot."),
    "#1a364c"
  ),
  'sketchbook' => array(
    _("Sketchbook"),
    _("A collection of traditional and digital preparatory drawings themed on Pepper&Carrot and its universe, including concept art and references."),
    "#CCCABE"
  ),
  'framasoft' => array(
    _("Framasoft"),
    _("A collection of commissioned artworks created for Framasoft, a nonprofit organization that inspired my passion for free (libre) and open-source software."),
    "#8fc4dc"
  ),
  'misc' => array(
    _("Misc"),
    _("A wide range of miscellaneous artwork, including illustrations for my blog and social media, life drawings, fan art, tributes, commissions and other works."),
    "#faedcd"
  ),

  'fan-art' => array(
    _("Fan-art"),
    _("Pepper&Carrot is free culture, encouraging the creation of unique interpretations. I love receiving fan art; send me your artworks, comics, or fiction via social media or email (link in the footer) and I'll showcase them here."),
    "#f7efe2"
  ),
  'fan-comics' => array(
    _("Fan comics"),
    _("Inspired to create your own comic based on Pepper & Carrot? I'd love to read it! Send me your comics via email or social media (links in the footer), and I'll host them on this page."),
    "#5f6263"
  ),
  'cosplay' => array(
    _("Cosplay"),
    _("Got cosplay photos? I want to see them! Send me your best shots via social media or email (links in the footer) and I'll showcase them here."),
    "#f7efe2"
  ),
  'fan-fiction' => array(
    _("Fan Fiction"),
    _("Are you inspired to write fan fiction based on Pepper & Carrot? I'd love to read your stories! Share your scenarios, fictions, and novels with us via email or social media (links in the footer), and I'll feature them on this page for everyone to enjoy!")
  ),

  'files' => array(
    _("Files"),
    _("Welcome to the techie corner of our website! It may not be flashy, but you'll find galleries dedicated to sharing source files, including those for our shop and printed book project. Enjoy exploring!")
  ),
  'all-comic-panels' => array(
    _("All Comic Panels"),
    _("A flat plan of all comic pages of Pepper&Carrot, presented without speech bubbles. This allows for cropping and reusing specific panels as artwork.")
  ),
  'episodes' => array(
    _("All episodes"),
    _("A collection of all Pepper&Carrot episodes. Clicking on a thumbnail provides access to all source files and renderings available in multiple languages for each episode.")
  ),
  'recent' => array(
    _("Recent files"),
    _("The latest uploads of artworks on the website file system including the gallery of Artworks, Files, and Fan-art.")
  ),

  'book-publishing' => array(
    _("Book-publishing"),
    _("Source files for the cover, pages, and special artworks that were essential in creating the books. Available for purchase in our e-shop.")
  ),
  'eshop' => array(
    _("Shop"),
    _("Source files for items available in the shop. These designs can be reproduced at home if you have a printer or other means to transfer them onto your own objects.")
  ),
  'press' => array(
    _("Press"),
    _("Resources for journalists, bloggers, and conference organizers covering Pepper&Carrot. Includes banners, portraits, photos, logos, and other materials.")
  ),
  'vector' => array(
    _("Vector"),
    _("A collection of SVG Inkscape vector images used for the website, book project, and speech bubbles.")
  ),
  'wallpapers' => array(
    _("Wallpapers"),
    _("High-resolution images available for download to use as backgrounds on computers or smartphones.")
  ),
  'wiki' => array(
    _("Wiki"),
    _("Artworks created to illustrate guides, wikis, and documentation.")
  ),

  'xyz' => array(
    _("XYZ"),
    _("An index of episodes currently in development and not yet available to the public on the website, intended for proofreading purposes. Access to this page is restricted to those with the link. Please do not reshare it on social media.")
  ),

  'philosophy' => array(
    _("Philosophy")
  )

);

if (isset($content_info[$content][0])) {
  $header_title = $content_info[$content][0];
}

# Load the 404 error mode
if (empty($header_title)) {
  $header_title = _("Not found:").' '.$mode.'/'.$content.'';
  $you_shall_not_pass = 1;
}

# Exclude $mode request starting by lib- (Libraries) or mod- (Modules)
# These pages are not supposed to be run by themselves.
if (false !== strpos($mode, 'mod-')) {
  $you_shall_not_pass = 1;
}
if (false !== strpos($mode, 'lib-')) {
  $you_shall_not_pass = 1;
}

###########################
# START BUILDING THE PAGE #
###########################

# Build the header of the html output page:
include($file_root.'core/mod-header.php');

# Build the content depending on $mode.
$filemode = $file_root.'core/'.$mode.'.php';
if (file_exists($filemode) AND $you_shall_not_pass !== 1 ) {
  # Include the main content
  include($filemode);
} else {
  # Fallback on a 404 page
  include($file_root.'core/404.php');
}

# Floating info bar debug
if ($debugmode > 0.9){
  echo ''."\n";
  echo '<div style="color: #fff; font: 13px monospace; background: #000; position:fixed; bottom: 0px; right: 0px; width: 100%; height: 18px; opacity: 0.7; padding-top:3px; z-index: 9999;">'."\n";
  echo '  <div style="text-align:center; margin: 0 auto;">'."\n";
  echo '<b>ALPHA website: work in progress, version '.$version.'</b>'."\n";
  $endtime = microtime(true);
  printf("| Page loaded in: %f seconds", $endtime - $starttime );
  echo '|| <span>'."\n";
  echo '$lang: '.$lang."\n";
  echo '| favlang: '.$favlang."\n";
  if (isset($_COOKIE) AND isset($_COOKIE['lang'])) {
    echo '| Cookie: '.$_COOKIE['lang']."\n";
  }
  echo '</span> || <span>'."\n";
  echo '$mode: '.$mode."\n";
  echo '| $content: '.$content."\n";
  echo '| $option: '._newoption('')."\n";
  echo '</span>'."\n";
  echo '| $test: '.$test."\n";
  echo '    <br/>'."\n";
  echo '  </div>'."\n";
  echo '</div>'."\n";
}

# Open graph debug
if ($debugmode > 2.9){

  echo ''."\n";
  echo '<div style="position:fixed; bottom: 20px; left: 0px; color: #9cbf05; font: 11px monospace; background-color: #2e3436; width: 500px; margin: 0 auto; padding: 20px; z-index: 1500; opacity: 0.7;">';
  echo '<img src="'.$social_media_thumbnail_URL.'" style="height:80px; float: left; margin: 0 1rem;">';
  echo '<b>Open Graph Debug</b><br>';
  echo '<br>';
  echo '<b>title</b>: '.$header_title.'<br>';
  echo '<b>media</b>: '.$social_media_thumbnail_URL.'<br>';
  echo '</div>';
}

# Build the footer of the html output page:
include($file_root.'core/mod-footer.php');
?>

