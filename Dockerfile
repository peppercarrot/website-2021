# DISCLAIMER / KNOWN ISSUE
# ========================
# This Dockerfile is not maintained by the owner of this repo.
# But it is here because it brings some convenience to building the website for the community.
#
# Known issues: 
# - Gettext has issue displaying the translations.
#
# For any questions, maintenance, or problems with it
# contact @EEG_emperor on our chat: https://matrix.to/#/#peppercarrot:matrix.org
# Alternatively, you can also contact @lagleki on Framagit.
# Thank you.

# Use an official PHP runtime as a parent image
FROM php:8.1-apache

# Set the working directory in the container
WORKDIR /var/www/html

# Install any needed packages
RUN apt-get update && apt-get install -y \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libzip-dev \
    libxml2-dev \
    libonig-dev \
    gettext \
    zip \
    unzip \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd zip xml mbstring gettext

# Copy the current directory contents into the container at /var/www/html
COPY . /var/www/html

# Set up the dummy database and required directories
RUN cp -r .0_sources 0_sources \
    && cp -r .data data \
    && cp .gitignore.template .gitignore \
    && cp -r .0_sources/0ther/artworks 0_sources/0ther/misc \
    && cp -r .0_sources/0ther/artworks 0_sources/0ther/sketchbook \
    && cp -r .0_sources/0ther/artworks 0_sources/0ther/book-publishing \
    && mkdir -p cache \
    && touch 0_sources/last_updated.txt

# Set permissions
RUN chown -R www-data:www-data /var/www/html \
    && chmod -R 755 /var/www/html \
    && chmod -R 777 cache \
    && chmod 666 0_sources/last_updated.txt \
    && mkdir -p /var/www/html/locale/en_US.utf8/LC_MESSAGES \
    && chown -R www-data:www-data /var/www/html/locale \
    && chmod -R 777 /var/www/html/locale

# Copy configure.php.new to configure.php
RUN cp configure.php.new configure.php

# Enable Apache mod_rewrite
RUN a2enmod rewrite

# Update Apache configuration to allow .htaccess overrides
RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf

# Run translation management commands
RUN cd /var/www/html/po \
    && ./extract_strings_from_PHP_and_save_to_catalog-pot.sh \
    && ./update_all_po_from_catalog-pot.sh \
    && ./compile_all_PO_into_MO.sh \
    && ./extract_translation_percent_completion.sh

# Switch to www-data user
USER www-data

# Expose port 80
EXPOSE 80

# Start Apache when the container launches
CMD ["apache2-foreground"]