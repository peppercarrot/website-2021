#!/bin/bash

Off=$'\e[0m'
Purple=$'\e[1;35m'
Blue=$'\e[1;34m'
Green=$'\e[1;32m'
Red=$'\e[1;31m'
Yellow=$'\e[1;33m'

# variable
script_dir="${PWD}"
target_dir="cache/opengraph_img"
website_root="http://localhost/peppercarrot/en"
url_to_capture=( ""
              "webcomics/peppercarrot.html" 
              "webcomics/miniFantasyTheater.html" 
              "webcomics/misc.html"

              "artworks/artworks.html" 
              "artworks/sketchbook.html"
              "artworks/framasoft.html"
              "artworks/misc.html"

              "files/files.html"
              "files/recent.html"
              "files/all-comic-panels.html"
              "files/episodes.html"
              "files/book-publishing.html"
              "files/eshop.html"
              "files/press.html"
              "files/vector.html"
              "files/wiki.html"

              "fan-art/fan-art.html"
              "fan-art/fan-comics.html"
              "fan-art/cosplay.html"
              "fan-art/fan-fiction.html"

              "philosophy/index.html"
              "contribute/index.html"
              "chat/index.html"
              "resources/index.html"
              "support/index.html"

              "wiki/index.html"
              "documentation/010_Translate_the_comic.html"
              "wallpapers/index.html"
              "chat/index.html"

              "about/index.html"
            )


echo ""
echo "${Yellow} Capture website screenshot to opengraph cache ${Off}"
echo "${Yellow} --------------------------------------------- ${Off}"
echo ""

cd $script_dir
if [ ! -d "$target_dir" ]; then
  mkdir -p "$target_dir"
fi

if ! command -v cutycapt &> /dev/null; then
    echo "${Red}Error:${Off} cutycapt is not installed. You can install it with:"
    echo "sudo apt-get install cutycapt"
else
    # cutycapt is installed
    # check Cache directory
    if [ -d "$target_dir" ]; then
      # Iterate over each url to capture
      for url in "${url_to_capture[@]}"; do
        target_url=$website_root/$url
        # Manage an exception if the url is empty (for homepage)
        if [ -z "$url" ]; then
            url="homepage"
        fi
        # Build a filename for the PNG without . or /
        # Replace all / with -- , and . with __
        url="${url//\//--}"
        url="${url//./__}"
        target_png_filename=$target_dir"/"$url".png"
        target_jpg_filename=$target_dir"/"$url".jpg"
        echo "${Blue}  Capture for: $target_url ${Off}"
        echo "  Target filename: $target_jpg_filename"
        cutycapt --url="$target_url" --min-width=1400 --min-height=900 --out="$target_png_filename"
        convert $target_png_filename -crop 1400x900+0+0 -resize 1280x -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 82%  $target_jpg_filename
        if [ -f "$target_png_filename" ]; then
          rm "$target_png_filename"
        fi
        if [ -f "$target_jpg_filename" ]; then
          echo "${Green}  Done:${Off} File is exported to jpg in target directory."
        else
          echo "${Red}  Error:${Off} File is not exported in target directory."
        fi
        echo ""

      done

    else
      echo "${Red}Error:${Off} $target is not a valid directory"
    fi
fi
