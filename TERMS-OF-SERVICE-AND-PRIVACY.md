# Terms of service and Privacy

This document assure compliance with the [General Data Protection Regulation](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation); a regulation in the European Union on data protection and privacy for all individuals within the European Union (EU) and the European Economic Area (EEA). 

It is only available in English.

The website https://www.peppercarrot.com is hosted in France and therefore subject to this regulation.

The purpose of this regulation is **transparency**, for you, the visitor. You'll know in this document who we are, what personal data we collect, why we collect it, and how long we retain it. How do we protect your data, and what data breach/moderation procedures we have in place.

## Who we are

My name is David Revoy, founder/author/artist and webdev of Pepper&Carrot and I own and maintain **almost everything myself here**, including the website https://www.peppercarrot.com

The sources of https://www.peppercarrot.com are available at https://framagit.org/peppercarrot/website-2021 and distributed under the [GPL3](https://framagit.org/peppercarrot/website/blob/master/LICENSE).

**No user data are on the Pepper&Carrot website** and I could end this GDPR document here, but I'll continue for detailing it anyway. Pepper&Carrot sources contains only this website's engine and the content.

### What about contributors?

The active contributors to the project (listed here https://framagit.org/groups/peppercarrot/-/group_members ) have the ability to modify the source code of the translations of the comic and also the engine of the websites (all changes needs my approval in order to appear on Pepper&Carrot). 

**Contributors do not have access** to any data. 

Also, in extension: contributors do not have access to my personal blog. Contributors do not have access to my social media accounts. Contributors do not have access to my patronage platform.

Therefore, I, David Revoy, am the only responsible in case of a data issue and that's why you can contact me at [info@davidrevoy.com](mailto:info@davidrevoy.com) to discuss any issue. I'll solve them as soon as possible.

## What personal data we collect, why we collect it, and how long we retain it

Pepper&Carrot doesn't use **sessionIDs**. Pepper&Carrot website **don't embed medias** from other platforms and **no third party or partners** can get your data here.

### Cookies

The website Pepper&Carrot uses a single cookie stored on your webbrowser for one year. This cookie let your webbrowser remember your favorite language − if you set one in the drop down language menu selection− and is totally optional. **The website works fine without it** and 99,999% of visitor here don't even use this feature in the language menu. Yes, that's one user on 100 000 according to the server log, and I highly suspect it is only my own visits of the website...

I don't have to put a "accept cookie" button on a bottom toolbar of the website because **this usage of cookie is permited and totally legal under this regulation**.

(Note: you can check yourself the content of the cookie by entering this address on the Firefox browser: chrome://browser/content/preferences/cookies.xul, then filter the website by entering `peppercarrot`. )

### Analytics

Pepper&Carrot website don't use any analytic services, or any script for that purpose.

But the hosting company I choosed for the two websites offer me tools to read "server logs". [OVH](https://www.ovh.com) and domain-name registrar [Gandi](http://www.gandi.net) own respectively the machine where the websites are hosted and their internet addresses. They can display in their settings an interface with diagrams. 

I am −David Revoy− the only possessor of the account to access such information. The graphic you might see me posting online when I speak about my audience, website visits, etc... are all based on this data and tools provided by my hosting services and not on a third parties analytic service.

(Note: Administrators and employee with access at OVH and Gandhi might also have access to read this logs because it is their own machine afterall). 

## How I protect your data

Good question, but a simple answer: as I don't have any data of yours, I have nothing to defend. This is in my opinion the best strategy for protecting your data.

## What data breach/moderation procedures we have in place

As I don't collect any sensible information about visitors, a big hack or data breach to Pepper&Carrot website would affect **nothing for you**. The website might just go temporary offline, or displaying something else in case of a big hack, and I'll just be deeply sorry and sad about it and probably I'll struggle with the team at OVH and Gandi for a couples of days to take back control of my accounts.

### Procedure

Nothing about visitor data will be compromised unless the hacker just put a very vicious change within the code itself of the website, without changing its appearance, something that would collect data of visitors, or run blockchain computation, or display adv, or I don't know what other horrors...

In order to be certain to not have a scenario like this happening too often, I'm mirroring the website repository (the source code) to the online version and periodically check for differences via a script I'm running manually. In case the code running online is different than the public source, the script automatically overwrite to the latest fresh version of the official source code.

### Moderation

For moderation issue, I'm redirecting you to [our code of conduct](https://www.peppercarrot.com/en/documentation/409_Code_of_Conduct.html).

_- Thank you for reading._

---

**Contact information**: [info@davidrevoy.com](mailto:info@davidrevoy.com)

---

*Note: the source of this document is the markdown file TERMS-OF-SERVICE-AND-PRIVACY.md available at the root of https://framagit.org/peppercarrot/website-2021*
